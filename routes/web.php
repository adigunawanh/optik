<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function () {
  Route::get('product', 'ProductController@index');
  Route::get('product/all', 'ProductController@all');
  Route::get('product/detail/{id}/{category_id}', 'ProductController@detail');
  Route::get('product/detail-branch/{id}/{category_id}/{branch_id}', 'ProductController@detail_branch');
  Route::get('product/branch/all', 'ProductController@branch_all');
  Route::get('product/search', 'ProductController@search');
  Route::get('product/category/{category}', 'ProductController@getByCategory');
  Route::get('product/create/{category}', 'ProductController@create');
  Route::post('product/store', 'ProductController@store');
  Route::get('product/edit/{id}', 'ProductController@edit');
  Route::post('product/update', 'ProductController@update');
  Route::get('product/delete/{id}', 'ProductController@destroy');
  Route::get('product/print/{id}/{stock}', 'ProductController@print');
  Route::post('product/print/custom', 'ProductController@print_custom');
  Route::post('product/scane-barcode', 'ProductController@scane_barcode');
  Route::post('product/scane-barcode-branch', 'ProductController@scane_barcode_branch');
  Route::get('product/excel', 'ProductController@excel');

  Route::get('product-field', 'ProductFieldController@index');
  Route::get('product-field/create', 'ProductFieldController@create');
  Route::post('product-field/store', 'ProductFieldController@store');
  Route::get('product-field/edit/{id}', 'ProductFieldController@edit');
  Route::post('product-field/update/{id}', 'ProductFieldController@update');
  Route::get('product-field/delete/{id}', 'ProductFieldController@destroy');

  Route::get('product-category/create', 'ProductCategoryController@create');
  Route::post('product-category/store', 'ProductCategoryController@store');
  Route::get('product-category/edit/{id}', 'ProductCategoryController@edit');
  Route::post('product-category/update/{id}', 'ProductCategoryController@update');
  Route::get('product-category/delete/{id}', 'ProductCategoryController@destroy');

  Route::get('rekamedis', 'RekamedisController@index');
  Route::get('rekamedis/member/{id}', 'RekamedisController@member');
  Route::get('rekamedis/create', 'RekamedisController@create');
  Route::post('rekamedis/store', 'RekamedisController@store');
  Route::get('rekamedis/search', 'RekamedisController@search');

  Route::get('transaction', 'TransactionController@index');
  Route::get('transaction/branch/{branch_id}', 'TransactionController@branch');
  Route::get('transaction/search', 'TransactionController@search');
  Route::get('transaction/create', 'TransactionController@create');
  Route::get('transaction/create/{id_rekamedis}', 'TransactionController@create');
  Route::post('transaction/store', 'TransactionController@store');
  Route::post('transaction/store-branch/{branch_id}', 'TransactionController@store_branch');
  Route::post('transaction/buy/{invoice}', 'TransactionController@buy');
  Route::get('transaction/detail/{invoice}', 'TransactionController@detail');
  Route::get('transaction/success/{invoice}', 'TransactionController@success');
  Route::get('transaction/print/{invoice}', 'TransactionController@print');
  Route::get('transaction/print/nota-process/{invoice}', 'TransactionController@print_nota_process');
  Route::get('transaction/print/nota-process-softlense/{invoice}', 'TransactionController@print_nota_process_softlense');
  Route::get('transaction/print/nota-process-frame/{invoice}', 'TransactionController@print_nota_process_frame');
  Route::get('transaction/excel', 'TransactionController@excel');
  Route::get('transaction/excel/branch/{id}', 'TransactionController@excel_branch');
  Route::get('transaction/closing/branch/{id}', 'TransactionController@closing_branch');
  Route::get('transaction/destroy/{invoice}', 'TransactionController@destroy');
  Route::get('transaction/complete/{invoice}', 'TransactionController@complete');
  Route::get('transaction/reporting', 'TransactionController@reporting');
  Route::get('transaction/kwitansi/{invoice}', 'TransactionController@print_kwitansi');

  Route::get('branch', 'BranchController@index');
  Route::get('branch/category/{id}', 'BranchController@category');
  Route::get('branch/product/detail/{category_id}/{branch_id}', 'BranchController@product');
  Route::get('branch/create', 'BranchController@create');
  Route::post('branch/store', 'BranchController@store');
  Route::get('branch/edit/{id}', 'BranchController@edit');
  Route::post('branch/update/{id}', 'BranchController@update');
  Route::get('branch/list-user/{id}', 'BranchController@list_user');
  Route::get('branch/add-user/{id}', 'BranchController@add_user');
  Route::post('branch/add-user/store/{id}', 'BranchController@add_user_store');
  Route::get('branch/edit-user/{branch_id}/{id}', 'BranchController@edit_user');
  Route::post('branch/edit-user/update/{branch_id}/{id}', 'BranchController@edit_user_update');
  Route::get('branch/delete-user/{id}', 'BranchController@delete_user');
  Route::get('branch/excel', 'BranchController@excel');
  Route::get('branch/list-user-excel/{branch_id}', 'BranchController@list_user_excel');
  Route::get('branch/delete/{id}', 'BranchController@destroy');

  Route::get('customer', 'CustomerController@index');
  Route::post('customer/store', 'CustomerController@store');

  Route::get('member', 'MemberController@index');
  Route::get('member/create', 'MemberController@create');
  Route::post('member/store', 'MemberController@store');
  Route::get('member/edit/{id}', 'MemberController@edit');
  Route::post('member/update/{id}', 'MemberController@update');
  Route::get('member/show/{id}', 'MemberController@show');
  Route::get('member/delete/{id}', 'MemberController@destroy');

  Route::get('pegawai', 'PegawaiController@index');
  Route::get('pegawai/create', 'PegawaiController@create');
  Route::post('pegawai/store', 'PegawaiController@store');
  Route::get('pegawai/edit/{id}', 'PegawaiController@edit');
  Route::post('pegawai/update/{id}', 'PegawaiController@update');
  Route::get('pegawai/delete/{id}', 'PegawaiController@destroy');
  Route::get('pegawai/excel', 'PegawaiController@excel');

  Route::get('supplier', 'SupplierController@index');
  Route::get('supplier/create', 'SupplierController@create');
  Route::post('supplier/store', 'SupplierController@store');
  Route::get('supplier/edit/{id}', 'SupplierController@edit');
  Route::post('supplier/update/{id}', 'SupplierController@update');
  Route::get('supplier/delete/{id}', 'SupplierController@destroy');
  Route::get('supplier/excel', 'SupplierController@excel');

  Route::get('surat-jalan', 'SuratJalanController@index');
  Route::get('surat-jalan/create/{branch_id}', 'SuratJalanController@create');
  Route::post('surat-jalan/store/{branch_id}', 'SuratJalanController@store');
  Route::get('surat-jalan/detail/{id}', 'SuratJalanController@detail');
  Route::get('surat-jalan/excel/{id}', 'SuratJalanController@excel');
  Route::get('surat-jalan/branch/{branch_id}', 'SuratJalanController@branch');
  Route::get('surat-jalan/edit/{surat_id}', 'SuratJalanController@edit');
  Route::post('surat-jalan/update/{surat_id}', 'SuratJalanController@update');
  Route::get('surat-jalan/print/{id}', 'SuratJalanController@pdf');

  Route::get('grosir', 'GrosirController@index');
  Route::get('grosir/create', 'GrosirController@create');
  Route::get('grosir/detail/{id_surat}', 'GrosirController@detail');
  Route::post('grosir/store', 'GrosirController@store');
  Route::get('grosir/return/{id}', 'GrosirController@return');
  Route::post('grosir/return/store/{id}', 'GrosirController@return_store');
  Route::get('grosir/pembeli', 'GrosirController@pembeli');
  Route::get('grosir/pembeli/create', 'GrosirController@pembeli_create');
  Route::post('grosir/pembeli/store', 'GrosirController@pembeli_store');
  Route::get('grosir/pembeli/edit/{id}', 'GrosirController@pembeli_edit');
  Route::post('grosir/pembeli/update/{id}', 'GrosirController@pembeli_update');
  Route::get('grosir/pembeli/delete/{id}', 'GrosirController@pembeli_destroy');
  Route::get('grosir/print/{id}', 'GrosirController@print');

  Route::get('logout', 'Auth\LoginController@logout')->name('logout' );
  Route::get('/home', 'HomeController@index')->name('home');
  Route::get('log', 'HomeController@log');
  Route::get('log/excel', 'HomeController@log_excel');
  Route::get('/', 'HomeController@index');
});

Auth::routes();
Route::post('login', 'Auth\LoginController@authenticate')->name('login');
