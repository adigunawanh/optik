<?php

use Illuminate\Database\Seeder;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('pegawai')->insert([
        [
          'nip' => '1',
          'nama' => 'John',
          'jabatan' => 'Kasir',
        ],
        [
          'nip' => '2',
          'nama' => 'Doe',
          'jabatan' => 'Sales',
        ],
        [
          'nip' => '3',
          'nama' => 'Peter',
          'jabatan' => 'Pusat',
        ],
      ]);
    }
}
