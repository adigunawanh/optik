<?php

use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('product_categories')->insert([
        [
          'name' => 'Softlense',
          'barcode_type' => 'C39'
        ],
        [
          'name' => 'Frame',
          'barcode_type' => 'QRCODE'
        ],
        [
          'name' => 'Sun Glass',
          'barcode_type' => 'QRCODE'
        ],
        [
          'name' => 'Lensa',
          'barcode_type' => 'C39'
        ],
        [
          'name' => 'Accessories Softlense',
          'barcode_type' => 'C39'
        ],
        [
          'name' => 'Accessories Kacamata',
          'barcode_type' => 'C39'
        ],
      ]);
    }
}
