<?php

use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('branches')->insert([
        [
          'name' => 'Pusat',
          'address' => 'Bandung',
          'status' => '1',
        ],
        [
          'name' => 'Cabang 1',
          'address' => 'Bandung',
          'status' => '0',
        ],
      ]);
    }
}
