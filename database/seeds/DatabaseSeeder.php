<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ProductFieldSeeder::class);
        $this->call(ProductCategorySeeder::class);
        $this->call(ProductCategoryFieldSeeder::class);
        $this->call(ProductFieldValuesSeeder::class);
        $this->call(PegawaiSeeder::class);
        $this->call(BranchSeeder::class);
        $this->call(BranchUserSeed::class);           
    }
}
