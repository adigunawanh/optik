<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        [
          'name' => 'Pusat',
          'email' => 'pusat@optik.dev',
          'password' => bcrypt('password'),
        ],
        [
          'name' => 'Bandung',
          'email' => 'bandung@optik.dev',
          'password' => bcrypt('password'),
        ],

      ]);
    }
}
