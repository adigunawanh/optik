<?php

use Illuminate\Database\Seeder;

class BranchUserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('branch_users')->insert([
        [
          'branch_id' => '1',
          'user_id' => '1',
        ],
        [
          'branch_id' => '2',
          'user_id' => '2',
        ],
      ]);
    }
}
