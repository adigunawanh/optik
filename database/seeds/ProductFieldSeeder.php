<?php

use Illuminate\Database\Seeder;

class ProductFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('product_fields')->insert([
        [
          'name' => 'Merk'
        ],
        [
          'name' => 'Jumlah'
        ],
        [
          'name' => 'Harga Beli'
        ],
        [
          'name' => 'Harga Jual'
        ],
        [
          'name' => 'Warna'
        ],
        [
          'name' => 'MiliLiter'
        ],
        [
          'name' => 'Type'
        ],
        [
          'name' => 'Ukuran'
        ],
        [
          'name' => 'Spheris'
        ],
        [
          'name' => 'Jenis Accessories Softlense'
        ],
        [
          'name' => 'Jenis Accessories Kacamata'
        ],
      ]);
    }
}
