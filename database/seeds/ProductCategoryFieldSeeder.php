<?php

use Illuminate\Database\Seeder;

class ProductCategoryFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('product_category_fields')->insert([
        [
          'product_category_id' => '1',
          'product_field_id' => '1,2,3,4,5,6,10',
        ],
        [
          'product_category_id' => '2',
          'product_field_id' => '1,2,3,4,5,6,8',
        ],
        [
          'product_category_id' => '3',
          'product_field_id' => '1,2,3,4,5,6,8',
        ],
        [
          'product_category_id' => '4',
          'product_field_id' => '1,2,3,4,5,9',
        ],
        [
          'product_category_id' => '5',
          'product_field_id' => '1,2,3,4,5,7,11',
        ],
        [
          'product_category_id' => '6',
          'product_field_id' => '1,2,3,4,5,12',
        ],
      ]);
    }
}
