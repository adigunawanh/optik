<?php

use Illuminate\Database\Seeder;

class ProductFieldValuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('product_field_values')->insert([
        [
          'product_field_id' => '8',
          'value' => 'Full'
        ],
        [
          'product_field_id' => '8',
          'value' => 'Semi'
        ],
        [
          'product_field_id' => '8',
          'value' => 'Rimless'
        ],
        [
          'product_field_id' => '11',
          'value' => 'Comf Drops'
        ],
        [
          'product_field_id' => '11',
          'value' => 'Box Softlense'
        ],
        [
          'product_field_id' => '11',
          'value' => 'MPS'
        ],
        [
          'product_field_id' => '12',
          'value' => 'Lense Cleaner'
        ],
        [
          'product_field_id' => '12',
          'value' => 'Lap'
        ],
        [
          'product_field_id' => '12',
          'value' => 'Box Frame'
        ],
        [
          'product_field_id' => '12',
          'value' => 'Rantai'
        ],
        [
          'product_field_id' => '12',
          'value' => 'Other'
        ],
      ]);
    }
}
