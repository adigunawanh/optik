<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('branches', function (Blueprint $table) {
         $table->increments('id');
         $table->string('name');
         $table->string('address');
         $table->enum('status', ['0','1'])->comment('0=cabang, 1=pusat');
         $table->longText('frame_description')->nullable();
         $table->longText('note_description')->nullable();
         $table->integer('point')->default(0);
         $table->integer('created_by')->default(1);
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('branches');
     }
}
