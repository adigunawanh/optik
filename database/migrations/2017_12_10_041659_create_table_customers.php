<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('customers', function (Blueprint $table) {
         $table->increments('id');
         $table->string('nik');
         $table->string('name');
         $table->string('tempat_lahir')->nullable();
         $table->date('tanggal_lahir')->nullable();
         $table->enum('jenis_kelamin', ['L', 'P'])->nullable();
         $table->string('telepon')->nullable();
         $table->string('email')->nullable();
         $table->string('alamat')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('customers');
     }
}
