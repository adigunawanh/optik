<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('transactions', function (Blueprint $table) {
         $table->increments('id');
         $table->string('invoice');
         $table->string('barcode');
         $table->integer('total');
         $table->integer('id_rekamedis');
         $table->integer('created_by');
         $table->integer('sales')->default(0);
         $table->integer('opticien')->default(0);
         $table->integer('dp')->default(0);
         $table->date('tanggal_selesai')->nullable();
         $table->enum('status', ['0','1'])->default('1')->comment('0=blm lunas, 1=lunas');
         $table->enum('transaction_status', ['0','1'])->default('1')->comment('0=cancel, 1=success');
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('transactions');
     }
}
