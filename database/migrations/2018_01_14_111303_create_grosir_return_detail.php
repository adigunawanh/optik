<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrosirReturnDetail extends Migration
{
  public function up()
  {
    Schema::create('grosir_return_detail', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('id_return');
      $table->integer('id_stock');
      $table->integer('jumlah');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('grosir_return_detail');
  }
}
