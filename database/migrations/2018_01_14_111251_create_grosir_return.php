<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrosirReturn extends Migration
{
  public function up()
  {
    Schema::create('grosir_return', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nama_peretur');
      $table->string('nomor_surat');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('grosir_return');
  }
}
