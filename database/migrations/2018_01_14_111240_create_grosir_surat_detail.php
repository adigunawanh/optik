<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrosirSuratDetail extends Migration
{
  public function up()
  {
    Schema::create('grosir_surat_detail', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('id_surat');
      $table->integer('id_stock');
      $table->integer('harga_jual')->nullable();
      $table->integer('jumlah')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('grosir_surat_detail');
  }
}
