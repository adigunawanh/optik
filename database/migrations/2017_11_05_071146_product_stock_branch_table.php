<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductStockBranchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('product_stock_branch', function (Blueprint $table) {
         $table->increments('id');
         $table->integer('product_stock_id');
         $table->integer('branch_id');
         $table->integer('selling_price');
         $table->integer('stock');
         $table->integer('created_by')->default(1);
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('product_stock_branch');
     }
}
