<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransactionsDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('transactions_details', function (Blueprint $table) {
         $table->increments('id');
         $table->string('invoice');
         $table->integer('product_id');
         $table->string('product');
         $table->string('price');
         $table->integer('qty');
         $table->integer('discount')->default(0);
         $table->string('ukuran_lensa')->nullable();
         $table->string('warna_lensa')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('transactions_details');
     }
}
