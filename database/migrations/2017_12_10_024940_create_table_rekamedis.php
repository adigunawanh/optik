<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRekamedis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('rekamedis', function (Blueprint $table) {
         $table->increments('id');
         $table->string('id_pasien');
         $table->enum('status', ['0', '1'])->comment('0=customer, 1=member');
         $table->string('od_sphiris')->nullable();
         $table->string('os_sphiris')->nullable();
         $table->string('od_cylindris')->nullable();
         $table->string('os_cylindris')->nullable();
         $table->string('od_axis')->nullable();
         $table->string('os_axis')->nullable();
         $table->string('addition')->nullable();
         $table->string('add_kiri')->nullable();
         $table->string('add_kanan')->nullable();
         $table->longText('notes')->nullable();
         $table->integer('created_by')->default(1)->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('rekamedis');
     }
}
