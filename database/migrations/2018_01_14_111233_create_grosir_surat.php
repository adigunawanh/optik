<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrosirSurat extends Migration
{
  public function up()
  {
    Schema::create('grosir_surat', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nomor_surat');
      $table->integer('id_pembeli')->nullable();
      $table->string('nama_pembeli')->nullable();
      $table->string('hp')->nullable();
      $table->integer('total_bayar')->default(0);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('grosir_surat');
  }
}
