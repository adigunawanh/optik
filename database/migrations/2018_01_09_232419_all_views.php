<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::statement("
        CREATE VIEW _view_products as
        select products.id AS id,products.product_field_content AS product_field_content,product_stock.barcode AS barcode,product_stock.id AS product_stock_id
        from (products join product_stock on((product_stock.product_id = products.id)))
      ");

      DB::statement("
        CREATE VIEW _view_products_branch as
        select product_field_content,products.id AS product_id,product_stock.id AS product_stock_id,product_stock_branch.id AS product_stock_branch_id,product_stock_branch.branch_id AS branch_id,product_stock_branch.selling_price AS selling_price_branch,product_stock_branch.stock AS stock_branch,product_stock.stock AS stock_pusat,product_stock.barcode AS barcode from ((products join product_stock on((product_stock.product_id = products.id))) join product_stock_branch on((product_stock_branch.product_stock_id = product_stock.id)))
        join branches on branches.id = product_stock_branch.branch_id
      ");

      DB::statement("
        CREATE VIEW _view_surat_jalan as
        select surat_jalan.id AS id_surat,branches.name AS branch,surat_jalan.tanggal_pengiriman AS tanggal_pengiriman,surat_jalan.tanggal_terima AS tanggal_terima,surat_jalan.nip_penerima AS nip_penerima, surat_jalan_detail.id_stock AS id_stock,surat_jalan_detail.jumlah AS jumlah 
        from (surat_jalan join surat_jalan_detail on((surat_jalan_detail.id_surat = surat_jalan.id)))
        join branches on branches.id = surat_jalan.branch_id
        join pegawai on pegawai.nip = surat_jalan.nip_penerima
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement("DROP VIEW _view_products");
      DB::statement("DROP VIEW _view_products_branch");
      DB::statement("DROP VIEW _view_surat_jalan");
    }
}
