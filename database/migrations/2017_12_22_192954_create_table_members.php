<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('members', function (Blueprint $table) {
         $table->increments('id');
         $table->string('member_id');
         $table->string('nik');
         $table->string('name');
         $table->string('tempat_lahir')->nullable();
         $table->date('tanggal_lahir')->nullable();
         $table->enum('jenis_kelamin', ['L', 'P'])->nullable();
         $table->string('telepon')->nullable();
         $table->string('email')->nullable();
         $table->string('alamat')->nullable();
         $table->integer('point')->default(0);
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('members');
     }
}
