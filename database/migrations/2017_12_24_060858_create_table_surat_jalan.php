<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSuratJalan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('surat_jalan', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('branch_id');
        $table->date('tanggal_pengiriman')->nullable();
        $table->date('tanggal_terima')->nullable();
        $table->string('nip_penerima')->nullable();
        $table->integer('created_by')->default(1);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('surat_jalan');
    }
}
