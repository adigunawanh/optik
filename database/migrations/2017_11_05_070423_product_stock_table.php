<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('product_stock', function (Blueprint $table) {
         $table->increments('id');
         $table->integer('product_id');
         $table->integer('stock');
         $table->longText('barcode');
         $table->integer('created_by')->default(1);
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('product_stock');
     }
}
