<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrosirPembeli extends Migration
{
  public function up()
  {
    Schema::create('grosir_pembeli', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nama_toko');
      $table->string('nama_pemilik');
      $table->string('alamat')->nullable();
      $table->string('telepon')->nullable();
      $table->string('hp')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('grosir_pembeli');
  }
}
