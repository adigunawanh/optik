<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSuratJalanDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('surat_jalan_detail', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('id_surat');
        $table->integer('id_stock');
        $table->integer('jumlah');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('surat_jalan_detail');
    }
}
