<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductStockBranch extends Model
{
  protected $table = 'product_stock_branch';
}
