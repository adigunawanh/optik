<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GrosirPembeli;
use App\GrosirSurat;
use App\GrosirSuratDetail;
use App\GrosirReturn;
use App\GrosirReturnDetail;
use Session;
use PDF;
use Helper;

class GrosirController extends Controller
{
  public function index(){
    $Grosir_surat = GrosirSurat::orderBy('id', 'desc')->paginate(10);
    $data = [
      'grosir_surat' => $Grosir_surat
    ];
    return view('grosir.index', $data);
  }

  public function create(){
    $data = [
      'pembeli' => GrosirPembeli::get(),
    ];
    return view('grosir.create', $data);
  }

  public function store(Request $request){

    $GrosirSurat = new GrosirSurat;
    $GrosirSurat->nomor_surat = $request->nomor_surat;
    $GrosirSurat->id_pembeli = $request->id_pembeli;
    $GrosirSurat->nama_pembeli = $request->nama_pembeli;
    $GrosirSurat->hp = $request->hp;
    $GrosirSurat->total_bayar = 0;
    $GrosirSurat->save();

    $total_bayar  = 0 ;
    foreach($request->id_stock as $index => $value){
      $GrosirSuratDetail = new GrosirSuratDetail;
      $GrosirSuratDetail->id_surat = $GrosirSurat->id;
      $GrosirSuratDetail->id_stock = $value;
      $GrosirSuratDetail->harga_jual = $request->price[$index];
      $GrosirSuratDetail->jumlah = $request->jumlah[$index];
      $GrosirSuratDetail->save();

      $total_bayar += $request->price[$index] * $request->jumlah[$index];
    }

    $GrosirSuratUpdate = GrosirSurat::where('id', $GrosirSurat->id)->first();
    $GrosirSuratUpdate->total_bayar = $total_bayar;
    $GrosirSuratUpdate->save();

    return redirect('grosir');
  }

  public function detail($id_surat){
    $GrosirSuratDetail = GrosirSuratDetail::where('id_surat', $id_surat)
      ->join('product_stock','product_stock.id','=','grosir_surat_detail.id_stock')
      ->join('products','products.id','=','product_stock.product_id')
      ->get();
    $data = [
      'grosir_surat_detail' => $GrosirSuratDetail,
    ];
    return view('grosir.detail', $data);
  }

  public function return($id_surat){
    $grosir_return = GrosirReturn::where('nomor_surat', $id_surat)->first();
    if($grosir_return){
      $grosir_return_detail = GrosirReturnDetail::where('id_return', $grosir_return->id)
        ->join('product_stock','product_stock.id','=','grosir_return_detail.id_stock')
        ->join('products','products.id','=','product_stock.product_id')
        ->get();
      $data = [
        'return' => $grosir_return,
        'return_detail' => $grosir_return_detail,
      ];
      return view('grosir.return_info', $data);
    }else{
      $grosir_surat_detail = GrosirSuratDetail::where('id_surat', $id_surat)
        ->join('product_stock','product_stock.id','=','grosir_surat_detail.id_stock')
        ->join('products','products.id','=','product_stock.product_id')
        ->get();
      $data = [
        'grosir_surat_detail' => $grosir_surat_detail,
      ];
      return view('grosir.return', $data);
    }
  }

  public function return_store($id, Request $request){
    $GrosirReturn = new GrosirReturn;
    $GrosirReturn->nama_peretur = $request->nama_peretur;
    $GrosirReturn->nomor_surat = $id;
    $GrosirReturn->save();

    foreach($request->id_stock as $index => $value){
      $GrosirReturnDetail = new GrosirReturnDetail;
      $GrosirReturnDetail->id_return = $GrosirReturn->id;
      $GrosirReturnDetail->id_stock = $value;
      $GrosirReturnDetail->jumlah = $request->jumlah[$index];
      $GrosirReturnDetail->save();
    }

    Session::flash('success', 'Barang berhasil di return');
    return redirect('grosir');
  }

  public function pembeli(){
    $data = [
      'pembeli' => GrosirPembeli::orderBy('id','desc')->paginate(10),
    ];
    return view('grosir.pembeli', $data);
  }

  public function pembeli_create(){
    return view('grosir.pembeli_create');
  }

  public function pembeli_store(Request $request){
    $GrosirPembeli = new GrosirPembeli;
    $GrosirPembeli->nama_toko = $request->nama_toko;
    $GrosirPembeli->nama_pemilik = $request->nama_pemilik;
    $GrosirPembeli->alamat = $request->alamat;
    $GrosirPembeli->telepon = $request->telepon_pemilik;
    $GrosirPembeli->hp = $request->hp_pemilik;
    $GrosirPembeli->save();

    return redirect('grosir/pembeli');
  }

  public function pembeli_edit($id){
    $data = ['pembeli' => GrosirPembeli::where('id', $id)->first()];
    return view('grosir.pembeli_edit', $data);
  }

  public function pembeli_update($id, Request $request){
    $GrosirPembeli = GrosirPembeli::where('id', $id)->first();
    $GrosirPembeli->nama_toko = $request->nama_toko;
    $GrosirPembeli->nama_pemilik = $request->nama_pemilik;
    $GrosirPembeli->alamat = $request->alamat;
    $GrosirPembeli->telepon = $request->telepon_pemilik;
    $GrosirPembeli->hp = $request->hp_pemilik;
    $GrosirPembeli->save();

    return redirect('grosir/pembeli');
  }

  public function pembeli_destroy($id){
    $GrosirPembeli = GrosirPembeli::where('id', $id)->delete();

    Session::flash('success', 'Delete record success');
    return redirect('grosir/pembeli');
  }

  public function print($id){
    Helper::newLog('go to grosir print'); // create log

    $Grosir = GrosirSurat::select('grosir_pembeli.*', 'grosir_surat.*', 'grosir_surat.created_at as created_at')
      ->join('grosir_pembeli', 'grosir_pembeli.id', '=', 'grosir_surat.id_pembeli')
      ->where('grosir_surat.id', $id)
      ->first();
    $GrosirDetail = GrosirSuratDetail::select('products.*', 'grosir_surat_detail.*')
      ->where('id_surat', $id)
      ->join('product_stock', 'product_stock.id', '=', 'grosir_surat_detail.id_stock')
      ->join('products', 'products.id', '=', 'product_stock.product_id')
      ->get();
    $data = [
      'Grosir' => $Grosir,
      'GrosirDetails' => $GrosirDetail,
    ];

    $html = view('grosir.pdf_information', $data)->render();
    return PDF::load($html, 'A5', 'portrait')->filename('/tmp'.'/'.time().'.pdf')->show();
  }
}
