<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use Helper;
use Excel;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      Helper::newLog('go to supplier list'); // create log

      $data = [
        'supplier' => Supplier::orderBy('id','desc')->paginate(10),
      ];
      return view('supplier.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Helper::newLog('go to supplier create'); // create log
        return view('supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validatedData = $request->validate([
          'name' => 'required|unique:suppliers,name',
          'address' => 'required|unique:suppliers,address',
          'phone' => 'required|unique:suppliers,phone',
      ]);

      Helper::newLog('go to supplier store'); // create log

      $Supplier = new Supplier;
      $Supplier->name = $request->name;
      $Supplier->address = $request->address;
      $Supplier->phone = $request->phone;
      $Supplier->save();

      return redirect('supplier');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Helper::newLog('go to supplier edit'); // create log

        $Supplier = Supplier::where('id', $id)->first();
        $data = ['supplier'=>$Supplier];
        return view('supplier.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      Helper::newLog('go to supplier update'); // create log

      $Supplier = Supplier::where('id', $id)->first();
      $Supplier->name = $request->name;
      $Supplier->address = $request->address;
      $Supplier->phone = $request->phone;
      $Supplier->save();

      return redirect('supplier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Helper::newLog('go to supplier destroy'); // create log
        $Supplier = Supplier::where('id', $id)->delete();
        return redirect('supplier');
    }

    public function excel(){
      Excel::create('Suppliers-' . date("Y-m-d"), function($excel) {
          $excel->sheet('New sheet', function($sheet) {
              $data = [
                'supplier' => Supplier::get(),
              ];
              $sheet->loadView('supplier.report', $data);
          });
      })->download('xls');
    }
}
