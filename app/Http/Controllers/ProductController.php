<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use App\ProductField;
use App\ProductCategoryField;
use App\ProductStock;
use App\ProductStockBranch;
use Illuminate\Http\Request;
use Session;
use DB;
use PDF;
use Helper;
use Excel;

class ProductController extends Controller
{

    public function index()
    {
      Helper::newLog('go to product category list'); // create log
      $Categories = ProductCategory::All();
      $data = [
        'Categories' => $Categories,
      ];
      return view('product.index', $data);
    }

    public function all()
    {
      Helper::newLog('go to product list'); // create log

      $q = \Request::get('q') ? \Request::get('q') : '';
      $Products = Product::select('products.id as id', 'products.product_field_content as product_field_content', 'product_categories.name as category', 'product_categories.id as category_id', 'product_stock.stock', 'product_stock.barcode')
        ->where('products.product_field_content', 'like', '%'.$q.'%')
        ->orWhere('product_stock.barcode', 'like', '%'.$q.'%')
        ->join('product_category_fields','product_category_fields.id','=','products.product_category_field_id')
        ->join('product_stock','product_stock.product_id','=','products.id')
        ->join('product_categories','product_categories.id','=','product_category_fields.product_category_id')->paginate(10);
      $data = [
        'Products' => $Products,
      ];
      return view('product.all', $data);
    }

    public function branch_all()
    {
      Helper::newLog('go to product list'); // create log
      $q = \Request::get('q') ? \Request::get('q') : '';
      $Products = Product::select('products.id as id', 'products.product_field_content as product_field_content', 'product_categories.name as category', 'product_categories.id as category_id', 'product_stock_branch.stock', 'product_stock_branch.branch_id', 'product_stock_branch.selling_price as branch_selling_price', 'branches.name as branch_name', 'product_stock.barcode')
        ->where('products.product_field_content', 'like', '%'.$q.'%')
        ->orWhere('product_stock.barcode', 'like', '%'.$q.'%')
        ->join('product_category_fields','product_category_fields.id','=','products.product_category_field_id')
        ->join('product_stock','product_stock.product_id','=','products.id')
        ->join('product_stock_branch','product_stock.id','=','product_stock_branch.product_stock_id')
        ->join('branches','branches.id','=','product_stock_branch.branch_id')
        ->join('product_categories','product_categories.id','=','product_category_fields.product_category_id')->paginate(10);
      $data = [
        'Products' => $Products,
      ];
      return view('product.branch_all', $data);
    }

    public function search(Request $request)
    {
      Helper::newLog('go to product list'); // create log
      $Products = Product::select('products.id as id', 'products.product_field_content as product_field_content', 'product_categories.name as category', 'product_categories.id as category_id', 'product_stock.stock', 'product_stock.barcode')
        ->where('products.product_field_content', 'like', '%'.$request->q.'%')
        ->orWhere('product_stock.barcode', 'like', '%'.$request->q.'%')
        ->join('product_category_fields','product_category_fields.id','=','products.product_category_field_id')
        ->join('product_stock','product_stock.product_id','=','products.id')
        ->join('product_categories','product_categories.id','=','product_category_fields.product_category_id')
        ->paginate(10);
      $data = [
        'Products' => $Products,
      ];
      // dd($data);
      return view('product.all', $data);
    }

    public function getByCategory($product_category_id)
    {
      Helper::newLog('go to product category list'); // create log

      $ProductCategoryField = ProductCategoryField::select('id','product_field_id')->where('product_category_id', $product_category_id)->first();
      $product_fields = explode(',', $ProductCategoryField->product_field_id);
      foreach($product_fields as $product_field){
        $product_field_id[] = $product_field;
      }
      $product_field_id_ordered = implode(',', $product_field_id); // order by fieldcategory
      $ProductField = ProductField::whereIn('id', $product_field_id)->orderByRaw(DB::raw("FIELD(id, $product_field_id_ordered)"))->get();
      $Products = Product::select('products.id as id', 'products.product_field_content as product_field_content', 'products.supplier', 'product_stock.stock', 'product_stock.barcode')
        ->where('products.product_category_field_id', $ProductCategoryField->id)
        ->join('product_stock','product_stock.product_id','=','products.id')
        ->get();
      $data = [
        'ProductFields' => $ProductField,
        'Products' => $Products,
        'Category' => ProductCategory::where('id', $product_category_id)->first(),
      ];
      return view('product.category', $data);
    }

    public function detail($id, $product_category_id)
    {
      Helper::newLog('go to product category list'); // create log

      $ProductCategoryField = ProductCategoryField::select('id','product_field_id')->where('product_category_id', $product_category_id)->first();
      $product_fields = explode(',', $ProductCategoryField->product_field_id);
      foreach($product_fields as $product_field){
        $product_field_id[] = $product_field;
      }
      $product_field_id_ordered = implode(',', $product_field_id); // order by fieldcategory
      $ProductField = ProductField::whereIn('id', $product_field_id)->orderByRaw(DB::raw("FIELD(id, $product_field_id_ordered)"))->get();
      $Products = Product::select('products.*', 'product_stock.*', 'products.id as id')
                  ->where('product_stock.product_id', $id)
                  ->where(['products.product_category_field_id' => $ProductCategoryField->id, 'products.id' => $id])
                  ->join('product_stock','product_stock.product_id','=','products.id')
                  ->get();
      $data = [
        'ProductFields' => $ProductField,
        'Products' => $Products,
        'Category' => ProductCategory::where('id', $product_category_id)->first(),
      ];
      return view('product.detail', $data);
    }

    public function detail_branch($id, $product_category_id, $branch_id)
    {
      Helper::newLog('go to product category list'); // create log

      $ProductCategoryField = ProductCategoryField::select('id','product_field_id')->where('product_category_id', $product_category_id)->first();
      $product_fields = explode(',', $ProductCategoryField->product_field_id);
      foreach($product_fields as $product_field){
        $product_field_id[] = $product_field;
      }
      $product_field_id_ordered = implode(',', $product_field_id); // order by fieldcategory
      $ProductField = ProductField::whereIn('id', $product_field_id)->orderByRaw(DB::raw("FIELD(id, $product_field_id_ordered)"))->get();
      $Products = Product::select('products.id as id', 'products.product_field_content as product_field_content', 'products.supplier as supplier', 'product_stock.stock', 'product_stock.barcode', 'product_stock_branch.selling_price as branch_selling_price')
                  ->where(['products.product_category_field_id' => $ProductCategoryField->id, 'products.id' => $id])
                  ->where('branch_id', $branch_id)
                  ->join('product_stock','product_stock.product_id','=','products.id')
                  ->join('product_stock_branch','product_stock.id','=','product_stock_branch.product_stock_id')
                  ->get();
      $data = [
        'ProductFields' => $ProductField,
        'Products' => $Products,
        'Category' => ProductCategory::where('id', $product_category_id)->first(),
      ];
      return view('product.detail_branch', $data);
    }

    public function create($product_category_id)
    {
      Helper::newLog('go to product create'); // create log

      $ProductCategoryField = ProductCategoryField::select('id','product_field_id')->where('product_category_id', $product_category_id)->first();
      $product_fields = explode(',', $ProductCategoryField->product_field_id);
      foreach($product_fields as $product_field){
        $product_field_id[] = $product_field;
      }
      $product_field_id_ordered = implode(',', $product_field_id); // order by fieldcategory
      $ProductField = ProductField::whereIn('id', $product_field_id)->orderByRaw(DB::raw("FIELD(id, $product_field_id_ordered)"))->get();
      $data = [
        'ProductFields' => $ProductField,
      ];
      return view('product.create', $data);
    }

    public function store(Request $request)
    {
      Helper::newLog('go to product stored'); // create log

      $ProductCategoryField = ProductCategoryField::where('product_category_id', $request->product_category_id)->first();
      $ProductContents = explode(',', $ProductCategoryField->product_field_id);
      $RequestContent = [];
      foreach($ProductContents as $ProductContent){
        $RequestContent[] = $request->get('field_'.$ProductContent);
      }
      $RequestContent = implode(",", $RequestContent);

      $Product = new Product;
      $Product->product_category_field_id = $ProductCategoryField->id;
      $Product->product_field_content = $RequestContent;
      $Product->supplier = $request->get('supplier');
      $Product->save();

      $ProductStock = new ProductStock;
      $ProductStock->product_id = $Product->id;
      $ProductStock->stock = $request->get('field_2');
      $ProductStock->barcode = rand(100000,999999);
      $ProductStock->save();

      Session::flash('success', 'Create record success');
      return redirect('product/category/'.$request->product_category_id);
    }

    public function show(Product $product)
    {
      $data = [];
      return view('product.show', $data);
    }


    public function edit($id)
    {
      Helper::newLog('go to product edit'); // create log

      $Product = Product::select('products.id as id','products.product_category_field_id','products.product_field_content','product_category_fields.product_category_id','product_stock.stock', 'products.supplier')
        ->where('products.id', $id)
        ->join('product_category_fields','product_category_fields.id','=','products.product_category_field_id')
        ->join('product_stock','product_stock.product_id','=','products.id')
        ->first();
      $ProductCategoryField = ProductCategoryField::select('id','product_field_id')->where('product_category_id', $Product->product_category_id)->first();
      $product_fields = explode(',', $ProductCategoryField->product_field_id);
      foreach($product_fields as $product_field){
        $product_field_id[] = $product_field;
      }
      $product_field_id_ordered = implode(',', $product_field_id); // order by fieldcategory
      $ProductFields = ProductField::whereIn('id', $product_field_id)->orderByRaw(DB::raw("FIELD(id, $product_field_id_ordered)"))->get();

      $product_contents = explode(',', $Product->product_field_content);
      $ProductFieldContents = [];
      $i = 0;
      foreach($ProductFields as $ProductField){
        array_push($ProductFieldContents, [
          'field_id' => $ProductField->id,
          'field_name' => $ProductField->name,
          'field_content' => $product_contents[$i],
        ]);
        $i++;
      }

      $data = [
        'ProductFieldContents' => $ProductFieldContents,
        'Product' => $Product,
      ];

      return view('product.edit', $data);
    }

    public function update(Request $request)
    {
      Helper::newLog('go to product updated'); // create log

      $Product = Product::where('id', $request->id)->first();

      $ProductCategoryField = ProductCategoryField::where('id', $Product->product_category_field_id)->first();
      $ProductContents = explode(',', $ProductCategoryField->product_field_id);
      $RequestContent = [];
      foreach($ProductContents as $ProductContent){
        $RequestContent[] = $request->get('field_'.$ProductContent);
      }
      $RequestContent = implode(",", $RequestContent);

      $Product->product_field_content = $RequestContent;
      $Product->supplier = $request->get('supplier');
      $Product->save();

      $ProductStock = ProductStock::where('product_id', $request->id)->first();
      $ProductStock->stock = $request->stock;
      $ProductStock->save();

      Session::flash('success', 'Create record success');
      return redirect('product/category/'.$ProductCategoryField->product_category_id);
    }

    public function destroy($id)
    {
      Helper::newLog('go to deleted product'); // create log

      $Product = Product::where('id', $id)->delete();
      Session::flash('success', 'Delete record success');
      return redirect()->back();
    }

    public function print($id, $stock){
      Helper::newLog('go to print product'); // create log

      $Product = Product::select('products.*','product_category_fields.product_category_id as product_category_id')
        ->where('products.id', $id)
        ->join('product_category_fields','product_category_fields.id','=','products.product_category_field_id')
        ->first();

      $ProductStock = DB::table('product_stock')->where('product_id', $id)->first();

      $data = [
        'Product' => $Product,
        'stock' => $stock,
        'ProductStock' => $ProductStock,
      ];

      return view('product.pdf_barcode', $data);
      // $html = view('product.pdf_barcode', $data)->render();
      // dd($html);
      // return PDF::load($html, 'A5', 'portrait')->filename('/tmp'.'/barcode-'.date('His').'.pdf')->show();
    }

    public function print_custom(Request $request){
      Helper::newLog('go to print custom product'); // create log

      $data_products = [];
      foreach($request->jumlah as $index => $value){
        $Product = Product::select('products.*', 'products.id as id', 'product_category_fields.product_category_id as product_category_id')
          ->where('products.id', $request->product_id[$index])
          ->join('product_category_fields','product_category_fields.id','=','products.product_category_field_id')
          ->first();
        $ProductStock = DB::table('product_stock')->where('product_id', $request->product_id[$index])->first();

        array_push($data_products, [
          'Product' => $Product,
          'ProductStock' => $ProductStock,
          'jumlah' => $value,
        ]);
      }

      $data = [
        'data_products' => $data_products,
      ];

      // dd($data);

      return view('product.pdf_barcode_custom', $data);
      // $html = view('product.pdf_barcode', $data)->render();
      // dd($html);
      // return PDF::load($html, 'A5', 'portrait')->filename('/tmp'.'/barcode-'.date('His').'.pdf')->show();
    }

    public function scane_barcode(Request $request){
      $Product = Product::select('products.product_field_content','products.id as id','products.product_category_field_id','product_stock.id as product_stock_id', 'product_stock.stock as stock')
        ->where('product_stock.barcode', $request->barcode)
        ->join('product_stock','product_stock.product_id','=','products.id')
        ->first();

      $ProductName = explode(',',$Product->product_field_content)[0];

      // jika produk accessories yg product_category_field_id = 5 | 6
      if($Product->product_category_field_id == 5 || $Product->product_category_field_id == 6){
        $ProductPrice = explode(',',$Product->product_field_content)[3];
      }else{
        $ProductPrice = explode(',',$Product->product_field_content)[3];
      }
      // jika produk accessories yg product_category_field_id = 5 | 6

      $ProductStockId = $Product->product_stock_id;
      $ProductStock = $Product->stock;

      $response = [
        'ProductId' => $Product->id,
        'ProductName' => $ProductName,
        'ProductCategoryFieldId' => $Product->product_category_field_id,
        'ProductPrice' => $ProductPrice,
        'ProductStockId' => $ProductStockId,
        'ProductStock' => $ProductStock,
      ];
      return response($response);
    }

    public function scane_barcode_branch(Request $request){
      $Product = Product::select('products.product_field_content','products.id','product_stock.id as product_stock_id')
        ->where('product_stock.barcode', $request->barcode)
        ->join('product_stock','product_stock.product_id','=','products.id')
        ->first();

      $ProductStockBranch = ProductStockBranch::where(['branch_id' => $request->branch_id, 'product_stock_id' => $Product->product_stock_id]);

      $ProductName = explode(',',$Product->product_field_content)[0];

      // jika produk accessories yg product_category_field_id = 5 | 6
      if($Product->product_category_field_id == 5 || $Product->product_category_field_id == 6){
        $ProductPrice = explode(',',$Product->product_field_content)[3];
      }else{
        $ProductPrice = explode(',',$Product->product_field_content)[3];
      }
      // jika produk accessories yg product_category_field_id = 5 | 6

      $ProductStockId = $Product->product_stock_id;

      $response = [
        'ProductId' => $Product->id,
        'ProductName' => $ProductName,
        'ProductCategoryFieldId' => $Product->product_category_field_id,
        'ProductPrice' => $ProductStockBranch->first()->selling_price,
        'ProductStockId' => $ProductStockId,
        'ProductStockBranch' => $ProductStockBranch->count(),
        'ProductStock' => $ProductStockBranch->first()->stock,
      ];
      return response($response);
    }

    public function excel(){
      Excel::create('Products-' . date("Y-m-d"), function($excel) {
          $excel->sheet('New sheet', function($sheet) {
              $Products = Product::select('products.id as id', 'products.product_field_content as product_field_content', 'product_categories.name as category', 'product_stock.stock', 'products.supplier')
                ->join('product_category_fields','product_category_fields.id','=','products.product_category_field_id')
                ->join('product_stock','product_stock.product_id','=','products.id')
                ->join('product_categories','product_categories.id','=','product_category_fields.product_category_id')
                ->get();
              $data = [
                'Products' => $Products,
              ];
              $sheet->loadView('product.report', $data);
          });
      })->download('xls');
    }

}
