<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rekamedis;
use App\Customer;
use App\Member;
use Auth;
use Helper;

class RekamedisController extends Controller
{
    public function index(){
      Helper::newLog('go to rekamedis list'); // create log

      $data = [
        'rekamedis' => Rekamedis::paginate(15),
      ];
      return view('rekamedis.index', $data);
    }

    public function search(Request $request){
      Helper::newLog('go to rekamedis list'); // create log
      $q = $request->get('q');
      $qStatus = $request->get('qStatus') == '0-1' ? explode('-', $request->get('qStatus')) : [$request->get('qStatus')];
      $Rekamedis = Rekamedis::select('*')
        ->whereIn('status', $qStatus)
        ->when($q, function($query) use ($q, $request){
          if($q){
            if($request->get('qStatus') == '1'){
              $query->where('members.name', 'like', '%'.$q.'%');
            }elseif($request->get('qStatus') == '0'){
              $query->where('customers.name', 'like', '%'.$q.'%');
            }else{
              $query->where('members.name', 'like', '%'.$q.'%');
              $query->orWhere('customers.name', 'like', '%'.$q.'%');
            }
            $query->orWhere('od_sphiris', '=', $q);
            $query->orWhere('os_sphiris', '=', $q);
            $query->orWhere('od_cylindris', '=', $q);
            $query->orWhere('os_cylindris', '=', $q);
            $query->orWhere('od_axis', '=', $q);
            $query->orWhere('os_axis', '=', $q);
            $query->orWhere('addition', '=', $q);
            $query->orWhere('add_kiri', '=', $q);
            $query->orWhere('add_kanan', '=', $q);
          }
        })
        ->leftJoin('members', 'members.member_id', '=', 'rekamedis.id_pasien')
        ->leftJoin('customers', 'customers.id', '=', 'rekamedis.id_pasien')
        ->paginate(15);

      $data = [
        'rekamedis' => $Rekamedis,
      ];
      return view('rekamedis.index', $data);
    }

    public function create(){
      return view('rekamedis.create');
    }

    public function store(Request $request){
      Helper::newLog('go to rekamedis stored'); // create log

      $Rekamedis = new Rekamedis;

      if($request->id_pasien == ""){
        $Customer = new Customer;
        $Customer->nik = $request->nik;
        $Customer->name = $request->name;
        $Customer->tempat_lahir = $request->tempat_lahir;
        $Customer->tanggal_lahir = $request->tanggal_lahir;
        $Customer->usia = $request->usia;
        $Customer->jenis_kelamin = $request->jenis_kelamin;
        $Customer->telepon = $request->telepon;
        $Customer->email = $request->email;
        $Customer->alamat = $request->alamat;
        $Customer->save();

        $Rekamedis->id_pasien = $Customer->id;
      }else{
        $Rekamedis->id_pasien = $request->id_pasien;
      }

      $Rekamedis->status = $request->status;
      $Rekamedis->od_sphiris = $request->od_sphiris;
      $Rekamedis->os_sphiris = $request->os_sphiris;
      $Rekamedis->od_cylindris = $request->od_cylindris;
      $Rekamedis->os_cylindris = $request->os_cylindris;
      $Rekamedis->od_axis = $request->od_axis;
      $Rekamedis->os_axis = $request->os_axis;
      $Rekamedis->addition = $request->addition;
      $Rekamedis->add_kiri = $request->add_kiri;
      $Rekamedis->add_kanan = $request->add_kanan;
      $Rekamedis->notes = $request->notes;
      $Rekamedis->od_av = $request->od_av;
      $Rekamedis->os_av = $request->os_av;
      $Rekamedis->pd = $request->pd;
      $Rekamedis->pd_mm = $request->pd_mm;
      $Rekamedis->created_by = Auth::user()->id;
      $Rekamedis->save();

      return response($Rekamedis);
      // return redirect('transaction/create/'. $Rekamedis->id);
    }

    public function member($member_id){
      $data = [];
      $Member = Member::where('member_id', $member_id)->first();
      $Rekamedis = Rekamedis::where('id_pasien', $member_id)->orderBy('id', 'desc')->first();
      if($Rekamedis){
        array_push($data, $Member); // array on response [0]
        array_push($data, $Rekamedis); // array on response [1]
      }else{
        array_push($data, $Member); // array on response [0]
      }
      return response($data);
    }
}
