<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Transaction;
use App\Branch;
use App\Pegawai;
use Helper;
use DB;
use Excel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      Helper::newLog('go to home page');

      $branch_id = Helper::getBranch()->id;

      $product_branch = DB::table('products')
        ->join('product_stock','product_stock.product_id','=','products.id')
        ->join('product_stock_branch','product_stock.id','=','product_stock_branch.product_stock_id')
        ->where('product_stock_branch.branch_id', $branch_id)
        ->get();

      $transactions = Transaction::select('transactions.*', 'branches.name')
        ->where('transaction_status', '1')
        ->orderBy('transactions.id', 'desc')
        ->join('branch_users','branch_users.user_id','=','transactions.created_by')
        ->join('branches', 'branches.id' ,'=', 'branch_users.branch_id');

      $transaction_branch = Transaction::select('transactions.*', 'branches.name')
        ->where('transaction_status', '1')
        ->orderBy('transactions.id', 'desc')
        ->where('branches.id', $branch_id)
        ->join('branch_users','branch_users.user_id','=','transactions.created_by')
        ->join('branches', 'branches.id' ,'=', 'branch_users.branch_id')->count();

      $branch = Branch::count();

      $pegawai = Pegawai::count();

      $data = [
        'product' => count(Product::get()),
        'product_branch' => count($product_branch),
        'transaction' => $transactions->count(),
        'transaction_branch' => $transaction_branch,
        'branch' => $branch,
        'pegawai' => $pegawai,
      ];
      return view('home.index', $data);
    }

    public function log(){
      Helper::newLog('go to log page'); // create log
      $data = [
        'logs' => DB::table('logs')->select('logs.*', 'users.name')->join('users','users.id','=','logs.user_id')->orderBy('id', 'desc')->paginate(15),
      ];
      return view('home.log', $data);
    }

    public function log_excel(){
      Excel::create('Log-' . date("Y-m-d"), function($excel) {
          $excel->sheet('New sheet', function($sheet) {
              $logs = DB::table('logs')->select('logs.*', 'users.name')->join('users','users.id','=','logs.user_id')->orderBy('id', 'desc');
              $data = [
                'logs' => $logs->get(),
              ];
              $sheet->loadView('home.log_excel', $data);
          });
      })->download('xls');
    }
}
