<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SuratJalan;
use App\SuratJalanDetail;
use App\Product;
use App\ProductStock;
use App\ProductStockBranch;
use Helper;
use Excel;
use PDF;

class SuratJalanController extends Controller
{
  public function index(){
    Helper::newLog('go to surat jalan'); // create log

    $SuratJalan = SuratJalan::select('surat_jalan.id as id','surat_jalan.tanggal_pengiriman','surat_jalan.tanggal_terima','branches.name as cabang', 'pegawai.nama as nama_penerima')
      ->join('branches','branches.id','=','surat_jalan.branch_id')
      ->leftJoin('pegawai','pegawai.nip','=','surat_jalan.nip_penerima')
      ->orderBy('surat_jalan.id','desc');
    $data = [
      'surat_jalan' => $SuratJalan->paginate(10),
    ];
    return view('surat_jalan.index', $data);
  }

  public function create(){
    Helper::newLog('go to create surat jalan page'); // create log

    $Products = Product::select('products.product_field_content','product_stock.id as id_stock')
      ->join('product_stock','product_stock.product_id','=','products.id');
    $data = [
      'Products' => $Products->get()
    ];
    return view('surat_jalan.create', $data);
  }

  public function store($branch_id, Request $request){
    Helper::newLog('go to stored surat jalan'); // create log

    $SuratJalan = new SuratJalan;
    $SuratJalan->branch_id = $branch_id;
    $SuratJalan->tanggal_pengiriman = date("Y-m-d H:i:s");
    $SuratJalan->save();

    foreach($request->id_stock as $index => $value){
      if($value != ""){
        $SuratJalanDetail = new SuratJalanDetail;
        $SuratJalanDetail->id_surat = $SuratJalan->id;
        $SuratJalanDetail->id_stock = $value;
        $SuratJalanDetail->jumlah = $request->jumlah[$index];
        $SuratJalanDetail->save();
      }
    }

    return redirect('surat-jalan');
  }

  public function detail($id){
    $SuratJalanDetail = SuratJalanDetail::where('id_surat', $id)
      ->join('product_stock','product_stock.id','=','surat_jalan_detail.id_stock')
      ->join('products','products.id','=','product_stock.product_id')
      ->get();
    $data = [
      'surat_jalan_detail' => $SuratJalanDetail
    ];
    return view('surat_jalan.detail', $data);
  }

  public function branch($branch_id){
    $SuratJalan = SuratJalan::select('surat_jalan.id as id','surat_jalan.tanggal_pengiriman','branches.name as cabang')
      ->where('branch_id', $branch_id)
      ->join('branches','branches.id','=','surat_jalan.branch_id')
      ->orderBy('surat_jalan.id','desc');
    $data = [
      'surat_jalan' => $SuratJalan->get(),
    ];
    return view('surat_jalan.branch', $data);
  }

  public function edit($id){
    $data = [
      'surat_jalan' => SuratJalan::where('id', $id)->first(),
    ];
    return view('surat_jalan.edit', $data);
  }

  public function update($id, Request $request){
    $SuratJalan = SuratJalan::where('id', $id)->first() ;
    $SuratJalan->tanggal_terima = $request->tanggal_terima;
    $SuratJalan->nip_penerima = $request->nip_penerima;
    $SuratJalan->save();

    $SuratJalanDetails = SuratJalanDetail::where('id_surat', $id)->get();

    foreach($SuratJalanDetails as $data){
      $SuratJalanDetail = SuratJalanDetail::where('id', $data->id)->first();

      // get data product
      $Product = Product::select('products.product_field_content','products.id','products.product_category_field_id','product_stock.id as product_stock_id')
        ->where('product_stock.id', $SuratJalanDetail->id_stock)
        ->join('product_stock','product_stock.product_id','=','products.id')
        ->first();
        // jika produk accessories yg product_category_field_id = 5 | 6
        // if($Product->product_category_field_id == 5 || $Product->product_category_field_id == 6){
        //   $ProductPrice = explode(',',$Product->product_field_content)[5];
        // }else{
        //   $ProductPrice = explode(',',$Product->product_field_content)[3];
        // }
        $ProductPrice = explode(',',$Product->product_field_content)[3];
        // jika produk accessories yg product_category_field_id = 5 | 6
      // get data product

      // update stok pusat
      $ProductStock = ProductStock::where('id', $SuratJalanDetail->id_stock)->first();
      $ProductStock->stock = $ProductStock->stock - $SuratJalanDetail->jumlah;
      $ProductStock->save();
      // update stok pusat

      // update stok branch
      $ProductStockBranch = ProductStockBranch::where(['product_stock_id' => $SuratJalanDetail->id_stock, 'branch_id' => $SuratJalan->branch_id])->first();
      if($ProductStockBranch){
        $ProductStockBranch->stock = $ProductStockBranch->stock + $SuratJalanDetail->jumlah;
        $ProductStockBranch->save();
      }else{
        $ProductStockBranch = new ProductStockBranch;
        $ProductStockBranch->branch_id = $SuratJalan->branch_id;
        $ProductStockBranch->product_stock_id = $SuratJalanDetail->id_stock;
        $ProductStockBranch->stock = $SuratJalanDetail->jumlah;
        $ProductStockBranch->selling_price = $ProductPrice;
        $ProductStockBranch->save();
      }
      // update stok branch
    }

    return redirect('surat-jalan');
  }

  public function excel($id){
    Excel::create('SuratJalan-' . date("Y-m-d"), function($excel) use($id) {
        $excel->sheet('New sheet', function($sheet) use($id) {

            $SuratJalanDetail = SuratJalanDetail::where('id_surat', $id)
              ->join('product_stock','product_stock.id','=','surat_jalan_detail.id_stock')
              ->join('products','products.id','=','product_stock.product_id')
              ->get();
            $data = [
              'surat_jalan_detail' => $SuratJalanDetail
            ];
            $sheet->loadView('surat_jalan.report', $data);
        });
    })->download('xls');
  }

  public function pdf($id){
    Helper::newLog('go to report surat jalan'); // create log

    $SuratJalan = SuratJalan::select('surat_jalan.id as id','surat_jalan.tanggal_pengiriman','surat_jalan.tanggal_terima','branches.name as cabang', 'branches.address as branch_address', 'pegawai.nama as nama_penerima')
      ->where('surat_jalan.id', $id)
      ->join('branches','branches.id','=','surat_jalan.branch_id')
      ->leftJoin('pegawai','pegawai.nip','=','surat_jalan.nip_penerima')
      ->orderBy('surat_jalan.id','desc')->first();

    $SuratJalanDetail = SuratJalanDetail::where('id_surat', $id)
      ->join('product_stock','product_stock.id','=','surat_jalan_detail.id_stock')
      ->join('products','products.id','=','product_stock.product_id')
      ->get();
    $data = [
      'surat_jalan_detail' => $SuratJalanDetail,
      'surat_jalan' => $SuratJalan
    ];

    $html = view('surat_jalan.report_pdf', $data)->render();
    return PDF::load($html, 'A5', 'portrait')->filename('/tmp'.'/surat-jalan-'.date("Y-M-d").'.pdf')->show();
  }

}
