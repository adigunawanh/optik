<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use DB;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(Request $request)
    {
       $credentials = array(
          'email' => $request->get('email'),
          'password' => $request->get('password'),
        );

        if (Auth::attempt($credentials))
        {
          if(Auth::user()->status == '1'){
            Session::flash('already', '1');
            Auth::logout();
            return redirect()->intended('/login');
          }else{
            DB::table('users')->where('id', Auth::user()->id)->update(['status'=>'1']);
            return redirect()->intended('/home');
          }
        }
        else
        {
          Session::flash('failed', '1');
          return redirect()->intended('/login');
        }
    }

    public function logout(Request $request){
      DB::table('users')->where('id', Auth::user()->id)->update(['status'=>'0']);
      $this->performLogout($request);
      return redirect('login');
    }
}
