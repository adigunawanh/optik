<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\TransactionDetail;
use App\ProductStock;
use App\ProductStockBranch;
use App\Branch;
use App\Member;
use DB;
use PDF;
use Auth;
use Session;
use Helper;
use Excel;

class TransactionController extends Controller
{
    public function index(){
      Helper::newLog('go to transaction list'); // create log

      $transactions = Transaction::select('transactions.*', 'branches.name')
        ->where('transaction_status', '1')
        ->orderBy('transactions.id', 'desc')
        ->join('branch_users','branch_users.user_id','=','transactions.created_by')
        ->join('branches', 'branches.id' ,'=', 'branch_users.branch_id');
      $data = [
        'transactions' => $transactions->paginate(10),
      ];
      return view('transaction.index', $data);
    }

    public function search(Request $request){
      Helper::newLog('go to transaction search'); // create log

      $transactions = Transaction::where('transaction_status', '1')
        ->where('barcode', 'like', '%' . $request->q . '%')
        ->join('branch_users','branch_users.user_id','=','transactions.created_by')
        ->join('branches', 'branches.id' ,'=', 'branch_users.branch_id');
      $data = [
        'transactions' => $transactions->paginate(10),
      ];
      return view('transaction.index', $data);
    }

    public function branch($id){
      Helper::newLog('go to transaction list'); // create log

      $transactions = Transaction::select('transactions.*', 'branches.name')
        ->where('transaction_status', '1')
        ->orderBy('transactions.id', 'desc')
        ->join('branch_users','branch_users.user_id','=','transactions.created_by')
        ->join('branches', 'branches.id' ,'=', 'branch_users.branch_id')
        ->where('branches.id', $id);
      $data = [
        'transactions' => $transactions->paginate(10),
      ];
      return view('transaction.index', $data);
    }

    public function create(){

      $data = [];
      return view('transaction.create', $data);
    }

    public function store(Request $request){
      Helper::newLog('new transaction'); // create log

      $invoice = "ZLRS" . rand(12345,99999);

      $total = 0;
      foreach($request->product as $index => $value){
        if($value){
          $TransactionDetail = new TransactionDetail;
          $TransactionDetail->invoice = $invoice;
          $TransactionDetail->product_id = $request->product_id[$index];
          $TransactionDetail->product = $value;
          $TransactionDetail->price = $request->price[$index];
          $TransactionDetail->qty = $request->qty[$index];
          $TransactionDetail->discount = $request->discount[$index];
          $TransactionDetail->ukuran_lensa = $request->ukuran_lensa[$index];
          $TransactionDetail->warna_lensa = $request->warna_lensa[$index];
          $TransactionDetail->save();
          $price_discount = ($request->discount[$index]);
          $total += ($request->price[$index] - $price_discount)  * $request->qty[$index];

          //pengurangan stock
          $ProductStock = ProductStock::where('product_id', $request->product_id[$index])->first();
          $final_stock = $ProductStock->stock - $request->qty[$index];
          $ProductStock->stock = $final_stock;
          $ProductStock->save();
          //pengurangan stock

        }

      }

      $Transaction = new Transaction;
      $Transaction->invoice = $invoice;
      $Transaction->barcode = rand(100000,999999);
      $Transaction->total = $total;
      $Transaction->id_rekamedis = $request->id_rekamedis;
      $Transaction->created_by = Auth::user()->id;
      $Transaction->sales = $request->sales;
      $Transaction->opticien = $request->opticien;
      $Transaction->member = $request->member;
      $Transaction->save();

      // penambahan point member
      if($request->member){
        $member = Member::where('member_id', $request->member)->first();
        $member->point = $member->point + 1;
        $member->save();
      }
      // penambahan point member

      return redirect('transaction/success/'. $invoice);
    }

    public function store_branch($branch_id, Request $request){
      Helper::newLog('new transaction branch'); // create log

      $invoice = "ZLRS" . rand(12345,99999);

      $total = 0;
      foreach($request->product as $index => $value){
        if($value){
          $TransactionDetail = new TransactionDetail;
          $TransactionDetail->invoice = $invoice;
          $TransactionDetail->product_id = $request->product_id[$index];
          $TransactionDetail->product = $value;
          $TransactionDetail->price = $request->price[$index];
          $TransactionDetail->qty = $request->qty[$index];
          $TransactionDetail->discount = $request->discount[$index];
          $TransactionDetail->ukuran_lensa = $request->ukuran_lensa[$index];
          $TransactionDetail->warna_lensa = $request->warna_lensa[$index];
          $TransactionDetail->save();
          $price_discount = ($request->discount[$index]);
          $total += ($request->price[$index] - $price_discount)  * $request->qty[$index];

          //pengurangan stock branch
          $ProductStock = ProductStock::where('product_id', $request->product_id[$index])->first();
          $ProductStockBranch = ProductStockBranch::where(['product_stock_id' => $ProductStock->id, 'branch_id' => $branch_id])->first();

          $final_stock = $ProductStockBranch->stock - $request->qty[$index];
          $ProductStockBranch->stock = $final_stock;
          $ProductStockBranch->save();
          //pengurangan stock branch

        }

      }

      $Transaction = new Transaction;
      $Transaction->invoice = $invoice;
      $Transaction->barcode = rand(100000,999999);
      $Transaction->total = $total;
      $Transaction->id_rekamedis = $request->id_rekamedis;
      $Transaction->created_by = Auth::user()->id;
      $Transaction->sales = $request->sales;
      $Transaction->opticien = $request->opticien;
      $Transaction->member = $request->member;
      $Transaction->save();

      // penambahan point member
      if($request->member){
        $member = Member::where('member_id', $request->member)->first();
        $member->point = $member->point + 1;
        $member->save();
      }
      // penambahan point member

      return redirect('transaction/success/'. $invoice);
    }

    public function detail($invoice){
      Helper::newLog('go to transaction detail'); // create log

      $Transaction = Transaction::select('transactions.*', 'transactions.created_at as created_at', 'users.name')
        ->where('invoice', $invoice)
        ->join('users', 'users.id', '=', 'transactions.created_by')->first();

      $TransactionDetail = TransactionDetail::where('invoice', $invoice)
        ->join('products', 'products.id', '=', 'transactions_details.product_id');

      $data = [
        'Transaction' => $Transaction,
        'TransactionDetails' => $TransactionDetail->get(),
        'product_category_field_id' => $TransactionDetail->first()->product_category_field_id,
        'Rekamedis' => DB::table('rekamedis')->where('id', $Transaction->id_rekamedis)->first(),
      ];
      return view('transaction.detail', $data);
    }

    public function success($invoice){
      $Transaction = Transaction::select('transactions.*', 'transactions.created_at as created_at', 'users.name')
        ->where('invoice', $invoice)
        ->join('users', 'users.id', '=', 'transactions.created_by')->first();

        $TransactionDetail = TransactionDetail::where('invoice', $invoice)
          ->join('products', 'products.id', '=', 'transactions_details.product_id');

      $data = [
        'Transaction' => $Transaction,
        'TransactionDetails' => $TransactionDetail->get(),
        'product_category_field_id' => $TransactionDetail->first()->product_category_field_id,
        'Rekamedis' => DB::table('rekamedis')->where('id', $Transaction->id_rekamedis)->first(),
      ];
      return view('transaction.success', $data);
    }

    public function print($invoice){
      Helper::newLog('go to transaction print'); // create log

      $Transaction = Transaction::where('invoice', $invoice)->first();
      $data = [
        'Transaction' => $Transaction,
        'TransactionDetails' => TransactionDetail::where('invoice', $invoice)->get(),
        'Rekamedis' => DB::table('rekamedis')->where('id', $Transaction->id_rekamedis)->first(),
      ];

      $html = view('transaction.pdf_information', $data)->render();
      return PDF::load($html, 'A5', 'portrait')->filename('/tmp'.'/'.$invoice.'.pdf')->show(false, true, false);
    }

    public function print_nota_process($invoice){
      Helper::newLog('go to transaction print'); // create log

      $Transaction = Transaction::where('invoice', $invoice)->first();
      $data = [
        'Transaction' => $Transaction,
        'TransactionDetails' => TransactionDetail::where('invoice', $invoice)->get(),
        'Rekamedis' => DB::table('rekamedis')->where('id', $Transaction->id_rekamedis)->first(),
      ];

      $html = view('transaction.pdf_information_nota_process', $data)->render();
      return PDF::load($html, 'A5', 'portrait')->filename('/tmp'.'/'.$invoice.'.pdf')->show(false, true, false);
    }

    public function print_nota_process_softlense($invoice){
      Helper::newLog('go to transaction print'); // create log

      $Transaction = Transaction::where('invoice', $invoice)->first();
      $data = [
        'Transaction' => $Transaction,
        'TransactionDetails' => TransactionDetail::where('invoice', $invoice)->get(),
        'Rekamedis' => DB::table('rekamedis')->where('id', $Transaction->id_rekamedis)->first(),
      ];

      $html = view('transaction.pdf_nota_process_softlense', $data)->render();
      return PDF::load($html, 'A5', 'portrait')->filename('/tmp'.'/'.$invoice.'.pdf')->show(false, true, false);
    }

    public function print_nota_process_frame($invoice){
      Helper::newLog('go to transaction print'); // create log

      $Transaction = Transaction::where('invoice', $invoice)->first();
      $data = [
        'Transaction' => $Transaction,
        'TransactionDetails' => TransactionDetail::where('invoice', $invoice)->get(),
        'Rekamedis' => DB::table('rekamedis')->where('id', $Transaction->id_rekamedis)->first(),
      ];

      // return view('transaction.pdf_nota_process_frame', $data);

      $html = view('transaction.pdf_nota_process_frame', $data)->render();
      return PDF::load($html, 'A5', 'landscape')->filename('/tmp'.'/'.$invoice.'.pdf')->show(false, true, false);
    }

    public function print_kwitansi($invoice){
      Helper::newLog('go to transaction print kwitansi'); // create log

      $Transaction = Transaction::where('invoice', $invoice)->first();
      $data = [
        'Transaction' => $Transaction,
        'TransactionDetails' => TransactionDetail::where('invoice', $invoice)->get(),
      ];

      // return view('transaction.pdf_nota_process_frame', $data);

      $html = view('transaction.pdf_kwitansi', $data)->render();
      return PDF::load($html, 'A5', 'landscape')->filename('/tmp'.'/'.$invoice.'.pdf')->show(false, true, false);
    }

    public function excel(){
      Excel::create('Transactions-' . date("Y-m-d"), function($excel) {
          $excel->sheet('New sheet', function($sheet) {
              $transactions = Transaction::select('transactions.*', 'branches.name', 'transactions_details.product', 'transactions_details.price', 'transactions_details.qty')
                ->where('transaction_status', '1')
                ->join('branch_users','branch_users.user_id','=','transactions.created_by')
                ->join('branches', 'branches.id' ,'=', 'branch_users.branch_id')
                ->join('transactions_details', 'transactions_details.invoice' ,'=', 'transactions.invoice')
                ->orderBy('transactions.id', 'desc');
              $data = [
                'transactions' => $transactions->get(),
              ];
              $sheet->loadView('transaction.report', $data);
          });
      })->download('xls');
    }

    public function excel_branch($id){
      Helper::newLog('go to report transaction list branch'); // create log

      Excel::create('Transactions-' . date("Y-m-d"), function($excel) use($id) {
          $excel->sheet('New sheet', function($sheet) use($id) {
            $transactions = Transaction::select('transactions.*', 'branches.name', 'transactions_details.product', 'transactions_details.price', 'transactions_details.qty')
              ->where('transaction_status', '1')
              ->orderBy('transactions.id', 'desc')
              ->join('branch_users','branch_users.user_id','=','transactions.created_by')
              ->join('branches', 'branches.id' ,'=', 'branch_users.branch_id')
              ->join('transactions_details', 'transactions_details.invoice' ,'=', 'transactions.invoice')
              ->orderBy('transactions.id', 'desc')
              ->where('branches.id', $id);
            $data = [
              'transactions' => $transactions->paginate(10),
            ];
              $sheet->loadView('transaction.report', $data);
          });
      })->download('xls');
    }

    public function buy($invoice, Request $request){
      $Transaction = Transaction::where('invoice', $invoice)->first();
      $Transaction->status = $request->status;
      $Transaction->dp = $request->dp;
      $Transaction->tanggal_selesai = $request->tanggal_selesai;
      $Transaction->save();

      Session::flash('success', 'Pembayaran Berhasil');
      return redirect('transaction/success/'. $invoice . '?buy=true');
    }

    public function destroy($invoice){
      $Transaction = Transaction::where('invoice', $invoice)->first();
      $TransactionDetails = TransactionDetail::where('invoice', $invoice)->get();
      foreach ($TransactionDetails as $TransactionDetail) {
        $ProductStock = ProductStock::where('product_id', $TransactionDetail->product_id)->first();
        $ProductStock->stock = $ProductStock->stock + $TransactionDetail->qty;
        $ProductStock->save();
      }
      $Transaction->transaction_status = '0';
      $Transaction->save();

      return redirect()->back();
    }

    public function complete($invoice){
      $Transaction = Transaction::where('invoice', $invoice)->first();
      $Transaction->status = '1';
      $Transaction->save();

      Session::flash('success', 'Transaksi Lunas');
      return redirect('transaction');
    }

    public function reporting(){
      $branch = \Request::get('branch');
      $date_start = \Request::get('date_start') ? \Request::get('date_start') : date("Y-m-d") ;
      $date_end = \Request::get('date_end') ? \Request::get('date_end') : date("Y-m-d");

      // jika download
      if(\Request::get('download')){
        Excel::create('Transactions-' . date("Y-m-d"), function($excel) use($branch, $date_start, $date_end) {
            $excel->sheet('New sheet', function($sheet) use($branch, $date_start, $date_end) {
                $transactions = Transaction::select('transactions.*', 'branches.name', 'transactions_details.product', 'transactions_details.price', 'transactions_details.qty')
                  ->where('transaction_status', '1')
                  ->where('branches.id', 'like', '%'.$branch.'%')
                  ->whereDate('transactions.created_at', '>=', $date_start)
                  ->whereDate('transactions.created_at', '<=', $date_end)
                  ->join('branch_users','branch_users.user_id','=','transactions.created_by')
                  ->join('branches', 'branches.id' ,'=', 'branch_users.branch_id')
                  ->join('transactions_details', 'transactions_details.invoice' ,'=', 'transactions.invoice')
                  ->orderBy('transactions.id', 'desc');
                $data = [
                  'transactions' => $transactions->get(),
                ];
                $sheet->loadView('transaction.report', $data);
            });
        })->download('xls');
      }

      $transactions = Transaction::select('transactions.*', 'branches.name', 'transactions_details.product', 'transactions_details.price', 'transactions_details.qty')
        ->where('transaction_status', '1')
        ->where('branches.id', 'like', '%'.$branch.'%')
        ->whereDate('transactions.created_at', '>=', $date_start)
        ->whereDate('transactions.created_at', '<=', $date_end)
        ->join('branch_users','branch_users.user_id','=','transactions.created_by')
        ->join('branches', 'branches.id' ,'=', 'branch_users.branch_id')
        ->join('transactions_details', 'transactions_details.invoice' ,'=', 'transactions.invoice')
        ->orderBy('transactions.id', 'desc');
      $data = [
        'branches' => Branch::get(),
        'transactions' => $transactions->get(),
      ];
      return view('transaction.reporting', $data);
    }

    public function closing_branch($branch_id){
      Helper::newLog('go to report transaction today'); // create log

      $transactions = Transaction::select('transactions.*', 'branches.name', 'transactions_details.product', 'transactions_details.price', 'transactions_details.qty')
        ->where('transaction_status', '1')
        ->where('branches.id', $branch_id)
        ->whereDate('transactions.created_at', '>=', date("Y-m-d"))
        ->whereDate('transactions.created_at', '<=', date("Y-m-d"))
        ->join('branch_users','branch_users.user_id','=','transactions.created_by')
        ->join('branches', 'branches.id' ,'=', 'branch_users.branch_id')
        ->join('transactions_details', 'transactions_details.invoice' ,'=', 'transactions.invoice')
        ->orderBy('transactions.id', 'desc');
      $data = [
        'transactions' => $transactions->get(),
      ];

      $html = view('transaction.pdf_closing_branch', $data)->render();
      return PDF::load($html, 'A5', 'landscape')->filename('/tmp'.'/closing-'.date("Y-M-d").'.pdf')->show(false, true, false);
    }
}
