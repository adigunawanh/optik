<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use Session;
use DB;

class ProductCategoryController extends Controller
{

  public function create(){
    $data = [
      'method' => 'post',
      'url' => '/product-category/store',
      'id' => old('id'),
      'name' => old('name'),
      'barcode_type' => old('barcode_type'),
      'product_fields' => DB::table('product_fields')->get(),
    ];
    return view('product_category.form', $data);
  }

  public function store(Request $request){
    $ProductCategory = new ProductCategory;
    $ProductCategory->name = $request->name;
    $ProductCategory->barcode_type = $request->barcode_type;
    $ProductCategory->save();

    $ProductCategoryField = DB::table('product_category_fields')
      ->insert([
        'product_category_id' => $ProductCategory->id,
        'product_field_id' => implode(',', $request->product_field_id),
      ]);

    Session::flash('success', 'Create record success');
    return redirect('product');
  }

  public function edit($id){
    $ProductCategory = ProductCategory::where('id', $id)->first();
    $data = [
      'method' => 'post',
      'url' => '/product-category/update/'.$id,
      'id' => old('id', $id),
      'name' => old('name', $ProductCategory->name),
      'barcode_type' => old('barcode_type', $ProductCategory->barcode_type),
      'product_fields' => DB::table('product_fields')->get(),
    ];
    return view('product_category.form', $data);
  }

  public function update($id, Request $request){
    $ProductCategory = ProductCategory::where('id', $id)->first();
    $ProductCategory->name = $request->name;
    $ProductCategory->barcode_type = $request->barcode_type;
    $ProductCategory->save();

    $ProductCategoryField = DB::table('product_category_fields')
      ->where('product_category_id', $id)
      ->update([
        'product_field_id' => implode(',', $request->product_field_id),
      ]);

    Session::flash('success', 'Update record success');
    return redirect('product');
  }

  public function destroy($id){
    $ProductCategory = ProductCategory::where('id', $id)->delete();

    Session::flash('success', 'Delete record success');
    return redirect('product');
  }

}
