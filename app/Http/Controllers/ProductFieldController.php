<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductField;

class ProductFieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = [
        'fields' => ProductField::paginate(15),
      ];
      return view('product_field.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data = [
        'method' => 'post',
        'url' => '/product-field/store',
        'id' => old('id'),
        'name' => old('name'),
      ];
      return view('product_field.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $ProductField = new ProductField;
      $ProductField->name = $request->name;
      $ProductField->save();

      return redirect('product-field');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $product_field = ProductField::where('id', $id)->first();
      $data = [
        'method' => 'post',
        'url' => '/product-field/update/'.$id,
        'id' => old('id', $id),
        'name' => old('name', $product_field->name),
      ];
      return view('product_field.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $ProductField = ProductField::where('id', $id)->first();
      $ProductField->name = $request->name;
      $ProductField->save();

      return redirect('product-field');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $ProductField = ProductField::where('id', $id)->delete();
      return redirect('product-field');
    }
}
