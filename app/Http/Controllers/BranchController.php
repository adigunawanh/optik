<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;
use App\BranchUser;
use App\User;
use App\Product;
use App\ProductCategory;
use App\ProductField;
use App\ProductCategoryField;
use App\ProductStock;
use DB;
use Helper;
use Session;
use Excel;

class BranchController extends Controller
{
  public function index(){
    Helper::newLog('go to branch page'); // create log
    $data = [
      'branches' => Branch::get(),
    ];

    return view('branch.index', $data);
  }

  public function category($branch_id)
  {
    $Categories = ProductCategory::All();
    $data = [
      'Categories' => $Categories,
      'branch_id' => $branch_id,
      'branch' => Branch::where('id', $branch_id)->first(),
    ];
    return view('branch.category', $data);
  }

  public function product($product_category_id, $branch_id){
    $ProductCategoryField = ProductCategoryField::select('id','product_field_id')->where('product_category_id', $product_category_id)->first();
    $product_fields = explode(',', $ProductCategoryField->product_field_id);
    foreach($product_fields as $product_field){
      $product_field_id[] = $product_field;
    }
    $product_field_id_ordered = implode(',', $product_field_id); // order by fieldcategory
    $ProductField = ProductField::whereIn('id', $product_field_id)->orderByRaw(DB::raw("FIELD(id, $product_field_id_ordered)"))->get();
    $Products = Product::select('products.*', 'product_stock.barcode', 'products.id as id', 'products.product_category_field_id', 'product_stock_branch.stock as stock')
      ->where('products.product_category_field_id', $ProductCategoryField->id)
      ->where('branch_id', $branch_id)
      ->join('product_stock','product_stock.product_id','=','products.id')
      ->join('product_stock_branch','product_stock.id','=','product_stock_branch.product_stock_id')
      ->get();
    $data = [
      'ProductFields' => $ProductField,
      'Products' => $Products,
    ];
    return view('branch.product', $data);
  }

  public function create(){
    Helper::newLog('go to create branch'); // create log
    return view('branch.create');
  }

  public function list_user($branch_id){
    $data = [
      'users' => DB::table('branch_users')->where('branch_id', $branch_id)->join('users','users.id','=','branch_users.user_id')->get(),
    ];
    return view('branch.list_user', $data);
  }

  public function add_user(){
    return view('branch.add_user');
  }

  public function store(Request $request){

    $validatedData = $request->validate([
        'email' => 'required|unique:users,email',
        'password' => 'required|min:6',
        'name' => 'required|unique:branches,name',
        'address' => 'required|unique:branches,address',
    ]);

    Helper::newLog('go to stored branch'); // create log

    $User = new User;
    $User->name = $request->name;
    $User->email = $request->email;
    $User->password = bcrypt($request->password);
    $User->save();

    $Branch = new Branch;
    $Branch->name = $request->name;
    $Branch->address = $request->address;
    $Branch->phone = $request->phone;
    $Branch->status = $request->status;
    $Branch->frame_description = $request->frame_description;
    $Branch->note_description = $request->note_description;
    $Branch->point = 0;
    $Branch->save();

    $BranchUser = new BranchUser;
    $BranchUser->branch_id = $Branch->id;
    $BranchUser->user_id = $User->id;
    $BranchUser->save();

    Session::flash('success', 'Create record success');
    return redirect('branch');
  }

  public function edit($id){
    Helper::newLog('go to edit branch'); // create log
    $data = [
      'Branch' => Branch::where('id', $id)->first()
    ];
    return view('branch.edit', $data);
  }

  public function update($id, Request $request){
    Helper::newLog('go to updated branch'); // create log

    $Branch = Branch::where('id', $id)->first();
    $Branch->name = $request->name;
    $Branch->address = $request->address;
    $Branch->phone = $request->phone;
    $Branch->frame_description = $request->frame_description;
    $Branch->note_description = $request->note_description;
    $Branch->save();

    Session::flash('success', 'Update record success');
    return redirect('branch');
  }

  public function destroy($id){
    $BranchUser = BranchUser::where('branch_id', $id)->first();
    $User = User::where('id', $BranchUser->user_id)->delete();
    $BranchUser->delete();
    $Branch = Branch::where('id', $id)->delete();

    Session::flash('success', 'Delete record success');
    return redirect('branch');
  }

  public function add_user_store($branch_id, Request $request){
    Helper::newLog('go to stored branch user'); // create log

    $User = new User;
    $User->name = $request->name;
    $User->email = $request->email;
    $User->level = $request->level;
    $User->password = bcrypt($request->password);
    $User->save();

    $BranchUser = new BranchUser;
    $BranchUser->branch_id = $branch_id;
    $BranchUser->user_id = $User->id;
    $BranchUser->save();

    Session::flash('success', 'Create user success');
    return redirect('branch');
  }

  public function edit_user($branch_id, $id){
    $data = [
      'user' => User::where('id', $id)->first()
    ];
    return view('branch.edit_user', $data);
  }

  public function edit_user_update($branch_id, $id, Request $request){
    Helper::newLog('go to edit branch user'); // create log

    $User = User::where('id', $id)->first();
    $User->name = $request->name;
    $User->email = $request->email;
    $User->level = $request->level;
    // $User->password = bcrypt($request->password);
    $User->save();

    Session::flash('success', 'Edit user success');
    return redirect('branch/list-user/'.$branch_id);
  }

  public function delete_user($id){
    $User = User::where('id', $id)->delete();

    Session::flash('success', 'Delete user success');
    return redirect()->back();
  }

  public function excel(){
    Excel::create('Branches-' . date("Y-m-d"), function($excel) {
        $excel->sheet('New sheet', function($sheet) {
            $data = [
              'branches' => Branch::get(),
            ];
            $sheet->loadView('branch.report', $data);
        });
    })->download('xls');
  }

  public function list_user_excel($branch_id){
    Excel::create('Branches-List-User' . date("Y-m-d"), function($excel) use($branch_id) {
        $excel->sheet('New sheet', function($sheet) use($branch_id){
            $data = [
              'users' => DB::table('branch_users')->where('branch_id', $branch_id)->join('users','users.id','=','branch_users.user_id')->get(),
            ];
            $sheet->loadView('branch.report_list_user', $data);
        });
    })->download('xls');
  }
}
