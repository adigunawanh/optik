<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use Helper;
use Excel;

class PegawaiController extends Controller
{
  public function index(){
    Helper::newLog('go to pegawai list'); // create log

    $data = [
      'pegawai' => Pegawai::paginate(10),
    ];
    return view('pegawai.index', $data);
  }

  public function create(){
    Helper::newLog('go to pegawai create'); // create log
    return view('pegawai.create');
  }

  public function store(Request $request){
    $validatedData = $request->validate([
        'nip' => 'required|unique:pegawai,nip',
    ]);

    Helper::newLog('go to pegawai store'); // create log

    $Pegawai = new Pegawai;
    $Pegawai->nip = $request->nip;
    $Pegawai->nama = $request->nama;
    $Pegawai->jabatan = $request->jabatan;
    $Pegawai->created_by = '1';
    $Pegawai->save();

    return redirect('pegawai');
  }

  public function edit($id){
    Helper::newLog('go to pegawai edit'); // create log
    $Pegawai = Pegawai::where('id', $id)->first();
    $data = [
      'Pegawai' => $Pegawai,
    ];
    return view('pegawai.edit', $data);
  }

  public function update(Request $request, $id){
    Helper::newLog('go to pegawai update'); // create log

    $Pegawai = Pegawai::where('id', $id)->first();
    $Pegawai->nip = $request->nip;
    $Pegawai->nama = $request->nama;
    $Pegawai->jabatan = $request->jabatan;
    $Pegawai->save();

    return redirect('pegawai');
  }

  public function destroy($id){
    $Pegawai = Pegawai::where('id', $id)->delete();
    return redirect('pegawai');
  }

  public function excel(){
    Excel::create('Pegawai-' . date("Y-m-d"), function($excel) {
        $excel->sheet('New sheet', function($sheet) {
            $data = [
              'pegawai' => Pegawai::get(),
            ];
            $sheet->loadView('pegawai.report', $data);
        });
    })->download('xls');
  }
}
