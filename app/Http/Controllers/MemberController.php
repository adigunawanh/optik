<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\Rekamedis;
use Helper;
use Session;

class MemberController extends Controller
{
  public function index(){
    Helper::newLog('go to member list'); // create log

    $data = [
      'members' => Member::paginate(10),
    ];
    return view('member.index', $data);
  }

  public function create(){
    Helper::newLog('go to member create'); // create log
    return view('member.create');
  }

  public function store(Request $request){
    Helper::newLog('go to member store'); // create log

    $validatedData = $request->validate([
        'member_id' => 'required|unique:members|max:255',
    ]);

    $Member = new Member;
    $Member->member_id = $request->member_id;
    $Member->nik = $request->nik;
    $Member->name = $request->name;
    $Member->tempat_lahir = $request->tempat_lahir;
    $Member->tanggal_lahir = $request->tanggal_lahir;
    $Member->usia = $request->usia;
    $Member->jenis_kelamin = $request->jenis_kelamin;
    $Member->telepon = $request->telepon;
    $Member->email = $request->email;
    $Member->alamat = $request->alamat;
    $Member->point = 0;
    $Member->save();

    return redirect('member');
  }

  public function show($id){
    $Member = Member::where('id', $id)->first();
    $Rekamedis = Rekamedis::where('id_pasien', $Member->member_id)->orderBy('id', 'desc')->first();
    $data = [
      'member' => $Member,
      'rekamedis' => $Rekamedis,
    ];
    return view('member.show', $data);
  }

  public function edit($id){
    Helper::newLog('go to member create'); // create log
    $data = ['member' => Member::where('id', $id)->first()];
    return view('member.edit', $data);
  }

  public function update($id, Request $request){
    Helper::newLog('go to member store'); // create log

    $Member = Member::where('id', $id)->first();
    $Member->member_id = $request->member_id;
    $Member->nik = $request->nik;
    $Member->name = $request->name;
    $Member->tempat_lahir = $request->tempat_lahir;
    $Member->tanggal_lahir = $request->tanggal_lahir;
    $Member->usia = $request->usia;
    $Member->jenis_kelamin = $request->jenis_kelamin;
    $Member->telepon = $request->telepon;
    $Member->email = $request->email;
    $Member->alamat = $request->alamat;
    $Member->point = $request->point;
    $Member->save();

    return redirect('member');
  }

  public function destroy($id){
    $Member = Member::where('id', $id)->delete();

    Session::flash('success', 'Delete record success');
    return redirect('member');
  }
}
