<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use Helper;

class CustomerController extends Controller
{

  public function index(){
    Helper::newLog('go to customer list'); // create log

    $data = [
      'Customers' => Customer::get(),
    ];
    return view('customer.index', $data);
  }

  public function store(Request $request){
    Helper::newLog('stored new customer'); // create log

    $Customer = new Customer;
    $Customer->nik = $request->nik;
    $Customer->name = $request->name;
    $Customer->tempat_lahir = $request->tempat_lahir;
    $Customer->tanggal_lahir = $request->tanggal_lahir;
    $Customer->jenis_kelamin = $request->jenis_kelamin;
    $Customer->telepon = $request->telepon;
    $Customer->email = $request->email;
    $Customer->alamat = $request->alamat;
    $Customer->save();

    return response($Customer);
  }
}
