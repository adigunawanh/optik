<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrosirReturnDetail extends Model
{
  public $table = 'grosir_return_detail';
}
