<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Auth;

class Helper{

  public static function getBranch(){
    $Branch = DB::table('branches')
      ->select('branches.id as id', 'branches.status as status', 'branches.name as name', 'branches.*')
      ->where('branch_users.user_id', Auth::user()->id)
      ->join('branch_users', 'branch_users.branch_id', '=', 'branches.id')
      ->first();
    return $Branch;
  }

  public static function getBranchById($id){
    $Branch = DB::table('branches')
      ->select('branches.id as id', 'branches.status as status', 'branches.name as name', 'branches.*')
      ->where('branches.id', $id)
      ->join('branch_users', 'branch_users.branch_id', '=', 'branches.id')
      ->first();
    return $Branch;
  }

  public static function ProductName($product_field_content){
    $product = explode(',', $product_field_content);
    return $product[0];
  }

  public static function pasien($status, $id){
    if($status == 1){
      $pasien = DB::table('members')->where('member_id', $id)->first();
    }else{
      $pasien = DB::table('customers')->where('id', $id)->first();
    }
    return $pasien;
  }

  public static function newLog($activity){
    return $log = DB::table('logs')->insert([
      'user_id' => Auth::user()->id,
      'activity' => $activity,
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s'),
    ]);
  }

  public static function pegawai($id){
    return $Pegawai = DB::table('pegawai')->where('id', $id)->first();
  }

  public static function user($id){
    return $User = DB::table('users')->where('id', $id)->first();
  }

  public static function rupiah($price){
    return number_format($price, 0 , '.', '.');
  }

}
