@extends('layouts.app')
@section('title', 'Form Produk Kategori')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Produk Kategori</h3>
      </div>
      <div class="panel-body">
        <form action="{{$url}}" method="{{$method}}">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Nama Kategori</label>
            <input type="text" name="name" value="{{$name}}" required class="form-control">
          </div>
          <div class="form-group">
            <label for="">Tipe Barcode</label>
            <select class="form-control" name="barcode_type">
              <option {{$barcode_type == "C39" ? 'selected' : ''}} value="C39">C39 - (Kecil)</option>
              <option {{$barcode_type == "QRCODE" ? 'selected' : ''}} value="QRCODE">QRCODE - (Besar)</option>
            </select>
          </div>
          <div class="form-group">
            <label for="">Product Fields</label> <br>
            @php
              $product_field_id = [];
              $product_category_fields = DB::table('product_category_fields')->where('product_category_id', $id)->first();
              if($product_category_fields){
                $product_field_id = explode(',', $product_category_fields->product_field_id);
              }
            @endphp
            @foreach($product_fields as $product_field)
              <input name="product_field_id[]" type="checkbox" {{in_array($product_field->id, $product_field_id) ? 'checked' : ''}} value="{{$product_field->id}}"> {{$product_field->name}} <br>
            @endforeach
          </div>
          <input type="submit" class="btn btn-primary" value="Simpan">
          <a href="#" onclick="goBack()" class="btn btn-default">Back</a>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
