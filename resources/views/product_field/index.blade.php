@extends('layouts.app')
@section('title', 'Produk Field')

@section('content')
  <div class="container">
    <div class="well well-sm">
      <a href="/product-field/create" class="btn btn-default">Buat Field</a>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">List Produk Field</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>Name</th>
              <th>Pilihan</th>
            </tr>
            @foreach ($fields as $data)
              <tr>
                <td>{{$data->name}}</td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Option
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <li><a href="/product-field/edit/{{$data->id}}">Edit</a></li>
                      <li><a onclick="return confirm('Yakin?')" href="/product-field/delete/{{$data->id}}">Hapus</a></li>
                    </ul>
                  </div>
                </td>
              </tr>
            @endforeach
          </table>
          {{$fields->render()}}
        </div>
      </div>
    </div>
  </div>
@endsection
