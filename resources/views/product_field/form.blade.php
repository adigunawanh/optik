@extends('layouts.app')
@section('title', 'Form Produk Field')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Produk Field</h3>
      </div>
      <div class="panel-body">
        <form action="{{$url}}" method="{{$method}}">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Nama Field</label>
            <input type="text" name="name" value="{{$name}}" required class="form-control">
          </div>
          <input type="submit" class="btn btn-primary" value="Simpan">
          <a href="#" onclick="goBack()" class="btn btn-default">Batal</a>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
