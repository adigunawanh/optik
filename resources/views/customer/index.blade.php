@extends('layouts.app')
@section('title', 'List Customers')

@section('content')
  <div class="container">
    {{-- <div class="well well-sm">
      <a href="/customer/create" class="btn btn-default">Create Customer</a>
    </div> --}}
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">List Customer</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>Customer ID</th>
              <th>NIK</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>No HP</th>
            </tr>
            @foreach ($Customers as $Customer)
              <tr>
                <td>{{$Customer->id}}</td>
                <td>{{$Customer->nik}}</td>
                <td>{{$Customer->name}}</td>
                <td>{{$Customer->alamat}}</td>
                <td>{{$Customer->telepon}}</td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
