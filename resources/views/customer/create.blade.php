@extends('layouts.app')
@section('title', 'Form Customer')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Customer</h3>
      </div>
      <div class="panel-body">
        <form action="/Customer/store" method="post">
          {{csrf_field()}}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Customer ID</label>
                <input type="text" name="Customer_id" value="" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Nik</label>
                <input type="number" name="nik" value="" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Nama</label>
                <input type="text" name="name" value="" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Tempat Lahir</label>
                <input type="text" name="tempat_lahir" value="" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Tanggal Lahir</label>
                <input type="date" name="tanggal_lahir" value="" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Jenis Kelamin</label><br>
                <input type="radio" name="jenis_kelamin" value="L"> Pria
                <input type="radio" name="jenis_kelamin" value="P"> Wanita
              </div>
              <div class="form-group">
                <label for="">Telepon</label>
                <input type="number" name="telepon" value="" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Email</label>
                <input type="email" name="email" value="" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Alamat</label>
                <input type="text" name="alamat" value="" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Point</label>
                <input type="number" name="point" value="" class="form-control">
              </div>
            </div>
          </div>
          <input type="submit" class="btn btn-primary" value="Simpan">
        </form>
      </div>
    </div>
  </div>
@endsection
