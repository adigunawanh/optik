@extends('layouts.app')
@section('title', 'Product Category')

@section('content')
  <div class="container">
    <div class="text-center">
      @if(Session::has('success'))
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! Session::get('success') !!}
      </div>
      @elseif(Session::has('error'))
      <div class="alert alert-error alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! Session::get('error') !!}
      </div>
      @endif
    </div>
    <div class="well well-sm">
      <div class="row">
        <div class="col-md-2">
          <a class="btn btn-default" href="/product-category/create">Create Product Category</a>
        </div>
        <div class="col-md-10">
          <form class="" action="/product/search" method="get">
            <div class="form-group">
              <input style="width:30%" type="text" class="form-control" value="{{Request::get('q')}}" name="q" placeholder="Cari produk...">
              @if(Request::get('q'))
                <a href="/product/all">Reset pencarian</a>
              @endif
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Product Category</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-hover">
            <tr>
              <th>Category</th>
              <th>Total Product</th>
              <th>Action</th>
            </tr>
            @foreach($Categories as $Category)
              @php
                $total_product = DB::table('products')->join('product_category_fields','product_category_fields.id','=','products.product_category_field_id')->where('product_category_field_id', $Category->id)->count();
              @endphp
              <tr>
                <td>{{$Category->name}}</td>
                <td>{{$total_product}}</td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Option
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <li><a href="/product/category/{{$Category->id}}">List Product</a></li>
                      <li class="divider"></li>
                      <li><a href="/product/create/{{$Category->id}}">Buat Produk Baru</a></li>
                      <li class="divider"></li>
                      <li><a href="/product-category/edit/{{$Category->id}}">Edit Kategori</a></li>
                      <li class="divider"></li>
                      <li><a onclick="return confirm('Yakin akan menghapus data?')" href="/product-category/delete/{{$Category->id}}">Hapus Kategori</a></li>
                    </ul>
                  </div>
                </td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
