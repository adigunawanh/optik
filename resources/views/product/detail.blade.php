@extends('layouts.app')
@section('title', 'Product')

@section('content')
  <div class="container">
    <div class="text-center">
      @if(Session::has('success'))
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! Session::get('success') !!}
      </div>
      @elseif(Session::has('error'))
      <div class="alert alert-error alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! Session::get('error') !!}
      </div>
      @endif
    </div>
    <div class="well well-sm">
      @if(Helper::getBranch()->status == '1')
        <a href="/product/create/{{Request::segment(4)}}" class="btn btn-default">Create</a>
      @endif

      @if(Auth::user()->level == '2' || Auth::user()->level == '4')
        <a href="#" data-toggle="modal" data-target="#ModalCustomPrint" class="btn btn-danger">Custom Print Barcode</a>
      @endif
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Product / {{$Category->name}}</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-hover">
            <tr>
              <th>ID Produk</th>
              @foreach($ProductFields as $field)
                @if(!Helper::getBranch()->status == '1')
                  @if($field->name != 'Harga Beli' && $field->name != 'Jumlah')
                    <th>{{$field->name}}</th>
                  @endif
                @else
                  <th>{{$field->name}}</th>
                @endif
              @endforeach
                <th>Supplier</th>
                <th>Stock</th>
                <th>Barcode</th>
            </tr>
            @foreach($Products as $Product)
              @php
                $ProductContents = [];
                $ProductContents = explode(',', $Product->product_field_content);
                $Supplier = DB::table('suppliers')->where('id', $Product->supplier)->first();

                if(!Helper::getBranch()->status == '1'){
                  unset($ProductContents[2]); // hide harga beli
                  unset($ProductContents[1]); // hide jumlah
                }
              @endphp
              <tr>
                <td>{{$Product->id}}</td>
                @foreach($ProductContents as $ProductContent)
                  <td>{{$ProductContent}}</td>
                @endforeach
                <td>{{$Supplier->name}}</td>
                <td>{{$Product->stock}}</td>
                <td><img width="80" src="data:image/png;base64,{!!DNS2D::getBarcodePNG($Product->barcode, "QRCODE")!!}" alt="barcode"/> <br> <span class="text-center"> {{$Product->barcode}} </span> </td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
    <div class="well well-sm">
      <a href="#" onclick="goBack()" class="btn btn-default">Back</a>
    </div>
  </div>

  <div class="modal fade" id="ModalCustomPrint" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Print Barcode Product</h4>
        </div>
        <div class="modal-body">
          <form target="_blank" action="/product/print/custom" method="post">
            {{csrf_field()}}
            <table class="table table-hover">
              <tr>
                <th>Product</th>
                <th>Jumlah</th>
              </tr>
              @foreach($Products as $Product)
                @php
                  $ProductContents = [];
                  $ProductContents = explode(',', $Product->product_field_content);
                @endphp
                <tr>
                  <td>{{$ProductContents[0]}}</td>
                  <td width="30%">
                    <input type="hidden" name="product_id[]" value="{{$Product->id}}"/>
                    <input type="number" name="jumlah[]" class="form-control" value="0" min="0"/>
                  </td>
                </tr>
              @endforeach
            </table>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Print</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
