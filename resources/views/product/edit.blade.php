@extends('layouts.app')
@section('title', 'Edit Product')

@section('content')
  <div class="container">
    <div class="well well-sm">
      <a href="#" onclick="goBack()" class="btn btn-default">Back</a>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Edit Product</h3>
      </div>
      <div class="panel-body">
        {{Form::open(['url'=>'/product/update', 'method'=>'post'])}}
        @foreach($ProductFieldContents as $ProductFieldContent)
          @php $FieldValues = DB::table('product_field_values')->where('product_field_id', $ProductFieldContent['field_id']); @endphp

          @if($FieldValues->count() > 0)
            <div class="form-group">
              <label for="">{{$ProductFieldContent['field_name']}}</label>
              <select class="form-control" name="field_{{$ProductFieldContent['field_id']}}" required>
                @foreach($FieldValues->get() as $FieldValue)
                  <option value="{{$FieldValue->value}}">{{$FieldValue->value}}</option>
                @endforeach
              </select>
            </div>
          @else
            <div class="form-group">
              <label for="">{{$ProductFieldContent['field_name']}}</label>
              <input type="text" name="field_{{$ProductFieldContent['field_id']}}" value="{{$ProductFieldContent['field_content']}}" class="form-control" required>
            </div>
          @endif
        @endforeach
        <div class="form-group">
          <label for="">Supplier</label>
          <select class="form-control select2" name="supplier" required>
            @php $suppliers = DB::table('suppliers')->get(); @endphp
            @foreach($suppliers as $supplier)
              <option {{$Product->supplier == $supplier->id ? 'selected' : ''}} value="{{$supplier->id}}">{{$supplier->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="">Stock</label>
          <input type="text" name="stock" value="{{$Product->stock}}" class="form-control" required>
        </div>
        <input type="hidden" name="id" value="{{$Product->id}}">
        <div class="form-group">
          <input type="submit" name="submit" value="Save" class="btn btn-primary">
        </div>
        {{Form::close()}}
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
