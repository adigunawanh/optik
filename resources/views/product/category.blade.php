@extends('layouts.app')
@section('title', 'Product')

@section('content')
  <div class="container">
    <div class="text-center">
      @if(Session::has('success'))
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! Session::get('success') !!}
      </div>
      @elseif(Session::has('error'))
      <div class="alert alert-error alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! Session::get('error') !!}
      </div>
      @endif
    </div>
    <div class="well well-sm">
      <a href="/product/create/{{Request::segment(3)}}" class="btn btn-default">Buat Produk</a>
      @if(Auth::user()->level == '2' || Auth::user()->level == '4')
        <a href="#" data-toggle="modal" data-target="#ModalCustomPrint" class="btn btn-danger">Custom Print Barcode</a>
      @endif
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Product / {{$Category->name}}</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-hover">
            <tr>
              <th>ID Produk</th>
              @foreach($ProductFields as $field)
                <th>{{$field->name}}</th>
              @endforeach
                <th>Supplier</th>
                <th>Stock</th>
                <th>Barcode</th>
                <th>Action</th>
            </tr>
            @foreach($Products as $Product)
              @php
                $ProductContents = [];
                $ProductContents = explode(',', $Product->product_field_content);
                $Supplier = DB::table('suppliers')->where('id', $Product->supplier)->first();
              @endphp

              <tr>
                <td>{{$Product->id}}</td>
                @foreach($ProductContents as $ProductContent)
                  <td>{{$ProductContent}}</td>
                @endforeach
                <td>{{$Supplier->name}}</td>
                <td>{{$Product->stock}}</td>
                <td><img width="80" src="data:image/png;base64,{!!DNS2D::getBarcodePNG($Product->barcode, "QRCODE")!!}" alt="barcode"/></td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Option
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <li><a href="/product/detail/{{$Product->id}}/{{$Category->id}}">Detail Product</a></li>
                      <li><a href="/product/edit/{{$Product->id}}">Edit</a></li>
                      <li class="divider"></li>
                      <li><a onclick="return confirm('Yakin akan menghapus data?')" href="/product/delete/{{$Product->id}}">Hapus</a></li>
                      <li class="divider"></li>
                      <li><a target="_blank" href="/product/print/{{$Product->id}}/{{$Product->stock}}">Print Barcode</a></li>
                    </ul>
                  </div>
                </td>
              </tr>

            @endforeach
          </table>
        </div>
      </div>
    </div>
    <div class="well well-sm">
      <a href="/product" class="btn btn-default">Back</a>
    </div>
  </div>

  <div class="modal fade" id="ModalCustomPrint" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Print Barcode Product</h4>
        </div>
        <div class="modal-body">
          <form target="_blank" action="/product/print/custom" method="post">
            {{csrf_field()}}
            <table class="table table-hover">
              <tr>
                <th>Product</th>
                <th>Jumlah</th>
              </tr>
              @foreach($Products as $Product)
                @php
                  $ProductContents = [];
                  $ProductContents = explode(',', $Product->product_field_content);
                @endphp
                <tr>
                  <td>{{$ProductContents[0]}}</td>
                  <td width="30%">
                    <input type="hidden" name="product_id[]" value="{{$Product->id}}"/>
                    <input type="number" name="jumlah[]" class="form-control" min="0" max="{{$Product->stock}}" value="0"/>
                  </td>
                </tr>
              @endforeach
            </table>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Print</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
