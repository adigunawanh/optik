@extends('layouts.app')
@section('title', 'Create Product')

@push('style')
  <link rel="stylesheet" href="/select2/css/select2.min.css">
@endpush

@section('content')
  <div class="container">
    {{-- <div class="well well-sm">
      <a href="/product/category/{{Request::segment(3)}}" class="btn btn-default">Back</a>
    </div> --}}
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Create Product</h3>
      </div>
      <div class="panel-body">
        {{Form::open(['url'=>'/product/store', 'method'=>'post'])}}
        @foreach($ProductFields as $ProductField)
          @php $FieldValues = DB::table('product_field_values')->where('product_field_id', $ProductField->id); @endphp

          @if($FieldValues->count() > 0)
            <div class="form-group">
              <label for="">{{$ProductField->name}}</label>
              <select class="form-control" name="field_{{$ProductField->id}}" required>
                @foreach($FieldValues->get() as $FieldValue)
                  <option value="{{$FieldValue->value}}">{{$FieldValue->value}}</option>
                @endforeach
              </select>
            </div>
          @else
            <div class="form-group">
              <label for="">{{$ProductField->name}}</label>
              <input type="{{$ProductField->name == 'Jumlah' || $ProductField->name == 'Harga Jual' || $ProductField->name == 'Harga Beli' ? 'number' : 'text' }}" name="field_{{$ProductField->id}}" value="" class="form-control" required>
            </div>
          @endif
        @endforeach
        <div class="form-group">
          <label for="">Supplier</label>
          <select class="form-control select2" name="supplier" required>
            @php $suppliers = DB::table('suppliers')->get(); @endphp
            @foreach($suppliers as $supplier)
              <option value="{{$supplier->id}}">{{$supplier->name}}</option>
            @endforeach
          </select>
        </div>
        {{-- <div class="form-group">
          <label for="">Stock</label>
          <input type="number" name="stock" value="" class="form-control" required>
        </div> --}}
        <input type="hidden" name="product_category_id" value="{{Request::segment(3)}}">
        <div class="form-group">
          <input type="submit" name="submit" value="Simpan" class="btn btn-primary">
          <a href="/product/category/{{Request::segment(3)}}" class="btn btn-default">Batal</a>
        </div>
        {{Form::close()}}
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script type="text/javascript" src="/select2/js/select2.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('.select2').select2();
  });
  </script>
@endpush
