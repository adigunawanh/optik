<table class="table table-hover">
  <tr>
    <th>Kategori</th>
    <th>Nama Produk</th>
    <th>Harga Produk</th>
    <th>Stok</th>
    <th>Suplier</th>
  </tr>
  @foreach($Products as $Product)
    @php
      $ProductContents = explode(',', $Product->product_field_content);
      $Supplier = DB::table('suppliers')->where('id', $Product->supplier)->first();
    @endphp
    <tr>
      <td>{{$Product->category}}</td>
      <td>{{$ProductContents[0]}}</td>
      <td>{{$ProductContents[3]}}</td>
      <td>{{$Product->stock}}</td>
      <td>{{$Supplier->name}}</td>
    </tr>
  @endforeach
</table>
