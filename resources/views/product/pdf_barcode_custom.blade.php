<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PDF Product</title>
    <link rel="stylesheet" href="/css/app.css">
  </head>
  <body style="background:white">
    @foreach($data_products as $data_product)
      @php
        $Product = $data_product['Product'];
        $ProductStock = $data_product['ProductStock'];
        $ProductContents = explode(',', $Product->product_field_content);
        $product_categories = DB::table('product_categories')->where('id', $Product->product_category_id)->first();
      @endphp

      @for($i=1; $i <= $data_product['jumlah']; $i++)
        @if($product_categories->barcode_type == 'C39')
          <div class="col-xs-4" style="padding:20px; height:0.7in; margin-top:1px">
            <div class="row">
              <div class="col-xs-12">
                <center>
                  <img src="data:image/png;base64,{!!DNS1D::getBarcodePNG($ProductStock->barcode, "C39")!!}" style="width:85%;height:15px"/>
                </center>
                <center><span style="font-size:6px;font-weight:bold;">{{$ProductStock->barcode}} - Rp. {{number_format($ProductContents[3], 0 , '.', '.')}}</span></center>
              </div>
            </div>
          </div>
        @else
          <div class="col-xs-6" style="padding:15px; height:1.5in; margin:0">
            <div class="row">
              <div class="col-xs-8">
                <center><img src="data:image/png;base64,{!!DNS2D::getBarcodePNG($ProductStock->barcode, "QRCODE")!!}" style="width:50px; height:50px"/></center>
                <center><span style="font-size:8px;font-weight:bold">{{$ProductStock->barcode}}</span></center>
                <center>
                  <img src="data:image/png;base64,{!!DNS1D::getBarcodePNG($ProductStock->barcode, "C39")!!}" style="width:70%;height:15px"/>
                </center>
              </div>
              <div class="col-xs-4" style="font-size:10px;">
                <div class="row">
                  <div class="text-right">
                    <img style="margin-right:10px" src="/img/zolaris.png" height="15px"><br>
                  </div>
                  {{$ProductContents[0]}}
                  @if($Product->product_category_id == '1')
                    <br>
                    min {{$ProductContents[6]}} - {{$ProductContents[5]}}
                    <br>
                  @elseif($Product->product_category_id == '4')
                    Ukuran : {{$ProductContents[5]}}
                  @endif
                </div>
              </div>
            </div>
          </div>
        @endif
      @endfor

    @endforeach
  </body>
</html>
