@extends('layouts.app')
@section('title', 'Product')

@section('content')
  <div class="container">
    <div class="text-center">
      @if(Session::has('success'))
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! Session::get('success') !!}
      </div>
      @elseif(Session::has('error'))
      <div class="alert alert-error alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! Session::get('error') !!}
      </div>
      @endif
    </div>
    <div class="well well-sm">
      <div class="row">
        <br>
        <div class="col-md-3">
          <form class="" action="" method="get">
            <div class="form-group">
              <input type="text" class="form-control" value="{{Request::get('q')}}" name="q" placeholder="Cari produk...">
              @if(Request::get('q'))
                <a href="{{strtok(Request::url(), '?')}}">Reset pencarian</a>
              @endif
            </div>
          </form>
        </div>
        <div class="col-md-6 text-right">
          @if(Helper::getBranch()->status == '1')
            @if(Auth::user()->level == '2' || Auth::user()->level == '4')
              <a href="#" data-toggle="modal" data-target="#ModalCustomPrint" class="btn btn-danger">Custom Print Barcode</a>
              <a class="btn btn-success" href="/product/excel">Download Report Excel</a>
            @endif
          @endif
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Product</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-hover">
            <tr>
              <th>No</th>
              <th>Nama Produk</th>
              <th>Stock</th>
              <th>Kategori</th>
              <th>Cabang</th>
              <th>Barcode</th>
              <th>Pilihan</th>
            </tr>
            @foreach($Products as $index => $Product)
              @php
                $ProductContents = explode(',', $Product->product_field_content);
              @endphp
              <tr>
                <td>{{$index+1}}</td>
                <td>{{$ProductContents[0]}}</td>
                <td>{{$Product->stock}}</td>
                <td>{{$Product->category}}</td>
                <td>{{$Product->branch_name}}</td>
                <td><img width="80" src="data:image/png;base64,{!!DNS2D::getBarcodePNG($Product->barcode, "QRCODE")!!}" alt="barcode"/> <br> <span class="text-center"> {{$Product->barcode}} </span> </td>
                  <td>
                    <div class="dropdown">
                      <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Option
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        <li><a href="/product/detail-branch/{{$Product->id}}/{{$Product->category_id}}/{{$Product->branch_id}}">Detail Product</a></li>
                        @if(Helper::getBranch()->status == '1')
                          <li><a href="/product/edit/{{$Product->id}}">Edit Product</a></li>
                          <li><a target="_blank" href="/product/print/{{$Product->id}}/{{$Product->stock}}">Print Barcode</a></li>
                        @endif
                      </ul>
                    </div>
                  </td>
              </tr>
            @endforeach
          </table>
          {{$Products->render()}}
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="ModalCustomPrint" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Print Barcode Product</h4>
        </div>
        <div class="modal-body">
          <form target="_blank" action="/product/print/custom" method="post">
            {{csrf_field()}}
            <table class="table table-hover">
              <tr>
                <th>Product</th>
                <th>Jumlah</th>
              </tr>
              @foreach($Products as $Product)
                @php
                  $ProductContents = explode(',', $Product->product_field_content);
                @endphp
                <tr>
                  <td>{{$ProductContents[0]}}</td>
                  <td width="30%">
                    <input type="hidden" name="product_id[]" value="{{$Product->id}}"/>
                    <input type="number" name="jumlah[]" class="form-control" value="{{$Product->stock}}"/>
                  </td>
                </tr>
              @endforeach
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Print</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
