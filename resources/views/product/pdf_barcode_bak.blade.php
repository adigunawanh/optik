<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PDF Product</title>
    <link rel="stylesheet" href="/css/app.css">
  </head>
  <body style="background:white">
    @if($Product->product_category_id == '2')
      <?php
        //Columns must be a factor of 12 (1,2,3,4,6,12)
        $numOfCols = 3;
        $rowCount = 0;
        $bootstrapColWidth = 12 / $numOfCols;
      ?>
      <div class="row">
        @for($i=1; $i <= $ProductStock->stock; $i++)
        <div class="col-xs-3" style="margin: 0px -5px 0px -5px; padding:15px">
          <center><img src="data:image/png;base64,{!!DNS1D::getBarcodePNG($ProductStock->barcode, "C39")!!}" style="width:100%; height:20px"/></center>
          <center><span style="font-size:8px;font-weight:bold">{{$ProductStock->barcode}}</span></center>
          <hr style="margin:0">
          <div class="">
            <div class="col-xs-6" style="padding:0">
              <img src="/img/zolaris.png" height="25"/>
            </div>
            <div class="col-xs-6" style="padding:0">
              <span style="font-size:8px;font-weight:bold;text-align:right" class="text-right">Product</span>
            </div>
          </div>
          <!-- <span style="font-size:5px;font-weight:bold;" class="pull-right">{{$ProductStock->barcode}}</span> -->
        </div>
        <div style="margin-bottom:10px;">

        </div>
        @php
          $rowCount++;
          if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
        @endphp

        @endfor
      </div>

    @else
    <div class="">
        @for($i=1; $i <= $ProductStock->stock; $i++)
          <div class="col-xs-4" style="padding:13px; margin-top:8px;">
            <center><img src="data:image/png;base64,{!!DNS1D::getBarcodePNG($ProductStock->barcode, "C39")!!}" style="width:100%; height:20px"/></center>
            <center><span style="font-size:8px;font-weight:bold">{{$ProductStock->barcode}}-{{$i}}</span></center>
          </div>
        @endfor
    </div>
    @endif
  </body>
</html>
