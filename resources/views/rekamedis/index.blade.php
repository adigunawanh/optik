@extends('layouts.app')
@section('title', 'Form Rekamedis')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Lists Rekamedis</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <form class="form-inline" action="/rekamedis/search" method="get">
              <div class="form-group">
                <select class="form-control" name="qStatus">
                  <option {{Request::get('qStatus') == '' ? 'selected' : ''}} value="0-1">Semua</option>
                  <option {{Request::get('qStatus') == '0' ? 'selected' : ''}} value="0">Customer</option>
                  <option {{Request::get('qStatus') == '1' ? 'selected' : ''}} value="1">Member</option>
                </select>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" value="{{Request::get('q')}}" name="q" placeholder="Cari data rekamedis...">
              </div>
              <input type="submit" class="btn btn-default" value="Cari">
              @if(Request::get('q'))
                <br>
                <a href="/rekamedis">Reset pencarian</a>
              @endif
            </form>
          </div>
        </div>
        <br>
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>Status</th>
              <th>Nama</th>
              <th>OD Sphiris</th>
              <th>OS Sphiris</th>
              <th>OD Cylindris</th>
              <th>OS Cylindris</th>
              <th>OD Axis</th>
              <th>OS Axis</th>
              <th>ADD</th>
              {{-- <th>Add Kiri</th>
              <th>Add Kanan</th> --}}
              <th>OD AV</th>
              <th>OS AV</th>
              <th>PD</th>
              <th>PD MM</th>
              <th>Keterangan</th>
            </tr>
            @foreach($rekamedis as $data)
              @php
                if($data->status == '1'){
                  $profile = DB::table('members')->where('member_id', $data->id_pasien)->first();
                }else{
                  $profile = DB::table('customers')->where('id', $data->id_pasien)->first();
                }
              @endphp
              <tr>
                <td>{{$data->status == '1' ? 'Member' : 'Customer'}}</td>
                <td>{{$profile->name}}</td>
                <td>{{$data->od_sphiris}}</td>
                <td>{{$data->os_sphiris}}</td>
                <td>{{$data->od_cylindris}}</td>
                <td>{{$data->os_cylindris}}</td>
                <td>{{$data->od_axis}}</td>
                <td>{{$data->os_axis}}</td>
                <td>{{$data->addition}}</td>
                {{-- <td>{{$data->add_kiri}}</td>
                <td>{{$data->add_kanan}}</td> --}}
                <td>{{$data->od_av}}</td>
                <td>{{$data->os_av}}</td>
                <td>{{$data->pd}}</td>
                <td>{{$data->pd_mm}}</td>
                <td>{{$data->notes}}</td>
              </tr>
            @endforeach
          </table>
          {{$rekamedis->render()}}
        </div>
      </div>
    </div>
  </div>
@endsection
