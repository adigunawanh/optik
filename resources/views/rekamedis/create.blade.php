@extends('layouts.app')
@section('title', 'Form Rekamedis')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Rekamedis</h3>
      </div>
      <div class="panel-body">
        <form class="" action="/rekamedis/store" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">ID Pasien</label>
            <input type="text" name="id_pasien" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Status</label>
            <input type="text" name="status" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">OD Sphiris</label>
            <input type="text" name="od_sphiris" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">OS Sphiris</label>
            <input type="text" name="os_sphiris" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">OD Cylindris</label>
            <input type="text" name="od_cylindris" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">OS Cylindris</label>
            <input type="text" name="os_cylindris" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">OD Axis</label>
            <input type="text" name="od_axis" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">OS Axis</label>
            <input type="text" name="os_axis" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Addition</label>
            <input type="text" name="addition" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Add Kiri</label>
            <input type="text" name="add_kiri" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Add Kanan</label>
            <input type="text" name="add_kanan" value="" class="form-control">
          </div>
          <input type="submit" value="Simpan" class="btn btn-primary">
        </form>
      </div>
    </div>
  </div>
@endsection
