<table class="table table-bordered">
  <tr>
    <th>NIP</th>
    <th>Nama</th>
    <th>Jabatan</th>
  </tr>
  @foreach ($pegawai as $data)
    <tr>
      <td>{{$data->nip}}</td>
      <td>{{$data->nama}}</td>
      <td>{{$data->jabatan}}</td>
    </tr>
  @endforeach
</table>
