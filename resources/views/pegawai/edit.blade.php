@extends('layouts.app')
@section('title', 'Form Pegawai')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Edit Pegawai</h3>
      </div>
      <div class="panel-body">
        <form action="/pegawai/update/{{$Pegawai->id}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Nip</label>
            <input type="text" name="nip" value="{{$Pegawai->nip}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="nama" value="{{$Pegawai->nama}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Jabatan</label>
            <select class="form-control" name="jabatan">
              <option {{$Pegawai->jabatan == "Kasir" ? 'selected' : ''}} value="Kasir">Kasir</option>
              <option {{$Pegawai->jabatan == "Sales" ? 'selected' : ''}} value="Sales">Sales</option>
              <option {{$Pegawai->jabatan == "Opticien" ? 'selected' : ''}} value="Opticien">Opticien</option>
            </select>
          </div>
          <input type="submit" class="btn btn-primary" value="Simpan">
        </form>
      </div>
    </div>
    <div class="well well-sm">
      <a href="#" onclick="goBack()" class="btn btn-default">Back</a>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
