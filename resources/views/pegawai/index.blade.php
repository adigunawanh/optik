@extends('layouts.app')
@section('title', 'Pegawai')

@section('content')
  <div class="container">
    <div class="well well-sm">
      <a href="/pegawai/create" class="btn btn-default">Create Pegawai</a>
      <a class="btn btn-success" href="/pegawai/excel">Download Report Excel</a>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">List Pegawai</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>No</th>
              <th>NIP</th>
              <th>Nama</th>
              <th>Jabatan</th>
              <th>Pilihan</th>
            </tr>
            @foreach ($pegawai as $index => $data)
              <tr>
                <td>{{$index+1}}</td>
                <td>{{$data->nip}}</td>
                <td>{{$data->nama}}</td>
                <td>{{$data->jabatan}}</td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Option
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <li><a href="/pegawai/edit/{{$data->id}}">Edit</a></li>
                      <li><a onclick="return confirm('Yakin akan hapus data?')" href="/pegawai/delete/{{$data->id}}">Hapus</a></li>
                    </ul>
                  </div>
                </td>
              </tr>
            @endforeach
          </table>
          {{$pegawai->render()}}
        </div>
      </div>
    </div>
  </div>
@endsection
