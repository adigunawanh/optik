@extends('layouts.app')
@section('title', 'Form Pegawai')

@section('content')
  <div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Pegawai</h3>
      </div>
      <div class="panel-body">
        <form action="/pegawai/store" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Nip</label>
            <input required type="text" name="nip" value="{{old('nip')}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Nama</label>
            <input required type="text" name="nama" value="{{old('nama')}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Jabatan</label>
            <select required class="form-control" name="jabatan">
              <option value="Kasir">Kasir</option>
              <option value="Sales">Sales</option>
              <option value="Opticien">Opticien</option>
            </select>
          </div>
          <input type="submit" class="btn btn-primary" value="Simpan">
          <a href="#" onclick="goBack()" class="btn btn-default">Batal</a>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
