<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/"> <img src="/img/zolaris.png" class="img-responsive" style="width:55px; margin-top:-17px" alt=""> </a>
    </div>

    @if(Auth::check())
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="navbar">
        <ul class="nav navbar-nav">
          <li><a href="/">Beranda</a></li>
          @if(Helper::getBranch()->status == '1')
            @if(Auth::user()->level == '2')
              {{-- menu superadmin --}}
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produk <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="/product/all">Produk List</a></li>
                  <li><a href="/product/">Produk Kategori</a></li>
                  <li><a href="/product-field">Produk Field</a></li>
                </ul>
              </li>
              <li><a href="/branch">Cabang</a></li>
              <li><a href="/customer">Customer</a></li>
              <li><a href="/member">Member</a></li>
              <li><a href="/supplier">Supplier</a></li>
              <li><a href="/pegawai">Pegawai</a></li>
              <li><a href="/rekamedis">Rekamedis</a></li>
              <li><a href="/surat-jalan">Surat Jalan</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Grosir <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="/grosir">List Grosir</a></li>
                  <li><a href="/grosir/pembeli">List Pembeli</a></li>
                  {{-- <li><a href="/grosir/create">Grosir Baru</a></li> --}}
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Transaksi <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="/transaction">List Transaksi</a></li>
                  <li><a href="/transaction/reporting">Laporan Transaksi</a></li>
                </ul>
              </li>
            @elseif(Auth::user()->level == '3')
              {{-- menu hr --}}
              <li><a href="/transaction/reporting">Laporan Transaksi</a></li>
              <li><a href="/pegawai">Pegawai</a></li>
            @elseif(Auth::user()->level == '4')
              {{-- menu gudang --}}
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produk <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="/product/all">Produk List</a></li>
                  <li><a href="/product/">Produk Kategori</a></li>
                  <li><a href="/product-field">Produk Field</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Grosir <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="/grosir">List Grosir</a></li>
                  <li><a href="/grosir/pembeli">List Pembeli</a></li>
                  {{-- <li><a href="/grosir/create">Grosir Baru</a></li> --}}
                </ul>
              </li>
              <li><a href="/surat-jalan">Surat Jalan</a></li>
            @elseif(Auth::user()->level == '1')
              {{-- menu manager --}}
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produk <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="/branch/category/{{Helper::getBranch()->id}}">Produk List</a></li>
                  <li><a href="/product/branch/all">Cari Produk Cabang</a></li>
                  <li><a href="/product/all">Cari Produk Pusat</a></li>
                </ul>
              </li>
              <li><a href="/member">Member</a></li>
              <li><a href="/rekamedis">Rekamedis</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Transaksi <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="/transaction/branch/{{Helper::getBranch()->id}}">List Transaksi</a></li>
                  <li><a href="/transaction/create">Transaksi Baru</a></li>
                  <li><a href="/transaction/reporting">Laporan Transaksi</a></li>
                </ul>
              </li>
            @elseif(Auth::user()->level == '0')
                {{-- menu kasir --}}
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produk <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="/branch/category/{{Helper::getBranch()->id}}">Produk List</a></li>
                    <li><a href="/product/branch/all">Cari Produk Cabang</a></li>
                    <li><a href="/product/all">Cari Produk Pusat</a></li>
                  </ul>
                </li>
                <li><a href="/member">Member</a></li>
                <li><a href="/rekamedis">Rekamedis</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Transaksi <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="/transaction/branch/{{Helper::getBranch()->id}}">List Transaksi</a></li>
                    <li><a href="/transaction/create">Transaksi Baru</a></li>
                    <li><a href="#">Laporan Transaksi</a></li>
                  </ul>
                </li>
            @endif

          @else
            @if(Auth::user()->level == '0')
              {{-- menu kasir --}}
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produk <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="/branch/category/{{Helper::getBranch()->id}}">Produk List</a></li>
                  <li><a href="/product/branch/all">Cari Produk Cabang</a></li>
                  <li><a href="/product/all">Cari Produk Pusat</a></li>
                </ul>
              </li>
              <li><a href="/member">Member</a></li>
              <li><a href="/rekamedis">Rekamedis</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Transaksi <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="/transaction/branch/{{Helper::getBranch()->id}}">List Transaksi</a></li>
                  <li><a href="/transaction/create">Transaksi Baru</a></li>
                </ul>
              </li>
            @elseif(Auth::user()->level == '1')
              {{-- menu manager --}}
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produk <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="/branch/category/{{Helper::getBranch()->id}}">Produk List</a></li>
                  <li><a href="/product/branch/all">Cari Produk Cabang</a></li>
                  <li><a href="/product/all">Cari Produk Pusat</a></li>
                </ul>
              </li>
              <li><a href="/member">Member</a></li>
              <li><a href="/rekamedis">Rekamedis</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Transaksi <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="/transaction/branch/{{Helper::getBranch()->id}}">List Transaksi</a></li>
                  <li><a href="/transaction/create">Transaksi Baru</a></li>
                  <li><a href="/transaction/reporting">Laporan Transaksi</a></li>
                </ul>
              </li>
            @endif

          @endif
        </ul>

        <ul class="nav navbar-nav navbar-right">
          <li><a>{{Auth::user()->name}}</a></li>
          <li><a href="{{route('logout')}}">Logout</a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    @endif
  </div><!-- /.container-fluid -->
</nav>
