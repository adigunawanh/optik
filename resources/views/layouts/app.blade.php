<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    @include('layouts.header')
  </head>
  <body>
    @include('layouts.navigation')
    @yield('content')

    @include('layouts.footer')
  </body>
</html>
