<link rel="stylesheet" href="/css/app.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="shorcut icon" href="/img/zolaris.png">
@stack('style')
