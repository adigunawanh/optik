<footer class="text-center">
  Copyright &copy; <?= date("Y") ?>. Zolaris IT Division.
</footer>

<script src="/js/app.js"></script>
@stack('script')
