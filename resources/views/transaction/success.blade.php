@extends('layouts.app')
@section('title', 'Transaksi Berhasil')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Information</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <h1>Zolaris Optik</h1>
            <!-- <h3>{{Helper::getBranch()->name}}</h3> -->
            <h3>{{Helper::getBranch()->address}}</h3>
          </div>
          <div class="col-md-6 text-right">
            <br>
            <img src="data:image/png;base64,{!!DNS2D::getBarcodePNG($Transaction->barcode, "QRCODE")!!}" alt="barcode"/>
            <br> <span>{{$Transaction->barcode}}</span>
          </div>
        </div>
        <br>
        <a target="_blank" href="/transaction/print/{{$Transaction->invoice}}" class="btn btn-danger">Cetak PDF</a>
        <a target="_blank" href="/transaction/kwitansi/{{$Transaction->invoice}}" class="btn btn-default">Cetak Kwitansi</a>
        @if($product_category_field_id == '4' || $product_category_field_id == '2')
          <a target="_blank" href="/transaction/print/nota-process-frame/{{$Transaction->invoice}}" class="btn btn-default">Cetak Nota</a>
        @else
          <a target="_blank" href="/transaction/print/nota-process-softlense/{{$Transaction->invoice}}" class="btn btn-default">Cetak Nota</a>
        @endif

        <br><br>
        @if($Transaction->id_rekamedis != 0)
        <table class="table table-bordered">
          <tr>
            <td>Nama Pasien</td>
            <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->name}}</td>
          </tr>
          <tr>
            <td>Telepon</td>
            <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->telepon}}</td>
          </tr>
          <tr>
            <td>Alamat</td>
            <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->alamat}}</td>
          </tr>
          <tr>
            <td>Usia</td>
            <td>{{ Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->usia}}</td>
          </tr>
          <tr>
            <td>Status</td>
            <td>{{$Rekamedis->status == '1' ? 'Member' : 'Non Member'}}</td>
          </tr>
        </table>

        <h3>Data Rekamedis</h3>

        <table class="table table-bordered">
          <tr>
            <th></th>
            <th>SPHIRIS</th>
            <th>CYLINDRIS</th>
            <th>AXIS</th>
            <th>AV</th>
          </tr>
          <tr>
            <td>OD</td>
            <td>{{$Rekamedis->od_sphiris}}</td>
            <td>{{$Rekamedis->od_cylindris}}</td>
            <td>{{$Rekamedis->od_axis}}</td>
            <td>{{$Rekamedis->od_av}}</td>
          </tr>
          <tr>
            <td>OS</td>
            <td>{{$Rekamedis->os_sphiris}}</td>
            <td>{{$Rekamedis->os_cylindris}}</td>
            <td>{{$Rekamedis->os_axis}}</td>
            <td>{{$Rekamedis->os_av}}</td>
          </tr>
        </table>

        <table class="table table-bordered">
          <tr>
            <td>ADD : {{$Rekamedis->addition}}</td>
            <td>PD : {{$Rekamedis->pd}}</td>
            <td>PD MM : {{$Rekamedis->pd_mm}}</td>
          </tr>
        </table>
        @endif

        <div class="row">
          <div class="col-md-6">
            <table class="table table-bordered">
              <tr>
                <td>Invoice</td>
                <td>{{$Transaction->invoice}}</td>
              </tr>
              <tr>
                <td>Barcode</td>
                <td><img width="80" src="data:image/png;base64,{!!DNS1D::getBarcodePNG($Transaction->barcode, "C39")!!}" alt="barcode"/></td>
              </tr>
              <tr>
                <td>Total</td>
                <td>Rp. {{number_format($Transaction->total, 0 , '.', '.')}}</td>
              </tr>
              <tr>
                <td>DP</td>
                <td>Rp. {{number_format($Transaction->dp, 0 , '.', '.')}}</td>
              </tr>
              <tr>
                <td>Sisa Pembayaran</td>
                <td>Rp. {{number_format($Transaction->total - $Transaction->dp, 0 , '.', '.')}}</td>
              </tr>
              <tr>
                <td>Tanggal Selesai</td>
                <td>{{$Transaction->tanggal_selesai}}</td>
              </tr>
              <tr>
                <td>Tanggal Transaksi</td>
                <td>{{$Transaction->created_at}}</td>
              </tr>
              <tr>
                <td>Cabang</td>
                <td>{{$Transaction->name}}</td>
              </tr>
              <tr>
                <td>Sales</td>
                <td>{{Helper::pegawai($Transaction->sales)->nama}}</td>
              </tr>
              <tr>
                <td>Opticien</td>
                <td>{{Helper::pegawai($Transaction->opticien)->nama}}</td>
              </tr>
            </table>
          </div>

          <div class="col-md-6">
            <div class="table-responsive">
              <table class="table table-bordered">
                <tr>
                  <th>Produk</th>
                  <th>Qty</th>
                  <th>Price</th>
                  <th>Discount</th>
                  <th>Total</th>
                </tr>
                <?php $total =  0;?>
                @foreach($TransactionDetails as $TransactionDetail)
                  <?php $price_discount = ($TransactionDetail->discount); ?>
                  <tr>
                    <td>{{$TransactionDetail->product}}</td>
                    <td>{{$TransactionDetail->qty}}</td>
                    <td>Rp. {{number_format($TransactionDetail->price, 0 , '.', '.')}}</td>
                    <td>Rp. {{number_format($TransactionDetail->discount, 0 , '.', '.')}}</td>
                    <td>Rp. {{number_format( ($TransactionDetail->price  - $price_discount)  * $TransactionDetail->qty, 0 , '.', '.' )}}</td>
                  </tr>
                  <?php
                    $total += ($TransactionDetail->price  - $price_discount)  * $TransactionDetail->qty;
                  ?>
                @endforeach

                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><strong>Sub Total : Rp. {{number_format($total, 0 , '.', '.')}}</strong></td>
                </tr>
              </table>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            @if(!Request::get('buy'))
              @if($Transaction->id_rekamedis != 0)
                <h3>Pembayaran</h3>
                <form class="" action="/transaction/buy/{{$Transaction->invoice}}" method="post">
                  {{csrf_field()}}
                  <div class="form-group">
                    <label for=""></label>
                    <select class="form-control" name="status">
                      <option value="">Pilih Pembayaran</option>
                      <option value="1">Lunas</option>
                      <option value="0">DP</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Nominal</label>
                    <input type="number" min="{{$total * 30 / 100 }}" value="{{$total * 30 / 100 }}" class="form-control" name="dp" placeholder="">
                    <p>*Keterangan : Dp minimal 30% dari total Transaksi</p>
                  </div>
                  <div class="form-group">
                    <label for="">Tanggal Selesai</label>
                    <input type="date" class="form-control" name="tanggal_selesai" placeholder="">
                  </div>
                  <input type="submit" class="btn btn-primary" value="Bayar">
                </form>
                <br>
              @endif
            @endif
          </div>
          <div class="col-md-6">

          </div>
        </div>

      </div>
    </div>
  </div>
@endsection
