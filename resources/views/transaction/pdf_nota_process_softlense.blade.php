<!DOCTYPE html>
<html>
<head>
  <title>PDF Product</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body style="background:white">
  <div class="row">
    <div class="col-xs-2">
      <img src="img/zolaris.png" height="100" alt="">
    </div>
    <div class="col-xs-4" style="font-size:12px">
      <br>
      Nama Optik: <span>{{Helper::getBranch()->name}}</span><br>
      Alamat: <i style="font-size: 12px">{{Helper::getBranch()->address}}</i><br>
      No. Telp: <i style="font-size: 12px">{{Helper::getBranch()->phone}}</i>
    </div>
    <div class="col-xs-4 text-right" style="font-size:12px">
      <br>
      <span>Tanggal: {{$Transaction->created_at}}</span><br>
      {{-- <span>Customer: {{$surat_jalan->cabang}}</span><br>
      <span>Alamat: {{$surat_jalan->branch_address}}</span> --}}
    </div>
  </div>
  <table class="table">
    <tr>
      <th>Nomor</th>
      <th>Nama Barang</th>
      <th>QTY</th>
      <th>Harga Satuan</th>
      <th>Total</th>
    </tr>
    @php
      $total_barang = 0;
      $total =  0;
    @endphp
    @foreach ($TransactionDetails as $index => $data)
      @php $price_discount = ($data->discount); @endphp
      <tr>
        <td>{{$index + 1}}</td>
        <td>{{$data->product}}</td>
        <td>{{$data->qty}}</td>
        <td>Rp. {{Helper::rupiah($data->price)}}</td>
        <td>Rp. {{Helper::rupiah($data->price * $data->qty)}}</td>
      </tr>
      @php
        $total_barang += $data->qty;
        $total += ($data->price  - $price_discount)  * $data->qty;
      @endphp
    @endforeach
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="font-size:10px">Discount</td>
      <td>Rp. {{Helper::rupiah($data->discount)}}</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="font-size:10px">Total</td>
      <td>Rp. {{Helper::rupiah($total)}}</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="font-size:10px">Anda Hemat: </td>
      <td>Rp. {{Helper::rupiah($data->discount)}}</td>
    </tr>
  </table>

  <div class="col-xs-6 text-left">
    <p>Sales<p>
    <br>
    <br>
    <br>
    ................................
    <br>
    <br>
    <i>Perjanjian: {{Helper::getBranch()->note_description}}</i>
  </div>

  <div class="col-xs-4 text-right">
    <p>Penerima</p>
    <br>
    <br>
    <br>
    ................................
    <br>
  </div>

</body>
</html>
