@extends('layouts.app')
@section('title', 'Transaction')
@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Lists Transaction</h3>
      </div>
      <div class="well well-sm">
        <div class="row">
          <div class="col-md-2">
            <a class="btn btn-default" href="/transaction/create">Buat Transaksi Baru</a>
          </div>
          <div class="col-md-10">
            <form class="form-inline" action="/transaction/search" method="get">
              <div class="form-group">
                <input type="text" class="form-control" value="{{Request::get('q')}}" name="q" placeholder="Cari transaksi...">
                @if(Request::get('q'))
                  <a href="/transaction">Reset pencarian</a>
                @endif
              </div>
              @if(Helper::getBranch()->status == '1')
                <a class="btn btn-success" href="/transaction/excel">Download Report Excel</a>
              @else
                <a class="btn btn-success" href="/transaction/excel/branch/{{Helper::getBranch()->id}}">Download Report Excel</a>
                <a href="/transaction/closing/branch/{{Helper::getBranch()->id}}" class="btn btn-danger">Total Transaksi Hari Ini</a>
              @endif
            </form>
          </div>
        </div>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>Cabang</th>
              <th>Invoice</th>
              <th>Status</th>
              <th>Barcode</th>
              <th></th>
            </tr>
            @foreach($transactions as $index => $transaction)
              <tr>
                <td>{{$index + 1}}</td>
                <td>{{$transaction->created_at}}</td>
                <td>{{$transaction->name}}</td>
                <td>{{$transaction->invoice}}</td>
                <td>{{$transaction->status == '1' ? 'Lunas' : 'Belum Lunas'}}</td>
                <td><img width="80" src="data:image/png;base64,{!!DNS2D::getBarcodePNG($transaction->barcode, "QRCODE")!!}" alt="barcode"/> <br> <span>{{$transaction->barcode}}</span> </td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Option
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <li><a href="/transaction/detail/{{$transaction->invoice}}">Detail</a></li>
                      @if(Auth::user()->level == '1' || Auth::user()->level == '2')
                        <li><a onclick="return confirm('Are you sure?')" href="/transaction/destroy/{{$transaction->invoice}}">Delete</a></li>
                      @endif
                    </ul>
                  </div>
                </td>
              </tr>
            @endforeach
          </table>
          {{$transactions->render()}}
        </div>
      </div>
    </div>
  </div>
@endsection
