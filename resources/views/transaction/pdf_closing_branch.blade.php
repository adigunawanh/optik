<!DOCTYPE html>
<html>
<head>
  <title>PDF Product</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body style="background:white">
  <div class="">
    <div class="row">
      <div class="col-xs-6">
        <img src="img/zolaris.png" height="100" alt="">
      </div>
      <div class="col-xs-4">
        <br>
        <span>{{Helper::getBranch()->name}}</span>
        <br>
        <i style="font-size: 12px">{{Helper::getBranch()->address}}</i>
      </div>
    </div>

    <table class="table table-bordered">
      @php $total_omset = 0 @endphp
      @foreach($transactions as $index => $transaction)
        @php $total_omset += $transaction->qty * $transaction->price  @endphp
      @endforeach
        <tr>
          <td colspan="8">Total Omset</td>
          <td colspan="4">Rp. {{number_format($total_omset, 0 , '.', '.')}}</td>
        </tr>
    </table>

    <div class="col-xs-6 text-left">
      <p>Manager<p>
      <br>
      <br>
      <br>
      ................................
    </div>
    <div class="col-xs-4 text-right">
      <p>Kasir</p>
      <br>
      <br>
      <br>
      ................................
      <br>
      {{Auth::user()->name}}
    </div>

  </div>
</body>
</html>
