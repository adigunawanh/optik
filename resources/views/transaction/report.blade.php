<table class="table table-bordered">
  <tr>
    <th>No</th>
    <th>Tanggal</th>
    <th>Cabang</th>
    <th>Invoice</th>
    <th>Barcode</th>
    <th>Product</th>
    <th>Price</th>
    <th>QTY</th>
    <th>Total</th>
    <th>Kasir</th>
    <th>Sales</th>
    <th>Opticien</th>
  </tr>
  @php $total_omset = 0 @endphp
  @foreach($transactions as $index => $transaction)
    <tr>
      <td>{{$index+1}}</td>
      <td>{{$transaction->created_at}}</td>
      <td>{{$transaction->name}}</td>
      <td>{{$transaction->invoice}}</td>
      <td>{{$transaction->barcode}}</td>
      <td>{{$transaction->product}}</td>
      <td>{{$transaction->price}}</td>
      <td>{{$transaction->qty}}</td>
      <td>{{$transaction->qty * $transaction->price}}</td>
      <td>{{Helper::user($transaction->created_by)->name}}</td>
      <td>{{Helper::pegawai($transaction->sales)->nama}}</td>
      <td>{{Helper::pegawai($transaction->opticien)->nama}}</td>
    </tr>
  @php $total_omset += $transaction->qty * $transaction->price  @endphp
  @endforeach
    <tr>
      <td colspan="8">Total Omset</td>
      <td colspan="4">{{$total_omset}}</td>
    </tr>
</table>
