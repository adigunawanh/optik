<!DOCTYPE html>
<html>
<head>
  <title>PDF Product</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body style="background:white">
  <div class="row">
    <div class="col-xs-2">
      <img src="img/zolaris.png" height="100" alt="">
    </div>
    <div class="col-xs-4">
      Nama Optik: <span>{{Helper::getBranch()->name}}</span><br>
      Alamat: <i style="font-size: 12px">{{Helper::getBranch()->address}}</i><br>
      No. Telp: <i style="font-size: 12px">{{Helper::getBranch()->phone}}</i>
    </div>
    <div class="col-xs-4 text-right">
      <span>
        No Invoice: {{$Transaction->invoice}}
      </span>
    </div>
  </div>

  <h1 class="text-center">Kwitansi</h1>
  <br>

  <?php $total =  0;?>
  <?php $total_discount =  0;?>
  <?php $products =  '';?>
  @foreach($TransactionDetails as $TransactionDetail)
    <?php $price_discount = ($TransactionDetail->discount); ?>

    <?php
      $products .= $TransactionDetail->product . " ";
      $total += ($TransactionDetail->price  - $price_discount)  * $TransactionDetail->qty;
      $total_discount += $price_discount;
    ?>
  @endforeach

  <table class="" style="width:100%">
    <tr>
      <td style="width:20%">Sudah Terima dari</td>
      <td style="width:5%">:</td>
      <td style="width:75%; border-bottom:3px dotted"></td>
    </tr>
    <tr>
      <td style="width:20%">Banyaknya Uang</td>
      <td style="width:5%">:</td>
      <td style="width:75%; border-bottom:3px dotted">Rp. {{Helper::rupiah($total)}}</td>
    </tr>
    <tr>
      <td style="width:20%">Untuk Pembayaran</td>
      <td style="width:5%">:</td>
      <td style="width:75%; border-bottom:3px dotted">{{$products}}</td>
    </tr>
  </table>

  <br><br><br>

  <div class="row">
    <div class="col-xs-6">

    </div>
    <div class="col-xs-4">
      <p style="border-bottom:2px dotted"></p>
      <br><br><br><br>
      <p style="border-bottom:2px dotted"></p>
    </div>
    <div class="col-xs-2">

    </div>
  </div>

</body>
</html>
