<!DOCTYPE html>
<html>
<head>
  <title>PDF Product</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body style="background:white">
  <div class="">
    <div class="row">
      <div class="col-xs-6">
        <h2>Zolaris Optik</h2>
        <!-- <h3>{{Helper::getBranch()->name}}</h3> -->
        <h4>{{Helper::getBranch()->address}}</h4>
      </div>
      <div class="col-xs-4">
        <div class="text-right">
          <br><br>
          <img src="data:image/png;base64,{!!DNS2D::getBarcodePNG($Transaction->barcode, "QRCODE")!!}" alt="barcode"/>
          <br>
          <img width="150px" height="25px" src="data:image/png;base64,{!!DNS1D::getBarcodePNG($Transaction->barcode, "C39")!!}" alt="barcode"/>
          <br>
          <p>{{$Transaction->barcode}}</p>
        </div>
      </div>
    </div>

    @if($Transaction->id_rekamedis != 0)
      <div class="row">
        <div class="col-md-12">
          <div class="">
            <table class="table table-bordered" style="font-size:9px;">
              <tr>
                <td>Nama Pasien</td>
                <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->name}}</td>
              </tr>
              <tr>
                <td>Telepon</td>
                <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->telepon}}</td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->alamat}}</td>
              </tr>
              <tr>
                <td>Usia</td>
                <td>{{ date("Y") - date("Y", strtotime(Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->tanggal_lahir))}}</td>
              </tr>
              <tr>
                <td>Status</td>
                <td>{{$Rekamedis->status == '1' ? 'Member' : 'Non Member'}}</td>
              </tr>
            </table>
          </div>
        </div>
        <div class="col-md-12">
          <div class="">
            <table class="table table-bordered" style="font-size:9px;">
              <tr>
                <th></th>
                <th>SPHIRIS</th>
                <th>CYLINDRIS</th>
                <th>AXIS</th>
                <th>AV</th>
              </tr>
              <tr>
                <td>OD</td>
                <td>{{$Rekamedis->od_sphiris}}</td>
                <td>{{$Rekamedis->od_cylindris}}</td>
                <td>{{$Rekamedis->od_axis}}</td>
                <td>{{$Rekamedis->od_av}}</td>
              </tr>
              <tr>
                <td>OS</td>
                <td>{{$Rekamedis->os_sphiris}}</td>
                <td>{{$Rekamedis->os_cylindris}}</td>
                <td>{{$Rekamedis->os_axis}}</td>
                <td>{{$Rekamedis->os_av}}</td>
              </tr>
            </table>
          </div>

          <div class="">
            <table class="table table-bordered" style="font-size:9px;">
              <tr>
                <td>ADD : {{$Rekamedis->addition}}</td>
                <td>PD : {{$Rekamedis->pd}}</td>
                <td>PD MM : {{$Rekamedis->pd_mm}}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    @endif

    <div class="row">
      <div class="col-xs-6">
        <div class="table-responsive">
          <table class="" style="font-size:9px; padding:7px">
            <tr>
              <td>Invoice</td>
              <td>{{$Transaction->invoice}}</td>
            </tr>
            <tr>
              <td>Tanggal Selesai</td>
              <td>{{$Transaction->tanggal_selesai}}</td>
            </tr>
            <tr>
              <td>Tanggal Transaksi</td>
              <td>{{$Transaction->created_at}}</td>
            </tr>
            <tr>
              <td>Sales</td>
              <td>{{Helper::pegawai($Transaction->sales)->nama}}</td>
            </tr>
            <tr>
              <td>Opticien</td>
              <td>{{Helper::pegawai($Transaction->opticien)->nama}}</td>
            </tr>
          </table>
        </div>
      </div>

      <div class="col-xs-4">
        <div class="table-responsive">
          <table class="" style="font-size:9px; padding:7px">
            <tr>
              <th>Produk</th>
              <th>Qty</th>
              <th>Price</th>
              <th>Discount</th>
              <th>Total</th>
            </tr>
            <?php $total =  0;?>
            @foreach($TransactionDetails as $TransactionDetail)
              <?php $price_discount = ($TransactionDetail->discount); ?>
              <tr>
                <td>{{$TransactionDetail->product}}</td>
                <td>{{$TransactionDetail->qty}}</td>
                <td>Rp. {{number_format($TransactionDetail->price, 0 , '.', '.')}}</td>
                <td>Rp. {{number_format($TransactionDetail->discount, 0 , '.', '.')}}</td>
                <td>Rp. {{number_format( ($TransactionDetail->price  - $price_discount)  * $TransactionDetail->qty, 0 , '.', '.' )}}</td>
              </tr>
              <?php
                $total += ($TransactionDetail->price  - $price_discount)  * $TransactionDetail->qty;
              ?>
            @endforeach

            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><strong>Sub Total : Rp. {{number_format($total, 0 , '.', '.')}}</strong></td>
            </tr>
          </table>
        </div>
      </div>
    </div>

  </div>
</body>
</html>
