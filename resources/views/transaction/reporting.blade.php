@extends('layouts.app')
@section('title', 'Laporan Transaksi')

@push('style')
  <link rel="stylesheet" href="/jquery-ui/jquery-ui.min.css">
@endpush
@section('content')
  <div class="container">
    <div class="well well-sm">
      <form class="form-inline" action="" method="get">
        <div class="form-group">
          <label for="">Pilih Cabang</label><br>
          <select class="form-control" name="branch">
            @foreach($branches as $branch)
              <option {{Request::get('branch') == $branch->id ? 'selected' : ''}} value="{{$branch->id}}">{{$branch->name}}</option>
            @endforeach
              <option value="">All</option>
          </select>
        </div>
        <div class="form-group">
          <label for="">Tanggal Awal</label><br>
          <input type="text" class="form-control datepicker" name="date_start" value="{{ Request::get('date_start') ? Request::get('date_start') : date("Y-m-d")}}">
        </div>
        <div class="form-group">
          <label for="">Tanggal Akhir</label><br>
          <input type="text" class="form-control datepicker" name="data_end" value="{{ Request::get('data_end') ? Request::get('data_end') : date("Y-m-d")}}">
        </div>
        <div class="form-group">
          <br>
          <input type="submit" name="" value="Lihat" class="btn btn-default">
          <input type="submit" name="download" value="Download Excel" class="btn btn-success">
        </div>
      </form>
    </div>

    <div class="table-responsive">
      <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Tanggal</th>
          <th>Cabang</th>
          <th>Invoice</th>
          <th>Barcode</th>
          <th>Product</th>
          <th>Price</th>
          <th>QTY</th>
          <th>Total</th>
          <th>Kasir</th>
          <th>Sales</th>
          <th>Opticien</th>
        </tr>
        @php $total_omset = 0 @endphp
        @foreach($transactions as $index => $transaction)
          <tr>
            <td>{{$index+1}}</td>
            <td>{{$transaction->created_at}}</td>
            <td>{{$transaction->name}}</td>
            <td>{{$transaction->invoice}}</td>
            <td>{{$transaction->barcode}}</td>
            <td>{{$transaction->product}}</td>
            <td>{{$transaction->price}}</td>
            <td>{{$transaction->qty}}</td>
            <td>{{$transaction->qty * $transaction->price}}</td>
            <td>{{Helper::user($transaction->created_by)->name}}</td>
            <td>{{Helper::pegawai($transaction->sales)->nama}}</td>
            <td>{{Helper::pegawai($transaction->opticien)->nama}}</td>
          </tr>
        @php $total_omset += $transaction->qty * $transaction->price  @endphp
        @endforeach
          <tr>
            <td colspan="8">Total Omset</td>
            <td colspan="4">Rp. {{number_format($total_omset, 0 , '.', '.')}}</td>
          </tr>
      </table>
    </div>

  </div>
@endsection

@push('script')
  <script src="/jquery-ui/jquery-ui.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $(".datepicker").datepicker({
      dateFormat: 'yy-mm-dd',
      changeYear: true,
      changeMonth: true,
    });
  });
</script>
@endpush
