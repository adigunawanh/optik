<!DOCTYPE html>
<html>
<head>
  <title>PDF Product</title>
  <link rel="stylesheet" href="css/app.css">
  <style media="screen">
    body{
      width: 595px !important;
      height: 420px !important;
      font-size: 9px;
    }
    .tables{
      border: 1px solid;
      width: 100%;
    }
  </style>
</head>
<body style="background:white">
  <div class="row">
    <div class="col-xs-6">
      <div class="row">
        <div class="col-xs-6">
          <div class="row">
            <div class="col-xs-3">
              <img src="img/zolaris.png" height="50px" alt="">
            </div>
            <div class="col-xs-9">
              Nama Optik: <span>{{Helper::getBranch()->name}}</span><br>
              Alamat: <i>{{Helper::getBranch()->address}}</i><br>
              No. Telp: <i>{{Helper::getBranch()->phone}}</i>
            </div>
            {{-- <div class="col-xs-5"> --}}
              {{-- <br>
              <img height="25px" src="data:image/png;base64,{!!DNS2D::getBarcodePNG($Transaction->barcode, "QRCODE")!!}" alt="barcode"/>
              <br>
              {{$Transaction->barcode}}
              <br>
              <img height="8px" src="data:image/png;base64,{!!DNS1D::getBarcodePNG($Transaction->barcode, "C39")!!}" alt="barcode"/> --}}
            {{-- </div> --}}
          </div>
        </div>
      </div>
      <div class="row">
        <br>
        <div class="col-xs-6">
          <table class="" style="width:100%">
            <tr>
              <td>No Faktur</td>
              <td>:</td>
              <td>{{$Transaction->invoice}}</td>
            </tr>
            <tr>
              <td>Nama</td>
              <td>:</td>
              <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->name}}</td>
            </tr>
            <tr>
              <td>No HP</td>
              <td>:</td>
              <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->telepon}}</td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td>:</td>
              <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->alamat}}</td>
            </tr>
            <tr>
              <td>Usia</td>
              <td>:</td>
              <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->usia}}</td>
            </tr>
          </table>
          <br>
          <table class="tables">
            <tr>
              <th></th>
              <th>SPHIRIS</th>
              <th>CYLINDRIS</th>
              <th>AXIS</th>
              <th>AV</th>
            </tr>
            <tr>
              <td>OD</td>
              <td>{{$Rekamedis->od_sphiris}}</td>
              <td>{{$Rekamedis->od_cylindris}}</td>
              <td>{{$Rekamedis->od_axis}}</td>
              <td>{{$Rekamedis->od_av}}</td>
            </tr>
            <tr>
              <td>OS</td>
              <td>{{$Rekamedis->os_sphiris}}</td>
              <td>{{$Rekamedis->os_cylindris}}</td>
              <td>{{$Rekamedis->os_axis}}</td>
              <td>{{$Rekamedis->os_av}}</td>
            </tr>
          </table>

          <br>

          <table class="tables">
            <tr>
              <td>ADD : {{$Rekamedis->addition}}</td>
              <td>PD : {{$Rekamedis->pd}}</td>
              <td>PD MM : {{$Rekamedis->pd_mm}}</td>
            </tr>
          </table>

          <br>

          <table class="" style="width:100%">
            <tr>
              <td>Opticien</td>
              <td>:</td>
              <td>{{Helper::pegawai($Transaction->opticien)->nama}}</td>
            </tr>
            <tr>
              <td>Sales</td>
              <td>:</td>
              <td>{{Helper::pegawai($Transaction->sales)->nama}}</td>
            </tr>
            <tr>
              <td>R/Dr</td>
              <td>:</td>
              <td>-</td>
            </tr>
            <tr>
              <td>Tgl Selesai</td>
              <td>:</td>
              <td>{{date_format(date_create($Transaction->tanggal_selesai), "d M Y")}}</td>
            </tr>
            <tr>
              <td>Catatan</td>
              <td>:</td>
              <td>{{$Rekamedis->notes}}</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
    <div class="col-xs-6">
      <div class="row">
        {{-- <div class="col-xs-3">
          <img src="img/zolaris.png" height="50" alt="">
        </div>
        <div class="col-xs-4">
          <br>
          Nama Optik: <span>{{Helper::getBranch()->name}}</span><br>
          Alamat: <i>{{Helper::getBranch()->address}}</i><br>
          No. Telp: <i>{{Helper::getBranch()->phone}}</i>
        </div> --}}
        <div class="col-xs-12 text-right">
          <img height="25" src="data:image/png;base64,{!!DNS2D::getBarcodePNG($Transaction->barcode, "QRCODE")!!}" alt="barcode"/>
          <br>
          {{$Transaction->barcode}}
          <br>
          <img height="8px" src="data:image/png;base64,{!!DNS1D::getBarcodePNG($Transaction->barcode, "C39")!!}" alt="barcode"/>
        </div>
      </div>
      <div class="row">
        <br>
        <div class="col-xs-12">
          <table>
            <tr>
              <td>No Faktur</td>
              <td>:</td>
              <td>{{$Transaction->invoice}}</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Nama</td>
              <td>:</td>
              <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->name}}</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>No HP</td>
              <td>:</td>
              <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->telepon}}</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td>:</td>
              <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->alamat}}</td>
              <td></td>
              <td></td>
            </tr>
            <?php $total =  0;?>
            <?php $total_discount =  0;?>
            @foreach($TransactionDetails as $TransactionDetail)
              <?php $price_discount = ($TransactionDetail->discount); ?>
              <tr>
                <td>Produk</td>
                <td>:</td>
                <td>{{$TransactionDetail->product}} {{$TransactionDetail->warna_lensa ? '/'.$TransactionDetail->warna_lensa : '' }}</td>
                <td>Harga</td>
                <td>Rp. {{Helper::rupiah($TransactionDetail->price)}}</td>
              </tr>
              <?php
                $total += ($TransactionDetail->price  - $price_discount)  * $TransactionDetail->qty;
                $total_discount += $price_discount;
              ?>
            @endforeach
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td>Diskon</td>
              <td>Rp. {{Helper::rupiah($total_discount)}}</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td>Harga Total</td>
              <td>Rp. {{Helper::rupiah($total)}}</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td>DP</td>
              <td>Rp. {{Helper::rupiah($Transaction->dp)}}</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td>Sisa</td>
              <td>Rp. {{Helper::rupiah($Transaction->total - $Transaction->dp)}}</td>
            </tr>
            <tr>
              <td>Tgl Selesai</td>
              <td>:</td>
              <td>{{date_format(date_create($Transaction->tanggal_selesai), "d M Y")}}</td>
              <td></td>
              <td></td>
            </tr>
          </table>
          <br>

          <div class="row">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-6 text-left">
                  <p>Tanda Tangan Pemesan<p>
                  <br>
                  <br>
                  ....................................
                  <p>Pemesan: Sdr. {{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->name}}</p>
                </div>

                <div class="col-xs-10 text-right">
                  <p>Hormat Kami</p>
                  <br>
                  <br>
                  ....................................
                  <br>
                  <p>Kasir: {{Helper::user($Transaction->created_by)->name}}</p>
                </div>
              </div>
            </div>

            <div class="row">
              <br><br><br><br><br><br><br>
              <div class="col-xs-12">
                <br>
                <strong>Perjanjian</strong>
                <p>
                  {{Helper::getBranch()->note_description}}
                </p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
