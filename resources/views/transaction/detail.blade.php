@extends('layouts.app')
@section('title', 'Detail Transaksi')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Detail Transaction</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <h1>Zolaris Optik</h1>
            <!-- <h3>{{Helper::getBranch()->name}}</h3> -->
            <h3>{{Helper::getBranch()->address}}</h3>
          </div>
          <div class="col-md-6 text-right">
            <br>
            <img src="data:image/png;base64,{!!DNS2D::getBarcodePNG($Transaction->barcode, "QRCODE")!!}" alt="barcode"/>
            <br>
            {{$Transaction->barcode}}
            <br>
            <img width="150px" height="25px" src="data:image/png;base64,{!!DNS1D::getBarcodePNG($Transaction->barcode, "C39")!!}" alt="barcode"/>
          </div>
        </div>
        <br>
        <a target="_blank" href="/transaction/print/{{$Transaction->invoice}}" class="btn btn-danger">Cetak PDF</a>
        <a target="_blank" href="/transaction/kwitansi/{{$Transaction->invoice}}" class="btn btn-default">Cetak Kwitansi</a>
        {{-- <a target="_blank" href="/transaction/print/nota-process/{{$Transaction->invoice}}" class="btn btn-danger">Cetak Nota Process</a> --}}
        @if($product_category_field_id == '4' || $product_category_field_id == '2')
          <a target="_blank" href="/transaction/print/nota-process-frame/{{$Transaction->invoice}}" class="btn btn-default">Cetak Nota</a>
        @else
          <a target="_blank" href="/transaction/print/nota-process-softlense/{{$Transaction->invoice}}" class="btn btn-default">Cetak Nota</a>
        @endif
        <br><br>
        @if($Transaction->id_rekamedis != 0)
          <div class="table-responsive">
            <table class="table table-bordered">
              <tr>
                <td>Nama Pasien</td>
                <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->name}}</td>
              </tr>
              <tr>
                <td>Telepon</td>
                <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->telepon}}</td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td>{{Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->alamat}}</td>
              </tr>
              <tr>
                <td>Usia</td>
                <td>{{ date("Y") - date("Y", strtotime(Helper::pasien($Rekamedis->status, $Rekamedis->id_pasien)->tanggal_lahir))}}</td>
              </tr>
              <tr>
                <td>Status</td>
                <td>{{$Rekamedis->status == '1' ? 'Member' : 'Non Member'}}</td>
              </tr>
            </table>
          <div class="table-responsive">

          <h3>Data Rekamedis</h3>

          <div class="table-responsive">
            <table class="table table-bordered">
              <tr>
                <th></th>
                <th>SPHIRIS</th>
                <th>CYLINDRIS</th>
                <th>AXIS</th>
                <th>AV</th>
              </tr>
              <tr>
                <td>OD</td>
                <td>{{$Rekamedis->od_sphiris}}</td>
                <td>{{$Rekamedis->od_cylindris}}</td>
                <td>{{$Rekamedis->od_axis}}</td>
                <td>{{$Rekamedis->od_av}}</td>
              </tr>
              <tr>
                <td>OS</td>
                <td>{{$Rekamedis->os_sphiris}}</td>
                <td>{{$Rekamedis->os_cylindris}}</td>
                <td>{{$Rekamedis->os_axis}}</td>
                <td>{{$Rekamedis->os_av}}</td>
              </tr>
            </table>

            <table class="table table-bordered">
              <tr>
                <td>ADD : {{$Rekamedis->addition}}</td>
                <td>PD : {{$Rekamedis->pd}}</td>
                <td>PD MM : {{$Rekamedis->pd_mm}}</td>
              </tr>
            </table>
          </div>
          <!-- <table class="table table-bordered">
            <tr>
              <td>OD Sphiris</td>
              <td>{{$Rekamedis->od_sphiris}}</td>
            </tr>
            <tr>
              <td>OS Sphiris</td>
              <td>{{$Rekamedis->os_sphiris}}</td>
            </tr>
            <tr>
              <td>OD Cylindris</td>
              <td>{{$Rekamedis->od_cylindris}}</td>
            </tr>
            <tr>
              <td>OS Cylindris</td>
              <td>{{$Rekamedis->os_cylindris}}</td>
            </tr>
            <tr>
              <td>OD Axis</td>
              <td>{{$Rekamedis->od_axis}}</td>
            </tr>
            <tr>
              <td>OS Axis</td>
              <td>{{$Rekamedis->os_axis}}</td>
            </tr>
            <tr>
              <td>Addition</td>
              <td>{{$Rekamedis->addition}}</td>
            </tr>
            <tr>
              <td>Add Kiri</td>
              <td>{{$Rekamedis->add_kiri}}</td>
            </tr>
            <tr>
              <td>Add Kanan</td>
              <td>{{$Rekamedis->add_kanan}}</td>
            </tr>
          </table> -->
        @endif

        <div class="row">
          <div class="col-md-6">
            <div class="table-responsive">
              <table class="table table-bordered">
                <tr>
                  <td>Invoice</td>
                  <td>{{$Transaction->invoice}}</td>
                </tr>
                <tr>
                  <td>Total</td>
                  <td>Rp. {{number_format($Transaction->total, 0 , '.', '.')}}</td>
                </tr>
                <tr>
                  <td>DP</td>
                  <td>Rp. {{number_format($Transaction->dp, 0 , '.', '.')}}</td>
                </tr>
                <tr>
                  <td>Sisa Pembayaran</td>
                  <td>Rp. {{number_format($Transaction->total - $Transaction->dp, 0 , '.', '.')}}</td>
                </tr>
                <tr>
                  <td>Tanggal Selesai</td>
                  <td>{{$Transaction->tanggal_selesai}}</td>
                </tr>
                <tr>
                  <td>Tanggal Transaksi</td>
                  <td>{{$Transaction->created_at}}</td>
                </tr>
                <tr>
                  <td>Cabang</td>
                  <td>{{$Transaction->name}}</td>
                </tr>
                <tr>
                  <td>Sales</td>
                  <td>{{Helper::pegawai($Transaction->sales)->nama}}</td>
                </tr>
                <tr>
                  <td>Opticien</td>
                  <td>{{Helper::pegawai($Transaction->opticien)->nama}}</td>
                </tr>
              </table>
            </div>
          </div>

          <div class="col-md-6">
            <div class="table-responsive">
              <table class="table table-bordered">
                <tr>
                  <th>Produk</th>
                  <th>Qty</th>
                  <th>Price</th>
                  <th>Discount</th>
                  <th>Total</th>
                </tr>
                <?php $total =  0;?>
                @foreach($TransactionDetails as $TransactionDetail)
                  <?php $price_discount = ($TransactionDetail->discount); ?>
                  <tr>
                    <td>{{$TransactionDetail->product}}</td>
                    <td>{{$TransactionDetail->qty}}</td>
                    <td>Rp. {{number_format($TransactionDetail->price, 0 , '.', '.')}}</td>
                    <td>Rp. {{number_format($TransactionDetail->discount, 0 , '.', '.')}}</td>
                    <td>Rp. {{number_format( ($TransactionDetail->price  - $price_discount)  * $TransactionDetail->qty, 0 , '.', '.' )}}</td>
                  </tr>
                  <?php
                    $total += ($TransactionDetail->price  - $price_discount)  * $TransactionDetail->qty;
                  ?>
                @endforeach

                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><strong>Sub Total : Rp. {{number_format($total, 0 , '.', '.')}}</strong></td>
                </tr>
              </table>

              @if($Transaction->status == '0')
                <a onclick="return confirm('Yakin telah lunas?')" href="/transaction/complete/{{$Transaction->invoice}}" class="btn btn-success"><i class="fa fa-money"></i> Transaksi Pelunasan</a>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
