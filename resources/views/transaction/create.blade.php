@extends('layouts.app')
@section('title', 'Form Transaksi')

@push('style')
  <link rel="stylesheet" href="/select2/css/select2.min.css">
  <link rel="stylesheet" href="/jquery-ui/jquery-ui.min.css">
@endpush

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Transaction</h3>
      </div>
      <div class="panel-body">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Scan Barcode</h3>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label for="">Barcode</label>
              <input required type="text" class="form-control" id="{{Helper::getBranch()->status == '1' ? 'ScaneBarcode' : 'ScaneBarcodeBranch'}}" placeholder="Scan barcode...">
            </div>
          </div>
        </div>
        <form action="{{Helper::getBranch()->status == '1' ? '/transaction/store' : '/transaction/store-branch/'.Helper::getBranch()->id}}" method="post">
          {{csrf_field()}}
          <input type="hidden" name="id_rekamedis" value="0" id="id_rekamedis">
          <label for="isLense">
            <input type="checkbox" name="isLense" id="isLense" value="" oninvalid="this.setCustomValidity('Harus diisi')" oninput="this.setCustomValidity('')"> <span>Input Rekamedis</span>
          </label>

          <div id="PlaceBarcodeResult"></div>

          <hr>
          <div class="form-group">
            <label for="">Sales</label><br>
            <select class="form-control select2" name="sales" required>
              @php $sales = DB::table('pegawai')->where('jabatan', 'Sales')->get(); @endphp
              @php $opticien = DB::table('pegawai')->where('jabatan', 'Opticien')->get(); @endphp
              @php $member = DB::table('members')->get(); @endphp
              @foreach($sales as $data)
                <option value="{{$data->id}}">{{$data->nama}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="">Opticien</label><br>
            <select class="form-control select2" name="opticien" required>
              @foreach($opticien as $data)
                <option value="{{$data->id}}">{{$data->nama}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="">Member <small>*Kosongkan jika nonmember</small> </label><br>
            <select class="form-control select2" name="member">
              <option value="">Pilih Member</option>
              @foreach($member as $data)
                <option value="{{$data->member_id}}">{{$data->member_id}}-{{$data->name}}</option>
              @endforeach
            </select>
          </div>
          <input type="submit" id="submitTransaction" disabled value="Lanjutkan" class="btn btn-primary">
          <a href="#" onclick="goBack()" class="btn btn-default">Batal</a>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalRekamedis" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Form Rekamedis</h4>
        </div>
        <div class="modal-body">
          <form class="" action="/rekamedis/store" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <label for="">Status</label>
              <select class="form-control" id="statusRekamedis">
                <option value="0">Non Member</option>
                <option value="1">Member</option>
              </select>
            </div>
            <div id="isCustomer">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="">Nik</label>
                    <input type="text" id="nikCustomer" value="" class="form-control" required>
                  </div>
                  <div class="form-group">
                    <label for="">Nama</label>
                    <input type="text" id="nameCustomer" value="" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Tempat Lahir</label>
                    <input type="text" id="tempat_lahirCustomer" value="" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Tanggal Lahir</label>
                    <input type="date" id="tanggal_lahirCustomer" value="" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Usia</label>
                    <input type="number" id="usiaCustomer" value="" class="form-control">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="">Jenis Kelamin</label><br>
                    <label><input checked type="radio" name="jenis_kelaminCustomer" value="L">Pria &nbsp; </label>
                    <label><input type="radio" name="jenis_kelaminCustomer" value="P">Wanita</label>
                  </div>
                  <div class="form-group">
                    <label for="">Telepon</label>
                    <input type="text" id="teleponCustomer" value="" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" id="emailCustomer" value="" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Alamat</label>
                    <input type="text" id="alamatCustomer" value="" class="form-control">
                  </div>
                </div>
              </div>

            </div>
            <div id="isMember">
              <div class="form-group">
                <label for="">ID Member</label>
                <input type="text" id="id_pasien" value="" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Member Name</label>
                <input type="text" readonly id="memberName" class="form-control">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">

                <table class="table">
                  <tr>
                    <th></th>
                    <th>SPHIRIS</th>
                    <th>CYLINDRIS</th>
                    <th>AXIS</th>
                    <th>AV</th>
                  </tr>
                  <tr>
                    <td>OD</td>
                    <td><input type="text" id="od_sphiris" value="" class="form-control"></td>
                    <td><input type="text" id="od_cylindris" value="" class="form-control"></td>
                    <td><input type="text" id="od_axis" value="" class="form-control"></td>
                    <td><input type="text" id="od_av" value="" class="form-control"></td>
                  </tr>
                  <tr>
                    <td>OS</td>
                    <td><input type="text" id="os_sphiris" value="" class="form-control"></td>
                    <td><input type="text" id="os_cylindris" value="" class="form-control"></td>
                    <td><input type="text" id="os_axis" value="" class="form-control"></td>
                    <td><input type="text" id="os_av" value="" class="form-control"></td>
                  </tr>
                </table>

                <div class="form-group">
                  Addition <input width="30px" type="text" id="addition" value="" class="">
                  PD <input width="30px" type="text" id="pd" value="" class=""> / <input width="30px" class="" id="pd_mm" type="text" name="" value=""> mm
                </div>

                <div class="form-group">
                  <label for="">Keterangan (tambahan)</label>
                  <textarea class="form-control" id="notes"></textarea>
                </div>
              </div>

              {{-- <div class="col-md-6"> --}}
              {{-- <div class="form-group">
                <label for="">OD Sphiris</label>
                <input type="text" id="od_sphiris" value="" class="form-control">
              </div>
              <div class="form-group">
                <label for="">OS Sphiris</label>
                <input type="text" id="os_sphiris" value="" class="form-control">
              </div>
              <div class="form-group">
                <label for="">OD Cylindris</label>
                <input type="text" id="od_cylindris" value="" class="form-control">
              </div>
              <div class="form-group">
                <label for="">OS Cylindris</label>
                <input type="text" id="os_cylindris" value="" class="form-control">
              </div>
              <div class="form-group">
                <label for="">OD Axis</label>
                <input type="text" id="od_axis" value="" class="form-control">
              </div>
              <div class="form-group">
                <label for="">OS Axis</label>
                <input type="text" id="os_axis" value="" class="form-control">
              </div> --}}
                {{-- <div class="form-group">
                  <label for="">ADD</label>
                  <input type="text" id="addition" value="" class="form-control">
                </div> --}}
                {{-- <div class="form-group">
                  <label for="">Add Kiri</label>
                  <input type="text" id="add_kiri" value="" class="form-control">
                </div>
                <div class="form-group">
                  <label for="">Add Kanan</label>
                  <input type="text" id="add_kanan" value="" class="form-control">
                </div> --}}
                {{-- <div class="form-group">
                  <label for="">AV</label>
                  <input type="text" id="av" value="" class="form-control">
                </div> --}}
                {{-- <div class="form-group">
                  <label for="">PD</label>
                  <input type="text" id="pd" value="" class="form-control">
                </div> --}}
                {{-- <div class="form-group">
                  <label for="">Keterangan (tambahan)</label>
                  <textarea class="form-control" id="notes"></textarea>
                </div> --}}
              {{-- </div> --}}

            </div>
            <input type="button" value="Simpan" class="btn btn-primary" id="storeRekamedis">
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script type="text/javascript">
    $("#isLense").click(function(){
      if($(this).is(":checked")){
        $("#modalRekamedis").modal('show');
        $("#lenseProduct").removeAttr('disabled');
        $("#lensePrice").removeAttr('disabled');
        $("#lenseQty").removeAttr('disabled');
      }else{
        $("#lenseProduct").attr('disabled', true);
        $("#lensePrice").attr('disabled', true);
        $("#lenseQty").attr('disabled', true);
      }
    })
  </script>

  <script type="text/javascript">
    $("#isCustomer").show()
    $("#isMember").hide()
    $("#statusRekamedis").change(function(){
      if($(this).val() == '0'){
        $("#isCustomer").show()
        $("#isMember").hide()
      }else{
        $("#isMember").show()
        $("#isCustomer").hide()
      }
    })
  </script>

  <script type="text/javascript">
    $("#storeRekamedis").click(function(){
      $.ajax({
        url : "/rekamedis/store",
        type : 'post',
        data : {
          _token : '{{csrf_token()}}',
          id_pasien : $("#id_pasien").val(),
          status : $("#statusRekamedis").val(),
          od_sphiris : $("#od_sphiris").val(),
          os_sphiris : $("#os_sphiris").val(),
          od_cylindris : $("#od_cylindris").val(),
          os_cylindris : $("#os_cylindris").val(),
          od_axis : $("#od_axis").val(),
          os_axis : $("#os_axis").val(),
          add_kiri : $("#add_kiri").val(),
          add_kanan : $("#add_kanan").val(),
          addition : $("#addition").val(),
          notes : $("#notes").val(),
          od_av : $("#od_av").val(),
          os_av : $("#os_av").val(),
          pd : $("#pd").val(),
          pd_mm : $("#pd_mm").val(),

          nik : $("#nikCustomer").val(),
          name : $("#nameCustomer").val(),
          tempat_lahir : $("#tempat_lahirCustomer").val(),
          tanggal_lahir : $("#tanggal_lahirCustomer").val(),
          usia : $("#usiaCustomer").val(),
          jenis_kelamin : $('input[name=jenis_kelaminCustomer]:checked').val(),
          telepon : $("#teleponCustomer").val(),
          email : $("#emailCustomer").val(),
          alamat : $("#alamatCustomer").val(),
        },
        success : function(response){
          // console.log(response);
          $("#id_rekamedis").val(response.id)
          $("#modalRekamedis").modal('hide');
        }
      })
    })

    $("#id_pasien").change(function(){
      $.ajax({
        url : "/rekamedis/member/"+$(this).val(),
        type : 'get',
        success : function(response){
          console.log(response);
          $("#memberName").val(response[0].name)
          if(response[1]){
            $("#od_sphiris").val(response[1].od_sphiris);
            $("#os_sphiris").val(response[1].os_sphiris);
            $("#od_cylindris").val(response[1].od_cylindris);
            $("#os_cylindris").val(response[1].os_cylindris);
            $("#od_axis").val(response[1].od_axis);
            $("#os_axis").val(response[1].os_axis);
            $("#add_kiri").val(response[1].add_kiri);
            $("#add_kanan").val(response[1].add_kanan);
            $("#notes").val(response[1].notes);
            $("#addition").val(response[1].addition);
            $("#od_av").val(response[1].od_av);
            $("#os_av").val(response[1].os_av);
            $("#pd").val(response[1].pd);
            $("#pd_mm").val(response[1].pd_mm);
          }
        }
      })
    })
  </script>

  <script type="text/javascript" src="/select2/js/select2.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('.select2').select2();
  });
  </script>

  <script type="text/javascript">
    $("#ScaneBarcode").change(function(){
      var barcode = $(this).val();
      if(barcode != ""){

        $.ajax({
          url : '/product/scane-barcode',
          type : 'post',
          data : {
            barcode : barcode,
            _token : '{{csrf_token()}}',
          },
          success : function(response){
            $("#ScaneBarcode").val('');
            // console.log(response);

            var product_lense = '';
            if(response.ProductCategoryFieldId == '4'){
              $("#isLense").prop('required', true);
              product_lense =
                '<div class="form-group">'+
                '<label for="">Ukuran</label>'+
                '<input type="number" name="ukuran_lensa[]" value="0" class="form-control">'+
                '</div>' +
                '<div class="form-group">'+
                '<label for="">Warna</label>'+
                '<input type="text" name="warna_lensa[]" class="form-control">'+
                '</div>'
              ;
            }else if(response.ProductCategoryFieldId == '2'){
              $("#isLense").prop('required', true);
            }else{
              product_lense =
                '<input type="hidden" name="ukuran_lensa[]" value="" class="form-control">' +
                '<input type="hidden" name="warna_lensa[]" class="form-control">'
              ;
            }

            // kondisi jika code barcode / produk sama
            if(response.ProductId == $("#"+response.ProductId).val()){
              $("#qty"+response.ProductId).val(parseInt($("#qty"+response.ProductId).val())+1);
            }else{
              $("#PlaceBarcodeResult").append(
                '<div class="form-group">'+
                '<label for="">Produk</label>'+
                '<input readonly type="hidden" name="product_id[]" id="'+response.ProductId+'" value="'+response.ProductId+'" class="form-control">'+
                '<input readonly type="text" name="product[]" value="'+response.ProductName+'" class="form-control">'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="">Price</label>'+
                '<input readonly type="text" name="price[]" value="'+response.ProductPrice+'" class="form-control">'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="">Qty</label>'+
                '<input type="number" max="'+response.ProductStock+'" id="qty'+response.ProductId+'" name="qty[]" value="1" class="form-control">'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="">Discount</label>'+
                '<input type="number" name="discount[]" value="0" min="0" max="'+response.ProductPrice+'" class="form-control">'+
                '</div>'+
                product_lense +
                '<hr>'
              );
            }
            $("#submitTransaction").prop('disabled', false)
          }

        })
      }
    })
  </script>

  <script type="text/javascript">
    $("#ScaneBarcodeBranch").change(function(){
      var barcode = $(this).val();
      if(barcode != ""){

        $.ajax({
          url : '/product/scane-barcode-branch',
          type : 'post',
          data : {
            barcode : barcode,
            branch_id : '{{Helper::getBranch()->id}}',
            _token : '{{csrf_token()}}',
          },
          success : function(response){
            $("#ScaneBarcodeBranch").val('');
            // console.log(response);

            var product_lense = '';
            if(response.ProductCategoryFieldId == '4'){
              $("#isLense").prop('required', true);
              product_lense =
                '<div class="form-group">'+
                '<label for="">Ukuran</label>'+
                '<input type="number" name="ukuran_lensa[]" value="0" class="form-control">'+
                '</div>' +
                '<div class="form-group">'+
                '<label for="">Warna</label>'+
                '<input type="text" name="warna_lensa[]" class="form-control">'+
                '</div>'
              ;
            }else if(response.ProductCategoryFieldId == '2'){
              $("#isLense").prop('required', true);
            }else{
              product_lense =
                '<input type="hidden" name="ukuran_lensa[]" value="" class="form-control">' +
                '<input type="hidden" name="warna_lensa[]" class="form-control">'
              ;
            }

            // kondisi jika code barcode / produk sama
            if(response.ProductId == $("#"+response.ProductId).val()){
              $("#qty"+response.ProductId).val(parseInt($("#qty"+response.ProductId).val())+1);
            }else{
              $("#PlaceBarcodeResult").append(
                '<div class="form-group">'+
                '<label for="">Produk</label>'+
                '<input readonly type="hidden" name="product_id[]" id="'+response.ProductId+'" value="'+response.ProductId+'" class="form-control">'+
                '<input readonly type="text" name="product[]" value="'+response.ProductName+'" class="form-control">'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="">Price</label>'+
                '<input readonly type="text" name="price[]" value="'+response.ProductPrice+'" class="form-control">'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="">Qty</label>'+
                '<input type="number" max="'+response.ProductStock+'" id="qty'+response.ProductId+'" name="qty[]" value="1" class="form-control">'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="">Discount</label>'+
                '<input type="number" name="discount[]" value="0" min="0" max="'+response.ProductPrice+'" class="form-control">'+
                '</div>'+
                product_lense +
                '<hr>'
              );
            }
            $("#submitTransaction").prop('disabled', false)
          }

        })
      }
    })
  </script>

  <script src="/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      $(".datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
      });
    });
  </script>

  <script>
    function goBack() {
      window.history.back();
    }
  </script>
@endpush
