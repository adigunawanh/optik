<table class="table table-bordered">
  <tr>
    <th>No</th>
    <th>User</th>
    <th>Activity</th>
    <th>Tanggal</th>
  </tr>
  @foreach($logs as $index => $log)
    <tr>
      <td>{{$index+1}}</td>
      <td>{{$log->name}}</td>
      <td>{{$log->activity}}</td>
      <td>{{$log->created_at}}</td>
    </tr>
  @endforeach
</table>
