@extends('layouts.app')
@section('title', 'Dashboard')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Dashboard</h3>
      </div>
      <div class="panel-body">
        @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
        @endif

        @if(Helper::getBranch()->status == '1')
          @if(Auth::user()->level == '1' || Auth::user()->level == '2')
            <a href="/transaction">
              <div class="col-md-3">
                  <div class="alert alert-warning text-center">
                    <h1>{{$transaction}}</h1>
                    <h2><i class=""></i></h2>
                    <h3><i class="fa fa-money"></i> Transaksi</h3>
                  </div>
              </div>
            </a>

            <a href="/product/all">
              <div class="col-md-3">
                <div class="alert alert-success text-center">
                  <h1>{{$product}}</h1>
                  <h3><i class="fa fa-tags"></i> Produk</h3>
                </div>
              </div>
            </a>

            <a href="/branch">
              <div class="col-md-3">
                <div class="alert alert-info text-center">
                  <h1>{{$branch}}</h1>
                  <h3><i class="fa fa-shopping-cart"></i> Cabang</h3>
                </div>
              </div>
            </a>

            <a href="#" data-target="#modalAllReport" data-toggle="modal">
              <div class="col-md-3">
                <div class="alert alert-danger text-center">
                  <h1>Cetak</h1>
                  <h3><i class="fa fa-bar-chart"></i> Laporan</h3>
                </div>
              </div>
            </a>

          @elseif(Auth::user()->level == '3')
            <a href="/pegawai">
              <div class="col-md-3">
                <div class="alert alert-success text-center">
                  <h1>{{$pegawai}}</h1>
                  <h3><i class="fa fa-users"></i> Pegawai</h3>
                </div>
              </div>
            </a>
          @elseif(Auth::user()->level == '4')
            <a href="/product/all}}">
              <div class="col-md-3">
                <div class="alert alert-success text-center">
                  <h1>{{$product}}</h1>
                  <h3><i class="fa fa-tags"></i> Produk</h3>
                </div>
              </div>
            </a>
          @elseif(Auth::user()->level == '0')
            <a href="/transaction/branch/{{Helper::getBranch()->id}}">
              <div class="col-md-3">
                  <div class="alert alert-warning text-center">
                    <h1>{{$transaction_branch}}</h1>
                    <h2><i class=""></i></h2>
                    <h3><i class="fa fa-money"></i> Transaksi</h3>
                  </div>
              </div>
            </a>

            <a href="/branch/category/{{Helper::getBranch()->id}}">
              <div class="col-md-3">
                <div class="alert alert-success text-center">
                  <h1>{{$product_branch}}</h1>
                  <h3><i class="fa fa-tags"></i> Produk</h3>
                </div>
              </div>
            </a>
          @endif

        @else
          @if(Auth::user()->level == '0' || Auth::user()->level == '1')
            <a href="/transaction/branch/{{Helper::getBranch()->id}}">
              <div class="col-md-3">
                  <div class="alert alert-warning text-center">
                    <h1>{{$transaction_branch}}</h1>
                    <h2><i class=""></i></h2>
                    <h3><i class="fa fa-money"></i> Transaksi</h3>
                  </div>
              </div>
            </a>

            <a href="/branch/category/{{Helper::getBranch()->id}}">
              <div class="col-md-3">
                <div class="alert alert-success text-center">
                  <h1>{{$product_branch}}</h1>
                  <h3><i class="fa fa-tags"></i> Produk</h3>
                </div>
              </div>
            </a>
          @endif
        @endif
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalAllReport" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Cetak Laporan</h4>
        </div>
        <div class="modal-body">
          <table class="table table-hovered">
            <tr>
              <th>Menu</th>
              <th>Pilihan</th>
            </tr>
            <tr>
              <td>Produk</td>
              <td> <a class="btn btn-success" href="/product/excel">Download Laporan</a> </td>
            </tr>
            <tr>
              <td>Transaksi</td>
              <td> <a class="btn btn-success" href="/transaction/reporting">Download Laporan</a> </td>
            </tr>
            <tr>
              <td>Supplier</td>
              <td> <a class="btn btn-success" href="/supplier/excel">Download Laporan</a> </td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection
