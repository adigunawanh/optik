@extends('layouts.app-static')
@section('title', 'Dashboard')

@section('content')
  <div class="container">
    <div class="well well-sm">
      <a class="btn btn-success" href="/log/excel">Download Excel</a>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Log Activity</h3>
      </div>
      <div class="panel-body">
        <table class="table table-bordered">
          <tr>
            <th>User</th>
            <th>Activity</th>
            <th>Tanggal</th>
          </tr>
          @foreach($logs as $log)
          <tr>
            <td>{{$log->name}}</td>
            <td>{{$log->activity}}</td>
            <td>{{$log->created_at}}</td>
          </tr>
          @endforeach
        </table>
        {{$logs->render()}}
      </div>
    </div>
  </div>
@endsection
