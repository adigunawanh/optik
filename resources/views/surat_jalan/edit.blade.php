@extends('layouts.app')
@section('title', 'Form Update Surat Jalan')

@push('style')
  <link rel="stylesheet" href="/select2/css/select2.min.css">
@endpush

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Update Surat Jalan</h3>
      </div>
      <div class="panel-body">
        <form action="/surat-jalan/update/{{Request::segment(3)}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Tanggal Terima</label>
            <input type="date" name="tanggal_terima" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">NIP Penerima</label>
            <select class="form-control select2" name="nip_penerima" required>
              @php
                $pegawai = DB::table('pegawai')->get();
              @endphp
              <option value="">Pilih Penerima</option>
              @foreach ($pegawai as $data)
                <option value="{{$data->nip}}">{{$data->nama}}</option>
              @endforeach
            </select>
          </div>
          <input type="submit" class="btn btn-primary" value="Update">
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script type="text/javascript" src="/select2/js/select2.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('.select2').select2();
  });
  </script>
@endpush
