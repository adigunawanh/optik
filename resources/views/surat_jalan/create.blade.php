@extends('layouts.app')
@section('title', 'Form Surat Jalan')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Surat Jalan</h3>
      </div>
      <div class="panel-body">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Scan Barcode</h3>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label for="">Barcode</label>
              <input type="text" class="form-control" id="ScaneBarcode" placeholder="Scan barcode...">
            </div>
          </div>
        </div>
        <hr>
        <form action="/surat-jalan/store/{{Request::segment(3)}}" method="post">
          {{csrf_field()}}
          <div id="PlaceBarcodeResult"></div>
          <input type="submit" class="btn btn-primary" value="Simpan">
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')

  <script type="text/javascript">
    $("#ScaneBarcode").change(function(){
      var barcode = $(this).val();
      if(barcode != ""){

        $.ajax({
          url : '/product/scane-barcode',
          type : 'post',
          data : {
            barcode : barcode,
            _token : '{{csrf_token()}}',
            branch_id : '{{Request::segment(3)}}',
          },
          success : function(response){
            $("#ScaneBarcode").val('');
            // console.log(response);
            if(response.ProductStockBranch > 0){
              $("#PlaceBarcodeResult").append(
                '<div class="form-group">'+
                '<label for="">Produk</label>'+
                '<input readonly type="text" value="'+response.ProductName+'" class="form-control">'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="">Jumlah</label>'+
                '<input type="text" name="jumlah[]" min="1" max="'+response.ProductStock+'" value="0" class="form-control">'+
                '</div>'+
                '<input type="hidden" name="id_stock[]" value="'+response.ProductStockId+'">'+
                '<hr>'
              );
            }else{
              $("#PlaceBarcodeResult").append(
                '<div class="form-group">'+
                '<label for="">Produk</label>'+
                '<input readonly type="text" value="'+response.ProductName+'" class="form-control">'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="">Jumlah</label>'+
                '<input type="number" name="jumlah[]" min="1" max="'+response.ProductStock+'" value="0" class="form-control">'+
                '</div>'+
                '<input type="hidden" name="id_stock[]" value="'+response.ProductStockId+'">'+
                '<hr>'
              );
            }
          }

        })
      }
    })
  </script>
@endpush
