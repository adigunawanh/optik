@extends('layouts.app')
@section('title', 'Surat Jalan')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Detail Surat Jalan</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>Produk</th>
              <th>Jumlah</th>
            </tr>
            @foreach ($surat_jalan_detail as $data)
              <tr>
                <td>{{explode(',',$data->product_field_content)[0]}}</td>
                <td>{{$data->jumlah}}</td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
