<!DOCTYPE html>
<html>
<head>
  <title>PDF Product</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body style="background:white">
  <div class="row">
    <div class="col-xs-2">
      <img src="img/zolaris.png" height="100" alt="">
    </div>
    <div class="col-xs-4" style="font-size:12px">
      <br>
      Nama Optik: <span>{{Helper::getBranch()->name}}</span><br>
      Alamat: <i style="font-size: 12px">{{Helper::getBranch()->address}}</i><br>
      No. Telp: <i style="font-size: 12px">{{Helper::getBranch()->phone}}</i>
    </div>
    <div class="col-xs-4 text-right" style="font-size:12px">
      <br>
      <span>Tanggal: {{$surat_jalan->tanggal_pengiriman}}</span><br>
      <span>Customer: {{$surat_jalan->cabang}}</span><br>
      <span>Alamat: {{$surat_jalan->branch_address}}</span>
    </div>
  </div>
  <table class="table">
    <tr>
      <th>Nomor</th>
      <th>Nama Item</th>
      <th>Jumlah</th>
      <th>Satuan</th>
      <th>Total</th>
    </tr>
    @php $total_barang = 0 @endphp
    @foreach ($surat_jalan_detail as $index => $data)
      <tr>
        <td>{{$index + 1}}</td>
        <td>{{explode(',',$data->product_field_content)[0]}}</td>
        <td>{{$data->jumlah}}</td>
        <td>Rp. {{Helper::rupiah(explode(',',$data->product_field_content)[3])}}</td>
        <td>Rp. {{Helper::rupiah(explode(',',$data->product_field_content)[3] * $data->jumlah)}}</td>
      </tr>
      @php $total_barang += $data->jumlah @endphp
    @endforeach
    <tr>
      <td></td>
      <td style="font-size:10px">Total Barang: {{$total_barang}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </table>

  <div class="col-xs-6 text-left">
    <p>Gudang<p>
    <br>
    <br>
    <br>
    ................................
  </div>
  <div class="col-xs-4 text-right">
    <p>Penerima</p>
    <br>
    <br>
    <br>
    ................................
    <br>
    {{$surat_jalan->nama_penerima}}
  </div>

</body>
</html>
