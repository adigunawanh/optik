@extends('layouts.app')
@section('title', 'Surat Jalan')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">List Surat Jalan</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>ID Surat</th>
              <th>Cabang</th>
              <th>Tanggal Pengiriman</th>
              <th>Tanggal Terima</th>
              <th>Penerima</th>
              <th>Status Surat Kirim</th>
              <th>Option</th>
            </tr>
            @foreach ($surat_jalan as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->cabang}}</td>
                <td>{{$data->tanggal_pengiriman}}</td>
                <td>{{$data->tanggal_terima}}</td>
                <td>{{$data->nama_penerima}}</td>
                <td>{{$data->tanggal_terima == '' ? 'Belum Diterima' : 'Diterima'}}</td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Option
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <li><a href="/surat-jalan/detail/{{$data->id}}">Lihat Detail</a></li>
                      <li><a href="/surat-jalan/print/{{$data->id}}">Print</a></li>
                      @if($data->tanggal_terima == '')
                        <li><a href="/surat-jalan/edit/{{$data->id}}">Update Surat Jalan</a></li>
                      @endif
                    </ul>
                  </div>
                </td>
              </tr>
            @endforeach
          </table>
          {{$surat_jalan->render()}}
        </div>
      </div>
    </div>
  </div>
@endsection
