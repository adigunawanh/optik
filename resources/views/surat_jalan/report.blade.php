<table class="table table-bordered">
  <tr>
    <th>Produk</th>
    <th>Jumlah</th>
  </tr>
  @foreach ($surat_jalan_detail as $data)
    <tr>
      <td>{{explode(',',$data->product_field_content)[0]}}</td>
      <td>{{$data->jumlah}}</td>
    </tr>
  @endforeach
</table>
