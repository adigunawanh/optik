@extends('layouts.app')
@section('title', 'Form Supplier')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Supplier</h3>
      </div>
      <div class="panel-body">
        <form action="/supplier/update/{{$supplier->id}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="name" value="{{$supplier->name}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Alamat</label>
            <input type="text" name="address" value="{{$supplier->address}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Telepon</label>
            <input type="number" name="phone" value="{{$supplier->phone}}" class="form-control">
          </div>
          <input type="submit" class="btn btn-primary" value="Simpan">
          <a href="#" onclick="goBack()" class="btn btn-default">Batal</a>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
