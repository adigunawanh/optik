@extends('layouts.app')
@section('title', 'Form Supplier')

@section('content')
  <div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Supplier</h3>
      </div>
      <div class="panel-body">
        <form action="/supplier/store" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Nama</label>
            <input required type="text" name="name" value="{{old('name')}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Alamat</label>
            <input required type="text" name="address" value="{{old('address')}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Telepon</label>
            <input required type="number" name="phone" value="{{old('phone')}}" class="form-control">
          </div>
          <input type="submit" class="btn btn-primary" value="Simpan">
          <a href="#" onclick="goBack()" class="btn btn-default">Batal</a>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
