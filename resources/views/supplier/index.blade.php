@extends('layouts.app')
@section('title', 'Supplier')

@section('content')
  <div class="container">
    <div class="well well-sm">
      <a href="/supplier/create" class="btn btn-default">Buat Supplier Baru</a>
      <a class="btn btn-success" href="/supplier/excel">Download Report Excel</a>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">List Supplier</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>Telepon</th>
              <th>Option</th>
            </tr>
            @foreach ($supplier as $index => $data)
              <tr>
                <td>{{$index+1}}</td>
                <td>{{$data->name}}</td>
                <td>{{$data->address}}</td>
                <td>{{$data->phone}}</td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="" data-toggle="dropdown">
                      Pilihan
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="">
                      <li><a href="/supplier/edit/{{$data->id}}">Edit</a></li>
                      <li><a onclick="return confirm('Yakin akan hapus data?')" href="/supplier/delete/{{$data->id}}">Delete</a></li>
                    </ul>
                  </div>
                </td>
              </tr>
            @endforeach
          </table>
          {{$supplier->render()}}
        </div>
      </div>
    </div>
  </div>
@endsection
