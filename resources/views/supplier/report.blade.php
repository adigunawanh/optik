<table class="table table-bordered">
  <tr>
    <th>Nama</th>
    <th>Alamat</th>
    <th>Telepon</th>
  </tr>
  @foreach ($supplier as $data)
    <tr>
      <td>{{$data->name}}</td>
      <td>{{$data->address}}</td>
      <td>{{$data->phone}}</td>
    </tr>
  @endforeach
</table>
