@extends('layouts.app')
@section('title', 'Form Member')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Member</h3>
      </div>
      <div class="panel-body">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="/member/store" method="post">
          {{csrf_field()}}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Member ID</label>
                <input required type="text" name="member_id" value="{{old('member_id')}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Nik</label>
                <input required type="number" name="nik" value="{{old('nik')}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Nama</label>
                <input required type="text" name="name" value="{{old('name')}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Tempat Lahir</label>
                <input required type="text" name="tempat_lahir" value="{{old('tempat_lahir')}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Tanggal Lahir</label>
                <input required type="date" name="tanggal_lahir" value="{{old('tanggal_lahir')}}" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Usia</label>
                <input required type="number" name="usia" value="{{old('usia')}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Jenis Kelamin</label><br>
                <input type="radio" name="jenis_kelamin" value="L" selected> Pria
                <input type="radio" name="jenis_kelamin" value="P"> Wanita
              </div>
              <div class="form-group">
                <label for="">Telepon</label>
                <input required type="number" name="telepon" value="{{old('telepon')}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Email</label>
                <input type="email" name="email" value="{{old('email')}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Alamat</label>
                <input required type="text" name="alamat" value="{{old('alamat')}}" class="form-control">
              </div>
              {{-- <div class="form-group">
                <label for="">Point</label>
                <input type="number" name="point" value="" class="form-control">
              </div> --}}
            </div>
          </div>
          <input type="submit" class="btn btn-primary" value="Simpan">
          <a href="#" onclick="goBack()" class="btn btn-default">Batal</a>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
