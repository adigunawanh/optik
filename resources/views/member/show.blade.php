@extends('layouts.app')
@section('title', 'Detail Member')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Detail Member</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <td>Member ID</td>
              <td>{{$member->member_id}}</td>
            </tr>
            <tr>
              <td>NIK</td>
              <td>{{$member->nik}}</td>
            </tr>
            <tr>
              <td>Nama</td>
              <td>{{$member->name}}</td>
            </tr>
            <tr>
              <td>Tempat Lahir</td>
              <td>{{$member->tempat_lahir}}</td>
            </tr>
            <tr>
              <td>Tanggal Lahir</td>
              <td>{{$member->tanggal_lahir}}</td>
            </tr>
            <tr>
              <td>Jenis Kelamin</td>
              <td>{{$member->jenis_kelamin}}</td>
            </tr>
            <tr>
              <td>Telepon</td>
              <td>{{$member->telepon}}</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>{{$member->email}}</td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td>{{$member->alamat}}</td>
            </tr>
          </table>

          @if($rekamedis)
            <h3>Data Rekamedis Terakhir</h3>

            <table class="table table-bordered">
              <tr>
                <th>OD Sphiris</th>
                <th>OS Sphiris</th>
                <th>OD Cylindris</th>
                <th>OS Cylindris</th>
                <th>OD Axis</th>
                <th>OS Axis</th>
                <th>Addition</th>
                <th>Add Kiri</th>
                <th>Add Kanan</th>
                <th>Keterangan</th>
              </tr>
              <tr>
                <td>{{$rekamedis->od_sphiris}}</td>
                <td>{{$rekamedis->os_sphiris}}</td>
                <td>{{$rekamedis->od_cylindris}}</td>
                <td>{{$rekamedis->os_cylindris}}</td>
                <td>{{$rekamedis->od_axis}}</td>
                <td>{{$rekamedis->os_axis}}</td>
                <td>{{$rekamedis->addition}}</td>
                <td>{{$rekamedis->add_kiri}}</td>
                <td>{{$rekamedis->add_kanan}}</td>
                <td>{{$rekamedis->notes}}</td>
              </tr>
            </table>
          @endif
        </div>
      </div>
    </div>
    <div class="well well-sm">
      <a href="#" onclick="goBack()" class="btn btn-default">Kembali</a>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
