@extends('layouts.app')
@section('title', 'Form Edit Member')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Edit Member</h3>
      </div>
      <div class="panel-body">
        <form action="/member/update/{{$member->id}}" method="post">
          {{csrf_field()}}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Member ID</label>
                <input type="text" name="member_id" value="{{$member->member_id}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Nik</label>
                <input type="number" name="nik" value="{{$member->nik}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Nama</label>
                <input type="text" name="name" value="{{$member->name}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Tempat Lahir</label>
                <input type="text" name="tempat_lahir" value="{{$member->tempat_lahir}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Tanggal Lahir</label>
                <input type="date" name="tanggal_lahir" value="{{$member->tanggal_lahir}}" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Usia</label>
                <input required type="number" name="usia" value="{{$member->usia}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Jenis Kelamin</label><br>
                <input {{$member->jenis_kelamin == "L" ? 'checked' : ''}} type="radio" name="jenis_kelamin" value="L"> Pria
                <input {{$member->jenis_kelamin == "P" ? 'checked' : ''}} type="radio" name="jenis_kelamin" value="P"> Wanita
              </div>
              <div class="form-group">
                <label for="">Telepon</label>
                <input type="number" name="telepon" value="{{$member->telepon}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Email</label>
                <input type="email" name="email" value="{{$member->email}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Alamat</label>
                <input type="text" name="alamat" value="{{$member->alamat}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Point</label>
                <input type="number" name="point" value="{{$member->point}}" class="form-control">
              </div>
            </div>
          </div>
          <input type="submit" class="btn btn-primary" value="Simpan">
          <a href="#" onclick="goBack()" class="btn btn-default">Batal</a>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
