@extends('layouts.app')
@section('title', 'List Member')

@section('content')
  <div class="container">
    @if(Session::get('success'))
      <div class="alert alert-success">{{Session::get('success')}}</div>
    @endif
    <div class="well well-sm">
      <a href="/member/create" class="btn btn-default">Tambah Member</a>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">List Member</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>No</th>
              <th>Member ID</th>
              <th>NIK</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>Telepon</th>
              <th>Point</th>
              <th>Pilihan</th>
            </tr>
            @foreach ($members as $index => $member)
              <tr>
                <td>{{$index+1}}</td>
                <td>{{$member->member_id}}</td>
                <td>{{$member->nik}}</td>
                <td>{{$member->name}}</td>
                <td>{{$member->alamat}}</td>
                <td>{{$member->telepon}}</td>
                <td>{{$member->point}}</td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="" data-toggle="dropdown">
                      Pilihan
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="">
                      <li><a href="/member/edit/{{$member->id}}">Edit</a></li>
                      <li><a href="/member/show/{{$member->id}}">Lihat Detail</a></li>
                      <li><a onclick="return confirm('Yakin akan hapus data?')" href="/member/delete/{{$member->id}}">Hapus</a></li>
                    </ul>
                  </div>
                </td>
              </tr>
            @endforeach
          </table>
          {{$members->render()}}
        </div>
      </div>
    </div>
  </div>
@endsection
