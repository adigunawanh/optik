<table class="table table-bordered">
  <tr>
    <th>Email</th>
    <th>Name</th>
    <th>Level</th>
  </tr>
  @foreach($users as $user)
    <tr>
      <td>{{$user->email}}</td>
      <td>{{$user->name}}</td>
      <td>{{$user->level == '1' ? 'Manager' : ($user->level == '2' ? 'Admin' : 'Kasir')}}</td>
    </tr>
  @endforeach
</table>
