@extends('layouts.app')
@section('title', 'List Cabang')

@section('content')
  <div class="container">

    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {!! Session::get('success') !!}
    </div>
    @elseif(Session::has('error'))
    <div class="alert alert-error alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {!! Session::get('error') !!}
    </div>
    @endif

    <div class="well well-sm">
      <a href="/branch/create/" class="btn btn-default">Buat Cabang Baru</a>
      <a class="btn btn-success" href="/branch/excel">Download Report Excel</a>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Cabang List</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>No</th>
              <th>Status</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>Pilihan</th>
            </tr>
            @foreach($branches as $index => $branch)
              <tr>
                <td>{{$index + 1}}</td>
                <td>{{$branch->status == '1' ? 'Pusat' : 'Cabang'}}</td>
                <td>{{$branch->name}}</td>
                <td>{{$branch->address}}</td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Option
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      @if($branch->id != 1)
                        <li><a href="/branch/category/{{$branch->id}}">Lihat Produk</a></li>
                        <li><a href="/surat-jalan/create/{{$branch->id}}">Kirim Barang</a></li>
                        <li><a href="/surat-jalan/branch/{{$branch->id}}">Lihat Surat Jalan</a></li>
                        <li><a onclick="return confirm('Apakah Yakin?')" href="/branch/delete/{{$branch->id}}">Hapus</a></li>
                      @endif
                      @if($branch->status == '1')
                        <li><a href="/product/all">Lihat Produk</a></li>
                      @endif
                      <li><a href="/branch/list-user/{{$branch->id}}">List User</a></li>
                      <li><a href="/branch/add-user/{{$branch->id}}">Add User</a></li>
                      <li><a href="/branch/edit/{{$branch->id}}">Edit</a></li>
                    </ul>
                  </div>
                </td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
