@extends('layouts.app')
@section('title', 'Form Branch')

@section('content')
  <div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Branch</h3>
      </div>
      <div class="panel-body">
        <form class="" action="/branch/store" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Email</label>
            <input required type="email" name="email" value="{{old('email')}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Password</label>
            <input required type="password" name="password" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Status</label>
            <select required class="form-control" name="status">
              <option value="1">Pusat</option>
              <option value="0">Cabang</option>
            </select>
          </div>
          <div class="form-group">
            <label for="">Nama</label>
            <input required type="text" name="name" value="{{old('name')}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Alamat</label>
            <input required type="text" name="address" value="{{old('address')}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Telepon</label>
            <input required type="text" name="phone" value="{{old('phone')}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Isi Perjanjian Frame</label>
            <textarea required name="frame_description" class="form-control" rows="8" cols="80">{{old('frame_description')}}</textarea>
          </div>
          <div class="form-group">
            <label for="">Isi Perjanjian Nota</label>
            <textarea required name="note_description" class="form-control" rows="8" cols="80">{{old('note_description')}}</textarea>
          </div>
          <input type="submit" value="Simpan" class="btn btn-primary">
          <a href="#" onclick="goBack()" class="btn btn-default">Batal</a>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
