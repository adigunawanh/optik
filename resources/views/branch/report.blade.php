<table class="table table-bordered">
  <tr>
    <th>Status</th>
    <th>Name</th>
    <th>Address</th>
  </tr>
  @foreach($branches as $branch)
    <tr>
      <td>{{$branch->status == '1' ? 'Pusat' : 'Cabang'}}</td>
      <td>{{$branch->name}}</td>
      <td>{{$branch->address}}</td>
    </tr>
  @endforeach
</table>
