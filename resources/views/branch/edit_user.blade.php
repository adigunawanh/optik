@extends('layouts.app')
@section('title', 'Form Edit Branch User')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Edit Branch User</h3>
      </div>
      <div class="panel-body">
        <form class="" action="/branch/edit-user/update/{{Request::segment(3)}}/{{Request::segment(4)}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Email</label>
            <input type="text" name="email" value="{{$user->email}}" class="form-control">
          </div>
          {{-- <div class="form-group">
            <label for="">Password</label>
            <input type="password" name="password" value="" class="form-control">
          </div> --}}
          <div class="form-group">
            <label for="">Name</label>
            <input type="text" name="name" value="{{$user->name}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Level</label>
            <select class="form-control" name="level">

              @if(!Helper::getBranchById(Request::segment(3))->status == '1')
                <option {{$user->level == '1' ? 'selected' : ''}} value="1">Manager</option>
                <option{{$user->level == '0' ? 'selected' : ''}}  value="0">Kasir</option>
              @else
                <option {{$user->level == '2' ? 'selected' : ''}} value="2">Super Admin</option>
                <option {{$user->level == '3' ? 'selected' : ''}} value="3">HR</option>
                <option {{$user->level == '4' ? 'selected' : ''}} value="4">Gudang</option>
                <option {{$user->level == '1' ? 'selected' : ''}} value="1">Manager</option>
                <option {{$user->level == '0' ? 'selected' : ''}} value="0">Kasir</option>
              @endif
            </select>
          </div>
          <input type="submit" value="Simpan" class="btn btn-primary">
        </form>
      </div>
    </div>
  </div>
@endsection
