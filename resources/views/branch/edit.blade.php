@extends('layouts.app')
@section('title', 'Form Branch')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Branch</h3>
      </div>
      <div class="panel-body">
        <form class="" action="/branch/update/{{$Branch->id}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="name" value="{{$Branch->name}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Alamat</label>
            <input type="text" name="address" value="{{$Branch->address}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Telepon</label>
            <input type="text" name="phone" value="{{$Branch->phone}}" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Isi Perjanjian Frame</label>
            <textarea name="frame_description" class="form-control" rows="8" cols="80">{{$Branch->frame_description}}</textarea>
          </div>
          <div class="form-group">
            <label for="">Isi Perjanjian Nota</label>
            <textarea name="note_description" class="form-control" rows="8" cols="80">{{$Branch->note_description}}</textarea>
          </div>
          <input type="submit" value="Simpan" class="btn btn-primary">
        </form>
      </div>
    </div>
    <div class="well well-sm">
      <a href="#" onclick="goBack()" class="btn btn-default">Back</a>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
