@extends('layouts.app')
@section('title', 'Product Branch - ' . $branch->name)

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">All Products Branch - {{$branch->name}}</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-hover">
            <tr>
              <th>Category</th>
              <th>Total Product</th>
              <th>Action</th>
            </tr>
            @foreach($Categories as $Category)
              @php
              $total_product = DB::table('products')
              ->join('product_category_fields','product_category_fields.id','=','products.product_category_field_id')
              ->join('product_stock','product_stock.product_id','=','products.id')
              ->join('product_stock_branch','product_stock.id','=','product_stock_branch.product_stock_id')
              ->where('product_category_field_id', $Category->id)
              ->where('branch_id', $branch_id)
              ->count();
              @endphp
              <tr>
                <td>{{$Category->name}}</td>
                <td>{{$total_product}}</td>
                <td>
                  <a class="btn btn-default" href="/branch/product/detail/{{$Category->id}}/{{$branch_id}}">Lihat List Produk</a>
                </td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
    <div class="well well-sm">
      <a href="/branch" class="btn btn-default">Back To Branch</a>
    </div>
  </div>
@endsection
