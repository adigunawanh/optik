@extends('layouts.app')
@section('title', 'Form Branch User')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Branch User</h3>
      </div>
      <div class="panel-body">
        <form class="" action="/branch/add-user/store/{{Request::segment(3)}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Email</label>
            <input type="text" name="email" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Password</label>
            <input type="password" name="password" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Name</label>
            <input type="text" name="name" value="" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Level</label>
            <select class="form-control" name="level">

              @if(!Helper::getBranchById(Request::segment(3))->status == '1')
                <option value="1">Manager</option>
                <option value="0">Kasir</option>
              @else
                <option value="2">Super Admin</option>
                <option value="3">HR</option>
                <option value="4">Gudang</option>
                <option value="1">Manager</option>
                <option value="0">Kasir</option>
              @endif
            </select>
          </div>
          <input type="submit" value="Simpan" class="btn btn-primary">
        </form>
      </div>
    </div>
  </div>
@endsection
