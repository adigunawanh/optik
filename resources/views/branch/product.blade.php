@extends('layouts.app')
@section('title', 'Product')

@section('content')
  <div class="container">
    <div class="text-center">
      @if(Session::has('success'))
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! Session::get('success') !!}
      </div>
      @elseif(Session::has('error'))
      <div class="alert alert-error alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! Session::get('error') !!}
      </div>
      @endif
    </div>
    <div class="well well-sm">
      {{-- <a href="/product/create/{{Request::segment(4)}}" class="btn btn-default">Create</a> --}}
      @if(Auth::user()->level == '2' || Auth::user()->level == '4')
        <a href="#" data-toggle="modal" data-target="#ModalCustomPrint" class="btn btn-danger">Custom Print Barcode</a>
      @endif
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Product</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-hover">
            <tr>
              <th>ID Produk</th>
              @foreach($ProductFields as $field)
                @if($field->name != 'Harga Beli' && $field->name != 'Jumlah')
                  <th>{{$field->name}}</th>
                @endif
              @endforeach
                <th>Stock</th>
                <th>Barcode</th>
                @if(Helper::getBranch()->status == '1')
                  <th>Action</th>
                @endif
            </tr>
            @foreach($Products as $Product)
              @php
                $ProductContents = [];
                $ProductContents = explode(',', $Product->product_field_content);
                unset($ProductContents[2]); // hide harga beli
                unset($ProductContents[1]); // hide jumlah
              @endphp
              <tr>
                <td>{{$Product->id}}</td>
                @foreach($ProductContents as $ProductContent)
                  <td>{{$ProductContent}}</td>
                @endforeach
                <td>{{$Product->stock}}</td>
                <td><img width="80" src="data:image/png;base64,{!!DNS2D::getBarcodePNG($Product->barcode, "QRCODE")!!}" alt="barcode"/> <br> <span class="text-center"> {{$Product->barcode}} </span> </td>
                @if(Helper::getBranch()->status == '1')
                  <td>
                    <div class="dropdown">
                      <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Option
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        <li><a href="/product/edit/{{$Product->id}}">Edit</a></li>
                        <li><a href="/product/delete/{{$Product->id}}">Delete</a></li>
                        <li><a target="_blank" href="/product/print/{{$Product->id}}/{{$Product->stock}}">Print Barcode</a></li>
                      </ul>
                    </div>
                  </td>
                @endif
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
    <div class="well well-sm">
      <a href="#" onclick="goBack()" class="btn btn-default">Back</a>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function goBack() {
        window.history.back();
    }
  </script>
@endpush
