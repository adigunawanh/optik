@extends('layouts.app')
@section('title', 'List Branch')

@section('content')
  <div class="container">

    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {!! Session::get('success') !!}
    </div>
    @elseif(Session::has('error'))
    <div class="alert alert-error alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {!! Session::get('error') !!}
    </div>
    @endif

    <div class="well well-sm">
      <a href="/branch/add-user/{{Request::segment(3)}}" class="btn btn-default">Tambah User</a>
      <a class="btn btn-success" href="/branch/list-user-excel/{{Request::segment(3)}}">Download Report Excel</a>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">List User</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>Email</th>
              <th>Name</th>
              <th>Level</th>
              <th>Option</th>
            </tr>
            @foreach($users as $user)
              <tr>
                <td>{{$user->email}}</td>
                <td>{{$user->name}}</td>
                <td>
                  {{
                    $user->level == '1' ?
                    'Manager' :
                    ($user->level == '2' ? 'Super Admin' :
                    ($user->level == '3' ? 'HR' : ($user->level == '4' ? 'Gudang' : 'Kasir')))
                  }}
                </td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="" data-toggle="dropdown">
                      Pilihan
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="">
                      <li> <a href="/branch/edit-user/{{Request::segment(3)}}/{{$user->id}}">Edit</a> </li>
                      <li> <a onclick="return confirm('yakin akan menghapus data?')" href="/branch/delete-user/{{$user->id}}">Hapus</a> </li>
                    </ul>
                  </div>
                </td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
