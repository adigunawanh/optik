@extends('layouts.app')
@section('title', 'Form Pembeli')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Pembeli</h3>
      </div>
      <div class="panel-body">
        <form action="/grosir/pembeli/store" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Nama Toko</label>
            <input required type="text" name="nama_toko" class="form-control" placeholder="Nama Toko">
          </div>
          <div class="form-group">
            <label for="">Nama Pemilik</label>
            <input required type="text" name="nama_pemilik" class="form-control" placeholder="Nama Pemilik">
          </div>
          <div class="form-group">
            <label for="">Alamat</label>
            <input required type="text" name="alamat" class="form-control" placeholder="Alamat">
          </div>
          <div class="form-group">
            <label for="">Telepon</label>
            <input required type="text" name="telepon_pemilik" class="form-control" placeholder="Telepon">
          </div>
          <div class="form-group">
            <label for="">Hp</label>
            <input required type="text" name="hp_pemilik" class="form-control" placeholder="No Hp Pemilik">
          </div>
          <input type="submit" class="btn btn-primary" value="Simpan">
          <a href="/grosir/pembeli" class="btn btn-default">Cancel</a>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')

@endpush
