@extends('layouts.app')
@section('title', 'Form Edit Pembeli')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Edit Pembeli</h3>
      </div>
      <div class="panel-body">
        <form action="/grosir/pembeli/update/{{$pembeli->id}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Nama Toko</label>
            <input required type="text" value="{{$pembeli->nama_toko}}" name="nama_toko" class="form-control" placeholder="Nama Toko">
          </div>
          <div class="form-group">
            <label for="">Nama Pemilik</label>
            <input required type="text" value="{{$pembeli->nama_pemilik}}" name="nama_pemilik" class="form-control" placeholder="Nama Pemilik">
          </div>
          <div class="form-group">
            <label for="">Alamat</label>
            <input required type="text" name="alamat" value="{{$pembeli->alamat}}" class="form-control" placeholder="Alamat">
          </div>
          <div class="form-group">
            <label for="">Telepon</label>
            <input required type="text" name="telepon_pemilik" value="{{$pembeli->telepon}}" class="form-control" placeholder="Telepon">
          </div>
          <div class="form-group">
            <label for="">Hp</label>
            <input  required type="text" name="hp_pemilik" value="{{$pembeli->hp}}" class="form-control" placeholder="No Hp Pemilik">
          </div>
          <input type="submit" class="btn btn-primary" value="Simpan">
          <a href="/grosir/pembeli" class="btn btn-default">Cancel</a>
        </form>
      </div>
    </div>
  </div>
@endsection
