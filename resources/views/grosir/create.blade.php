@extends('layouts.app')
@section('title', 'Form Grosir')

@push('style')
  <link rel="stylesheet" href="/select2/css/select2.min.css">
@endpush

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Grosir</h3>
      </div>
      <div class="panel-body">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Scan Barcode</h3>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label for="">Barcode</label>
              <input type="text" class="form-control" id="ScaneBarcode" placeholder="Scan barcode...">
            </div>
          </div>
        </div>
        <hr>
        <form action="/grosir/store" method="post">
          {{csrf_field()}}
          <div id="PlaceBarcodeResult"></div>
          <hr>
          <div class="form-group">
            <label for="">Pembeli</label>
            <select class="form-control select2" name="id_pembeli">
              @foreach($pembeli as $data)
                <option value="{{$data->id}}">{{$data->nama_toko}}</option>
              @endforeach
            </select>
          </div>
          <hr>
          <div class="form-group">
            <label for="">Nomor Surat</label>
            <input type="text" name="nomor_surat" class="form-control" placeholder="Nomor Surat" required>
          </div>
          <div class="form-group">
            <label for="">Nama Pembeli</label>
            <input type="text" name="nama_pembeli" class="form-control" placeholder="Nama Pembeli" required>
          </div>
          <div class="form-group">
            <label for="">Telepon Pembeli</label>
            <input type="text" name="hp" class="form-control" placeholder="Telepon Pembeli" required>
          </div>
          <input type="submit" id="submitGrosir" disabled class="btn btn-primary" value="Lanjutkan">
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')

  <script type="text/javascript" src="/select2/js/select2.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('.select2').select2();
  });
  </script>

  <script type="text/javascript">
    $("#ScaneBarcode").change(function(){
      var barcode = $(this).val();
      if(barcode != ""){

        $.ajax({
          url : '/product/scane-barcode',
          type : 'post',
          data : {
            barcode : barcode,
            _token : '{{csrf_token()}}',
            branch_id : '{{Request::segment(3)}}',
          },
          success : function(response){
            $("#ScaneBarcode").val('');
            // console.log(response);
            if(response.ProductStockBranch > 0){
              $("#PlaceBarcodeResult").append(
                '<div class="form-group">'+
                '<label for="">Produk</label>'+
                '<input readonly type="text" value="'+response.ProductName+'" class="form-control">'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="">Price</label>'+
                '<input readonly type="text" name="price[]" value="'+response.ProductPrice+'" class="form-control">'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="">Jumlah</label>'+
                '<input required min="1" type="text" name="jumlah[]" value="0" class="form-control">'+
                '</div>'+
                '<input type="hidden" name="id_stock[]" value="'+response.ProductStockId+'">'+
                '<hr>'
              );
            }else{
              $("#PlaceBarcodeResult").append(
                '<div class="form-group">'+
                '<label for="">Produk</label>'+
                '<input readonly type="text" value="'+response.ProductName+'" class="form-control">'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="">Price</label>'+
                '<input readonly type="text" name="price[]" value="'+response.ProductPrice+'" class="form-control">'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="">Jumlah</label>'+
                '<input required min="1" type="number" name="jumlah[]" value="0" class="form-control">'+
                '</div>'+
                '<input type="hidden" name="id_stock[]" value="'+response.ProductStockId+'">'+
                '<hr>'
              );
            }
            $("#submitGrosir").prop('disabled', false)
          }

        })
      }
    })
  </script>
@endpush
