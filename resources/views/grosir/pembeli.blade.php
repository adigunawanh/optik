@extends('layouts.app')
@section('title', 'List Pembeli')

@section('content')
  <div class="container">
    @if(Session::get('success'))
      <div class="alert alert-success">{{Session::get('success')}}</div>
    @endif
    <div class="well well-sm">
      <a class="btn btn-default" href="/grosir/pembeli/create">Buat Pembeli</a>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">List Pembeli</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>No</th>
              <th>Nama Toko</th>
              <th>Nama Pemilik</th>
              <th>Alamat</th>
              <th>Telepon</th>
              <th>Hp</th>
              <th>Pilihan</th>
            </tr>
            @foreach ($pembeli as $index => $data)
              <tr>
                <td>{{$index+1}}</td>
                <td>{{$data->nama_toko}}</td>
                <td>{{$data->nama_pemilik}}</td>
                <td>{{$data->alamat}}</td>
                <td>{{$data->telepon}}</td>
                <td>{{$data->hp}}</td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="" data-toggle="dropdown">
                      Pilihan
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="">
                      <li><a href="/grosir/pembeli/edit/{{$data->id}}">Edit</a></li>
                      <li><a onclick="return confirm('Yakin akan hapus data?')" href="/grosir/pembeli/delete/{{$data->id}}">Hapus</a></li>
                    </ul>
                  </div>
                </td>
              </tr>
            @endforeach
          </table>
          {{$pembeli->render()}}
        </div>
      </div>
    </div>
  </div>
@endsection
