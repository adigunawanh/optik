<!DOCTYPE html>
<html>
<head>
  <title>PDF Product</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body style="background:white">
  <div class="">
    <div class="row">
      <div class="col-xs-6">
        <img src="img/zolaris.png" height="100">
        <!-- <h3>{{Helper::getBranch()->name}}</h3> -->
        <h4>{{Helper::getBranch()->address}}</h4>
        <span>Tanggal : {{$Grosir->created_at}}</span><br>
      </div>
      <div class="col-xs-4">
        <div class="text-right">
          <br>
          <img src="data:image/png;base64,{!!DNS2D::getBarcodePNG($Grosir->nomor_surat, "QRCODE")!!}" alt="barcode"/>
          <br>
          <img width="150px" height="25px" src="data:image/png;base64,{!!DNS1D::getBarcodePNG($Grosir->nomor_surat, "C39")!!}" alt="barcode"/>
          <br>
          <p>{{$Grosir->nomor_surat}}</p>
          <br>
          <span>Nama Toko : {{$Grosir->nama_toko}}</span><br>
          <span>Nama Pembeli : {{$Grosir->nama_pembeli}}</span><br>
          <span>Alamat : {{$Grosir->alamat}}</span>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <br>
        <div class="table-responsive">
          <table class="table table-bordered" style="font-size:9px; padding:7px">
            <tr>
              <th>Nama Item</th>
              <th>Jumlah</th>
              <th>Satuan</th>
              <th>Harga</th>
              <th>Diskon</th>
              <th>Total</th>
            </tr>
            <?php $total =  0?>
            @foreach($GrosirDetails as $GrosirDetail)
              @php
                $ProductContents = explode(',', $GrosirDetail->product_field_content);
              @endphp
              <tr>
                <td>{{$ProductContents[0]}}</td>
                <td>{{$GrosirDetail->jumlah}}</td>
                <td>Rp. {{number_format($GrosirDetail->harga_jual, 0 , '.', '.')}}</td>
                <td>Rp. {{number_format($GrosirDetail->harga_jual * $GrosirDetail->jumlah, 0 , '.', '.')}}</td>
                <td>0%</td>
                <td>Rp. {{number_format($GrosirDetail->harga_jual * $GrosirDetail->jumlah, 0 , '.', '.')}}</td>
              </tr>
              <?php $total += $GrosirDetail->harga_jual * $GrosirDetail->jumlah ?>
            @endforeach

            <tr>
              <td style="text-align:right" colspan="6"><strong>Sub Total : Rp. {{number_format($total, 0 , '.', '.')}}</strong></td>
            </tr>
          </table>
        </div>
      </div>
    </div>

    <div class="row" style="font-size:10px">
      <div class="col-xs-6">
        <p>1. Pembayaran dapat ditransfer ke rekening</p>
        <p>2. Pembayaran hanya dilakukan ke rekening tertera</p>
        <p>3. Pembayaran dengan metode transfer harap mencantumkan No Invoice</p>
        <p>4. Pembayaran dengan metode Cek / Giro dianggap lunas bila diterima di rekening Kami</p>
        <p>5. Nilai invoice adalah FINAL, tidak dapat lagi dilakukan perubahan</p>
        <p>6. Mohon dicek kembali barang yang Anda terima jika dinyatakan cacat dapat ditukar dalam 3x24 jam</p>
      </div>
      <div class="col-xs-4 text-right">
        <p>Hormat Kami</p>
        <br>
        <br>
        <br>
        <br>
        ______________________
      </div>
    </div>

  </div>
</body>
</html>
