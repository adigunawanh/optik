@extends('layouts.app')
@section('title', 'Detail Grosir')

@section('content')
  <div class="container">
    <div class="well well-sm">
      <a class="btn btn-default" href="/grosir">Back</a>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Info Return</h3>
      </div>
      <div class="panel-body">
        <div class="alert alert-success">Barang telah di return oleh : {{$return->nama_peretur}}</div>
          <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>Produk</th>
              <th>Jumlah</th>
            </tr>
            @foreach ($return_detail as $data)
              <tr>
                <td>{{explode(',',$data->product_field_content)[0]}}</td>
                <td>{{$data->jumlah}}</td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
