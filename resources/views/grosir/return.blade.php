@extends('layouts.app')
@section('title', 'Form Return Grosir')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Form Return Grosir</h3>
      </div>
      <div class="panel-body">
        <form action="/grosir/return/store/{{Request::segment(3)}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Nama Peretur</label>
            <input type="text" name="nama_peretur" class="form-control" placeholder="Nama Peretur">
          </div>
          @foreach($grosir_surat_detail as $data)
            <div class="form-group">
              <label for="">Product</label>
              <input readonly type="text" value="{{explode(',',$data->product_field_content)[0]}}" class="form-control" placeholder="Nama Peretur">
            </div>
            <input type="hidden" name="id_stock[]" value="{{$data->id_stock}}">
            <div class="form-group">
              <label for="">Jumlah</label>
              <input type="text" name="jumlah[]" value="{{$data->jumlah}}" class="form-control" placeholder="Jumlah">
            </div>
          @endforeach
          <input type="submit" class="btn btn-primary" value="Simpan">
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
@endpush
