@extends('layouts.app')
@section('title', 'Detail Grosir')

@section('content')
  <div class="container">
    <div class="well well-sm">
      <a class="btn btn-default" href="/grosir">Back</a>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Detail Surat Grosir</h3>
      </div>
      <div class="panel-body">
        <table class="table table-bordered">
          <tr>
            <th>Produk</th>
            <th>Jumlah</th>
            <th>Harga Jual</th>
          </tr>
          @foreach ($grosir_surat_detail as $data)
            <tr>
              <td>{{explode(',',$data->product_field_content)[0]}}</td>
              <td>{{$data->jumlah}}</td>
              <td>{{$data->harga_jual}}</td>
            </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
@endsection
