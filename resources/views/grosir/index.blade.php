@extends('layouts.app')
@section('title', 'Surat Grosir')

@section('content')
  <div class="container">
    <div class="well well-sm">
      <a class="btn btn-default" href="grosir/create">Buat Grosir Baru</a>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">List Surat Grosir</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>No</th>
              <th>Nomor Surat</th>
              <th>Nama Pembeli</th>
              <th>No Hp Pembeli</th>
              <th>Total Bayar</th>
              <th>Tanggal</th>
              <th>Option</th>
            </tr>
            @foreach ($grosir_surat as $index => $data)
              <tr>
                <td>{{$index+1}}</td>
                <td>{{$data->nomor_surat}}</td>
                <td>{{$data->nama_pembeli}}</td>
                <td>{{$data->hp}}</td>
                <td>{{$data->total_bayar}}</td>
                <td>{{$data->created_at}}</td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Option
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <li><a href="/grosir/detail/{{$data->id}}">Lihat Detail</a></li>
                      <li><a href="/grosir/return/{{$data->id}}">Return</a></li>
                      <li><a href="/grosir/print/{{$data->id}}">Cetak Nota</a></li>
                    </ul>
                  </div>
                </td>
              </tr>
            @endforeach
          </table>
          {{$grosir_surat->render()}}
        </div>
      </div>
    </div>
  </div>
@endsection
